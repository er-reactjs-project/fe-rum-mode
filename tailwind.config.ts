import type { Config } from 'tailwindcss';
import defaultTheme from 'tailwindcss/defaultTheme';

const config: Config = {
  content: [
    './app/**/*.{js,ts,jsx,tsx,mdx}',
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',

    // Or if using `src` directory:
    './src/**/*.{js,ts,jsx,tsx,mdx}'
  ],
  theme: {
    extend: {
      screens: {
        xs: '475px',
        '2xs': '300px',
        ...defaultTheme.screens
      },
      fontFamily: {
        glory: ['var(--font-glory)']
      },
      colors: {
        // Primary Series
        'primary-01': '#F4F4FF',
        'primary-02': '#BDBDFF',
        'primary-03': '#8D8CF2',
        'primary-04': '#4746AC',
        'primary-05': '#1F1E84',
        'primary-06': '#010066', // ** Main Color
        'primary-07': '#00003E',
        'primary-08': '#0A0A2A',
        // Neutral Series
        'neutral-01': '#FFFFFF',
        'neutral-02': '#E6E6E6',
        'neutral-02-background': '#F0F0F0',
        'neutral-03': '#C8C8C8',
        'neutral-04': '#A0A0A0',
        'neutral-05': '#787878',
        'neutral-06': '#505050', // ** Main Color
        'neutral-07': '#282828',
        'neutral-08': '#000000',
        // Red Series
        'red-01': '#D43030',

        secondary: '#C97A4E'
      },
      keyframes: {
        'fade-in-down': {
          '0%': {
            opacity: '0',
            transform: 'translateY(-10px)'
          },
          '100%': {
            opacity: '1',
            transform: 'translateY(0)'
          }
        }
      },
      animation: {
        'fade-in-down': 'fade-in-down 0.5s ease-out'
      },
      backgroundImage: {
        'indonesian-flag': "url('/indonesian-flag.svg')",
        'english-flag': "url('/english-flag.svg')",
        //
        'landing-rumah-mode':
          "url('/assets/images/landing-page/rumah-mode.svg')",
        'banner-landing-page-1':
          "url('/assets/images/banner/landing-page/HOME-WEB-1.jpg')",
        'banner-landing-page-2':
          "url('/assets/images/banner/landing-page/HOME-WEB-2.jpg')",
        'banner-landing-page-3':
          "url('/assets/images/banner/landing-page/HOME-WEB-3.jpg')",
        'banner-event-1': "url('/assets/images/event/Image1.png')",
        'banner-event-2': "url('/assets/images/event/Image2.png')",
        'banner-event-3': "url('/assets/images/event/Image3.png')",
        'banner-event-4': "url('/assets/images/event/Image4.png')",
        'banner-event-5': "url('/assets/images/event/Image5.png')",
        'banner-todo-1': "url('/assets/images/todo/Image1.png')",
        'banner-todo-2': "url('/assets/images/todo/Image2.png')",

        'whatsapp-logo': "url('/assets/images/whatsapp.svg')",
        'about-us-banner': "url('/assets/images/banner/aboutus_banner.svg')",
        'shopping-fashion-banner':
          "url('/assets/images/banner/shopping_fashion_banner.svg')",
        'shopping-assistant-banner':
          "url('/assets/images/banner/shopping-assistant-banner.svg')",
        'login-banner': "url('/assets/images/login/bg_login.svg')",
        'register-banner': "url('/assets/images/login/bg_register.svg')",
        //
        'dummy-shopping-fashion-1':
          "url('/assets/images/shopping/fashion/dummy-shopping-fashion-1.svg')",
        'dummy-shopping-fashion-2':
          "url('/assets/images/shopping/fashion/dummy-shopping-fashion-2.svg')",
        'dummy-shopping-fashion-3':
          "url('/assets/images/shopping/fashion/dummy-shopping-fashion-3.svg')",
        'dummy-shopping-fashion-4':
          "url('/assets/images/shopping/fashion/dummy-shopping-fashion-4.svg')",
        'dummy-shopping-fashion-5':
          "url('/assets/images/shopping/fashion/dummy-shopping-fashion-5.svg')",
        'dummy-shopping-fashion-6':
          "url('/assets/images/shopping/fashion/dummy-shopping-fashion-6.svg')",
        //
        'dummy-brand-banner':
          "url('/assets/images/shopping/fashion/brand/dummy-brand-banner.svg')",
        'dummy-brand-1':
          "url('/assets/images/shopping/fashion/brand/dummy-brand-1.svg')",
        'dummy-brand-2':
          "url('/assets/images/shopping/fashion/brand/dummy-brand-2.svg')",
        'dummy-brand-3':
          "url('/assets/images/shopping/fashion/brand/dummy-brand-3.svg')",
        'dummy-brand-4':
          "url('/assets/images/shopping/fashion/brand/dummy-brand-4.svg')",
        //
        'reservation-packages-banner':
          "url('/assets/images/reservation/packages/packages-banner.svg')",
        'reservation-packages-detail-banner':
          "url('/assets/images/reservation/packages/packages-detail-banner.svg')",
        'packages-immage-1':
          "url('/assets/images/reservation/packages/packages-immage-1.svg')",
        'packages-immage-2':
          "url('/assets/images/reservation/packages/packages-immage-2.svg')",
        'packages-immage-3':
          "url('/assets/images/reservation/packages/packages-immage-3.svg')",
        'packages-immage-4':
          "url('/assets/images/reservation/packages/packages-immage-4.svg')",
        'packages-detail-image-1':
          "url('/assets/images/reservation/packages/packages-detail-image-1.svg')",
        'packages-detail-image-2':
          "url('/assets/images/reservation/packages/packages-detail-image-2.svg')",
        //
        'rumod-maps': "url('/assets/images/maps/rumod-maps.svg')",
        //
        'gojek-logo': "url('/assets/images/access/gojek-logo.svg')",
        'grab-logo': "url('/assets/images/access/grab-logo.svg')",
        'indrive-logo': "url('/assets/images/access/indrive-logo.svg')",
        'maxim-logo': "url('/assets/images/access/maxim-logo.svg')",
        'angkot-logo': "url('/assets/images/access/angkot-logo.svg')",
        'tmetro-logo': "url('/assets/images/access/tmetro-logo.svg')",
        //
        'membership-banner':
          "url('/assets/images/membership/membership-banner.svg')",
        'membership-benefit-1':
          "url('/assets/images/membership/membership-benefit-1.svg')",
        'membership-benefit-2':
          "url('/assets/images/membership/membership-benefit-2.svg')",
        'membership-benefit-3':
          "url('/assets/images/membership/membership-benefit-3.svg')",
        'membership-benefit-4':
          "url('/assets/images/membership/membership-benefit-4.svg')",
        'membership-benefit-5':
          "url('/assets/images/membership/membership-benefit-5.svg')",
        'membership-benefit-6':
          "url('/assets/images/membership/membership-benefit-6.svg')",
        //
        'dummy-blog-image-1':
          "url('/assets/images/blog/dummy-blog-image-1.svg')",
        'dummy-blog-image-2':
          "url('/assets/images/blog/dummy-blog-image-2.svg')",
        'dummy-blog-image-3':
          "url('/assets/images/blog/dummy-blog-image-3.svg')",
        'dummy-blog-image-4':
          "url('/assets/images/blog/dummy-blog-image-4.svg')",
        'dummy-blog-image-5':
          "url('/assets/images/blog/dummy-blog-image-5.svg')",
        'dummy-blog-image-6':
          "url('/assets/images/blog/dummy-blog-image-6.svg')",
        //
        'dummy-profile': "url('/assets/images/profile/dummy-profile.svg')",
        'my-profile-icon': "url('/assets/images/profile/my-profile-icon.svg')",
        'my-poin-icon': "url('/assets/images/profile/my-poin-icon.svg')",
        'my-history-icon': "url('/assets/images/profile/my-history-icon.svg')",
        'my-reward-icon': "url('/assets/images/profile/my-reward-icon.svg')"
      }
    }
  },
  plugins: []
};
export default config;
