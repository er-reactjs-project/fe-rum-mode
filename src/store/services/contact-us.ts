import { createAsyncThunk } from '@reduxjs/toolkit';
import { serviceGetWithoutAuth } from './_config';

const getContactUs = createAsyncThunk(
  'get/getContactUs',
  async (payload: any, { rejectWithValue }) => {
    return serviceGetWithoutAuth(
      `${process.env.NEXT_PUBLIC_API_URL}/contact-us`
    )(payload).catch((err) => rejectWithValue(err.response.data));
  }
);

const ContactUsService = {
  getContactUs
};

export default ContactUsService;
