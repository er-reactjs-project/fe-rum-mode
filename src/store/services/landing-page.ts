import { createAsyncThunk } from '@reduxjs/toolkit';
import { serviceGetWithoutAuth } from './_config';

const getLandingPage = createAsyncThunk(
  'get/getLandingPage',
  async (payload: any, { rejectWithValue }) => {
    return serviceGetWithoutAuth(
      `${process.env.NEXT_PUBLIC_API_URL}/landing-page`
    )(payload).catch((err) => rejectWithValue(err.response.data));
  }
);

const LandingPageService = {
  getLandingPage
};

export default LandingPageService;
