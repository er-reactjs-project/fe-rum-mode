import { createAsyncThunk } from '@reduxjs/toolkit';
import { serviceGet, servicePut, servicePutFormData } from './_config';

const getProfile = createAsyncThunk('user/getProfile', async (payload: any, { rejectWithValue }) => {
    return serviceGet(`${process.env.NEXT_PUBLIC_API_URL}/users/profile`)(
      payload
    ).catch((err) => rejectWithValue(err.response.data));
  }
);
const updateProfile = createAsyncThunk('user/updateProfile', async (payload: any, { rejectWithValue }) => {
    return servicePut(`${process.env.NEXT_PUBLIC_API_URL}/users/profile`)(
      payload
    ).catch((err) => rejectWithValue(err.response.data));
  }
);
const updateAvatar = createAsyncThunk('user/updateAvatar', async (payload: any, { rejectWithValue }) => {
    return servicePutFormData(`${process.env.NEXT_PUBLIC_API_URL}/users/change-avatar`)(
      payload
    ).catch((err) => rejectWithValue(err.response.data));
  }
);

const UserService = {
  getProfile,
  updateAvatar,
  updateProfile,
};

export default UserService;
