import axios from 'axios';

import { getToken } from '@/libs/local-storage/token';

axios.interceptors.response.use(
  (response) => response,
  (error) => {
    if (error.code === 'ERR_NETWORK') {
      error.message =
        'Unable to connect to server, please try again in a while.';
    }

    if (!error.response) {
      error.message =
        'Unable to get desired response from server, please try again in a while.';
    }

    return Promise.reject(error);
  }
);

// POST
export const servicePost = (api: string) => (data: any) => {
  return axios.post(api, data, {
    headers: { Authorization: `Bearer ${getToken()}` }
  });
};

export const servicePostWithSlug = (api: string) => (slug: string, data: any) => {
  return axios.post(`${api}/${slug}`, data, {
    // URL 1 :
    headers: { Authorization: `Bearer ${getToken()}` }
    // URL 2
    // headers: {
    //   Authorization: `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9yZWNvbi1zdmMua2FpLmlkXC9hcGlcL2F1dGgiLCJpYXQiOjE3MDczMDg3MDAsImV4cCI6MTcwNzMxMjMwMCwibmJmIjoxNzA3MzA4NzAwLCJqdGkiOiJqcVkxWWpoeXNVY2IzTWFzIiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.2As2M6rBDBrAJyCX35NOTnxkdEKGeJpXxhrifGYr_4A`
    // }
  });
};

export const servicePostWithoutAuth = (api: string) => (data: any) => {
  return axios.post(api, data);
};

// GET
export const serviceGet = (api: string) => (params: any) => {
  return axios.get(api, {
    // URL 1 :
    headers: { Authorization: `Bearer ${getToken()}` },
    // URL 2
    // headers: {
    //   Authorization: `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9yZWNvbi1zdmMua2FpLmlkXC9hcGlcL2F1dGgiLCJpYXQiOjE3MDczMDg3MDAsImV4cCI6MTcwNzMxMjMwMCwibmJmIjoxNzA3MzA4NzAwLCJqdGkiOiJqcVkxWWpoeXNVY2IzTWFzIiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.2As2M6rBDBrAJyCX35NOTnxkdEKGeJpXxhrifGYr_4A`
    // },
    params
  });
};

export const serviceGetWithSlug = (api: string) => (slug: string, params: any) => {
  return axios.get(`${api}/${slug}`, {
    // URL 1 :
    headers: { Authorization: `Bearer ${getToken()}` },
    // URL 2
    // headers: {
    //   Authorization: `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9yZWNvbi1zdmMua2FpLmlkXC9hcGlcL2F1dGgiLCJpYXQiOjE3MDczMDg3MDAsImV4cCI6MTcwNzMxMjMwMCwibmJmIjoxNzA3MzA4NzAwLCJqdGkiOiJqcVkxWWpoeXNVY2IzTWFzIiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.2As2M6rBDBrAJyCX35NOTnxkdEKGeJpXxhrifGYr_4A`
    // },
    params
  });
};

export const serviceGetWithoutAuth = (api: string) => (params: any) => {
  return axios.get(api, { params });
};

export const serviceGetByIdWithoutAuth = (api: string) => (id: string, params: any) => {
  return axios.get(`${api}/${id}`, { params });
};

// PUT
export const servicePut = (api: string) => (data: any) => {
  return axios.put(api, data, {
    headers: { Authorization: `Bearer ${getToken()}` }
  });
};

export const servicePutWithSlug = (api: string) => (slug: string, data: any) => {
  return axios.put(`${api}/${slug}`, data, {
    // URL 1 :
    headers: { Authorization: `Bearer ${getToken()}` }
    // URL 2
    // headers: {
    //   Authorization: `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9yZWNvbi1zdmMua2FpLmlkXC9hcGlcL2F1dGgiLCJpYXQiOjE3MDczMDg3MDAsImV4cCI6MTcwNzMxMjMwMCwibmJmIjoxNzA3MzA4NzAwLCJqdGkiOiJqcVkxWWpoeXNVY2IzTWFzIiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.2As2M6rBDBrAJyCX35NOTnxkdEKGeJpXxhrifGYr_4A`
    // }
  });
};

// DELETE
export const serviceDelete = (api: string) => () => {
  return axios.delete(api, {
    // URL 1 :
    headers: { Authorization: `Bearer ${getToken()}` }
    // URL 2
    // headers: {
    //   Authorization: `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9yZWNvbi1zdmMua2FpLmlkXC9hcGlcL2F1dGgiLCJpYXQiOjE3MDczMDg3MDAsImV4cCI6MTcwNzMxMjMwMCwibmJmIjoxNzA3MzA4NzAwLCJqdGkiOiJqcVkxWWpoeXNVY2IzTWFzIiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.2As2M6rBDBrAJyCX35NOTnxkdEKGeJpXxhrifGYr_4A`
    // }
  });
};

export const serviceDeleteWithSlug = (api: string) => (slug: string) => {
  return axios.delete(`${api}/${slug}`, {
    // URL 1 :
    headers: { Authorization: `Bearer ${getToken()}` }
    // URL 2
    // headers: {
    //   Authorization: `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9yZWNvbi1zdmMua2FpLmlkXC9hcGlcL2F1dGgiLCJpYXQiOjE3MDczMDg3MDAsImV4cCI6MTcwNzMxMjMwMCwibmJmIjoxNzA3MzA4NzAwLCJqdGkiOiJqcVkxWWpoeXNVY2IzTWFzIiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.2As2M6rBDBrAJyCX35NOTnxkdEKGeJpXxhrifGYr_4A`
    // }
  });
};

// FORM DATA
export const servicePutFormData = (api: string) => (data: FormData) => {
  return axios.put(api, data, {
    headers: { 
      Authorization: `Bearer ${getToken()}`,
      "Content-Type": "multipart/form-data", 
    }
  });
};