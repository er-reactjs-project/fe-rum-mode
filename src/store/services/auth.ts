import { createAsyncThunk } from '@reduxjs/toolkit';
import { servicePostWithoutAuth } from './_config';

const register = createAsyncThunk(
  'auth/register',
  async (payload: any, { rejectWithValue }) => {
    return servicePostWithoutAuth(
      `${process.env.NEXT_PUBLIC_API_URL}/auth/register`
    )(payload).catch((err) => rejectWithValue(err.response.data));
  }
);
const login = createAsyncThunk(
  'auth/login',
  async (payload: any, { rejectWithValue }) => {
    return servicePostWithoutAuth(
      `${process.env.NEXT_PUBLIC_API_URL}/auth/login`
    )(payload).catch((err) => rejectWithValue(err.response.data));
  }
);
const getVerificationCode = createAsyncThunk(
  'auth/getVerificationCode',
  async (payload: any, { rejectWithValue }) => {
    return servicePostWithoutAuth(
      `${process.env.NEXT_PUBLIC_API_URL}/auth/get-verification-code`
    )(payload).catch((err) => rejectWithValue(err.response.data));
  }
);
const forgotPassword = createAsyncThunk(
  'auth/forgotPassword',
  async (payload: any, { rejectWithValue }) => {
    return servicePostWithoutAuth(
      `${process.env.NEXT_PUBLIC_API_URL}/auth/forgot-password`
    )(payload).catch((err) => rejectWithValue(err.response.data));
  }
);

const AuthService = {
  register,
  login,
  getVerificationCode,
  forgotPassword
};

export default AuthService;
