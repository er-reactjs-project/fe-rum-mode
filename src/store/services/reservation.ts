import { createAsyncThunk } from '@reduxjs/toolkit'
import { serviceGetWithoutAuth } from './_config'

const getReservation = createAsyncThunk('get/getReservation', async (payload: any, { rejectWithValue }) => {
  return serviceGetWithoutAuth(`${process.env.NEXT_PUBLIC_API_URL}/reservation`)(payload).catch(err =>
    rejectWithValue(err.response.data)
  )
})

const getDetailPackage = createAsyncThunk('get/getDetailPackage', async (payload: { [key: string]: any; }, { rejectWithValue }) => {
  const id = payload.id
  delete payload.id

  return serviceGetWithoutAuth(`${process.env.NEXT_PUBLIC_API_URL}/reservation/packages/${id}`)({...payload}).catch(err =>
    rejectWithValue(err.response.data)
  )
})

const reservation = {
  getReservation,
  getDetailPackage,
}

export default reservation
