import { createAsyncThunk } from '@reduxjs/toolkit';
import { serviceGetWithoutAuth } from './_config';

const getPersonalization = createAsyncThunk('get/getPersonalization', async (payload: any, { rejectWithValue }) => {
  return serviceGetWithoutAuth(`${process.env.NEXT_PUBLIC_API_URL}/personalizations`)(payload).catch((err) =>
    rejectWithValue(err.response.data)
  );
});

const PersonalizationService = {
  getPersonalization
};

export default PersonalizationService;
