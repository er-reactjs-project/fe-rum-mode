import AuthService from './auth';
import PersonalizationService from './personalization';
import AboutService from './about';
import ShoppingService from './shopping';
import DiscoverService from './discover';
import ReservationService from './reservation';
import KitchenService from './kitchen';
import BlogService from './blog';
import SpecialOfferService from './special-offer';
import ContactUsService from './contact-us';
import CareerService from './career';
import UserService from './user';
import LandingPageService from './landing-page';

export {
  AuthService,
  PersonalizationService,
  AboutService,
  ShoppingService,
  DiscoverService,
  ReservationService,
  KitchenService,
  BlogService,
  SpecialOfferService,
  ContactUsService,
  CareerService,
  UserService,
  LandingPageService
};
