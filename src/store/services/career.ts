import { createAsyncThunk } from '@reduxjs/toolkit';
import { serviceGetWithoutAuth } from './_config';

const getCareer = createAsyncThunk(
  'get/getCareer',
  async (payload: any, { rejectWithValue }) => {
    return serviceGetWithoutAuth(`${process.env.NEXT_PUBLIC_API_URL}/careers`)(
      payload
    ).catch((err) => rejectWithValue(err.response.data));
  }
);

const CareerService = {
  getCareer
};

export default CareerService;
