import { createAsyncThunk } from '@reduxjs/toolkit'
import { serviceGetWithoutAuth } from './_config'

const getSpecialOffer = createAsyncThunk('get/getSpecialOffer', async (payload: any, { rejectWithValue }) => {
  return serviceGetWithoutAuth(`${process.env.NEXT_PUBLIC_API_URL}/special-offer`)(payload).catch(err =>
    rejectWithValue(err.response.data)
  )
})
const getListOfferItems = createAsyncThunk('get/getListOfferItems', async (payload: any, { rejectWithValue }) => {
  return serviceGetWithoutAuth(`${process.env.NEXT_PUBLIC_API_URL}/special-offer/items`)(payload).catch(err =>
    rejectWithValue(err.response.data)
  )
})

const specialOffer = {
  getSpecialOffer,
  getListOfferItems,
}

export default specialOffer
