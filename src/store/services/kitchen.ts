import { createAsyncThunk } from '@reduxjs/toolkit'
import { serviceGetWithoutAuth } from './_config'

const getModeKitchen = createAsyncThunk('get/getModeKitchen', async (payload: any, { rejectWithValue }) => {
  return serviceGetWithoutAuth(`${process.env.NEXT_PUBLIC_API_URL}/mode-kitchen`)(payload).catch(err =>
    rejectWithValue(err.response.data)
  )
})

const getAllStore = createAsyncThunk('get/getAllStore', async (payload: any, { rejectWithValue }) => {
  return serviceGetWithoutAuth(`${process.env.NEXT_PUBLIC_API_URL}/mode-kitchen/stores/all`)(payload).catch(err =>
    rejectWithValue(err.response.data)
  )
})

const getDetailStore = createAsyncThunk('get/getDetailStore', async (payload: { [key: string]: any; }, { rejectWithValue }) => {
  const id = payload.id
  delete payload.id

  return serviceGetWithoutAuth(`${process.env.NEXT_PUBLIC_API_URL}/mode-kitchen/stores/${id}`)({...payload}).catch(err =>
    rejectWithValue(err.response.data)
  )
})

const kitchen = {
  getModeKitchen,
  getAllStore,
  getDetailStore,
}

export default kitchen
