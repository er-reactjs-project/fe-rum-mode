import { createAsyncThunk } from '@reduxjs/toolkit'
import { serviceGetWithoutAuth } from './_config'

const getAbout = createAsyncThunk('get/about', async (payload: any, { rejectWithValue }) => {
  return serviceGetWithoutAuth(`${process.env.NEXT_PUBLIC_API_URL}/about/detail`)(payload).catch(err =>
    rejectWithValue(err.response.data)
  )
})

const about = {
  getAbout
}

export default about
