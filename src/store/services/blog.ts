import { createAsyncThunk } from '@reduxjs/toolkit'
import { serviceGetWithoutAuth } from './_config'

const getListBlog = createAsyncThunk('get/getListBlog', async (payload: any, { rejectWithValue }) => {
  return serviceGetWithoutAuth(`${process.env.NEXT_PUBLIC_API_URL}/blogs`)(payload).catch(err =>
    rejectWithValue(err.response.data)
  )
})

const getDetailBlog = createAsyncThunk('get/getDetailBlog', async (payload: { [key: string]: any; }, { rejectWithValue }) => {
  const id = payload.id
  delete payload.id

  return serviceGetWithoutAuth(`${process.env.NEXT_PUBLIC_API_URL}/blogs/${id}`)({...payload}).catch(err =>
    rejectWithValue(err.response.data)
  )
})

const reservation = {
  getListBlog,
  getDetailBlog,
}

export default reservation
