import { createAsyncThunk } from '@reduxjs/toolkit';
import { serviceGetWithoutAuth, serviceGetByIdWithoutAuth } from './_config';

const getFashionList = createAsyncThunk('get/FashionList', async (payload: any, { rejectWithValue }) => {
  return serviceGetWithoutAuth(`${process.env.NEXT_PUBLIC_API_URL}/shopping/fashion`)(payload).catch((err) =>
    rejectWithValue(err.response.data)
  );
});

const getFashionDetail = createAsyncThunk('get/FashionDetail', async ({ id, payload }: any, { rejectWithValue }) => {
  return serviceGetByIdWithoutAuth(`${process.env.NEXT_PUBLIC_API_URL}/shopping/fashion`)(id, payload).catch((err) =>
    rejectWithValue(err.response.data)
  );
});

const getShoppingAssistant = createAsyncThunk('get/ShoppingAssistant', async (payload: any, { rejectWithValue }) => {
  return serviceGetWithoutAuth(`${process.env.NEXT_PUBLIC_API_URL}/shopping/assistant`)(payload).catch((err) =>
    rejectWithValue(err.response.data)
  );
});

const getEstore = createAsyncThunk('get/Estore', async (payload: any, { rejectWithValue }) => {
  return serviceGetWithoutAuth(`${process.env.NEXT_PUBLIC_API_URL}/shopping/estore`)(payload).catch((err) =>
    rejectWithValue(err.response.data)
  );
});

const ShoppingService = {
  getFashionList,
  getFashionDetail,
  getShoppingAssistant,
  getEstore
};

export default ShoppingService;
