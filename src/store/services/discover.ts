import { createAsyncThunk } from '@reduxjs/toolkit'
import { serviceGetWithoutAuth } from './_config'

const getDiscoverByMenu = createAsyncThunk('get/getDiscoverByMenu', async (payload: any, { rejectWithValue }) => {
  return serviceGetWithoutAuth(`${process.env.NEXT_PUBLIC_API_URL}/discover`)(payload).catch(err =>
    rejectWithValue(err.response.data)
  )
})

const getFacilities = createAsyncThunk('get/getFacilities', async (payload: any, { rejectWithValue }) => {
  return serviceGetWithoutAuth(`${process.env.NEXT_PUBLIC_API_URL}/discover/facilities`)(payload).catch(err =>
    rejectWithValue(err.response.data)
  )
})

const getDetailFacility = createAsyncThunk('get/getDetailFacility', async (payload: { [key: string]: any; }, { rejectWithValue }) => {
  const id = payload.id
  delete payload.id

  return serviceGetWithoutAuth(`${process.env.NEXT_PUBLIC_API_URL}/discover/facilities/${id}`)({...payload}).catch(err =>
    rejectWithValue(err.response.data)
  )
})

const discover = {
  getDetailFacility,
  getDiscoverByMenu,
  getFacilities,
}

export default discover
