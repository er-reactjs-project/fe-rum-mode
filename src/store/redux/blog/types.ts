export interface Blog {
  id: string;
  title: string;
  banner: string;
  read_time_minute: number;
  content: string;
  created_at?: string;
}

export interface ListBlog {
  main: Blog;
  records: Blog[];
}

export interface DetailBlog extends Blog {
  other_blogs: Blog[];
}






