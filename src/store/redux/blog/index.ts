import { createSlice } from '@reduxjs/toolkit'
import { HYDRATE } from 'next-redux-wrapper'
import { DetailBlog, ListBlog } from './types'
import { BlogService } from '@/store/services'
import { Pagination } from '../types'

const initialState = {
  loading: false,
  blog: undefined as DetailBlog | undefined,
  listBlog: undefined as ListBlog | undefined,
  paging: {
    page: 1,
    page_size: 5,
    total_data: 0,
    total_page: 0,
  } as Pagination
}

export const blogSlice = createSlice({
  name: 'blogSlice',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(HYDRATE, (state, action) => {
      return {
        ...state,
        ...action
      }
    })

    // Get List Blog
    builder.addCase(BlogService.getListBlog.pending, state => {
      state.loading = true
    })
    builder.addCase(BlogService.getListBlog.fulfilled, (state, action) => {
      state.loading = false
      state.listBlog = action.payload.data.data
      state.paging = action.payload.data.paging
    })
    builder.addCase(BlogService.getListBlog.rejected, state => {
      state.loading = false
    })

    // Get Detail Blog
    builder.addCase(BlogService.getDetailBlog.pending, state => {
      state.loading = true
    })
    builder.addCase(BlogService.getDetailBlog.fulfilled, (state, action) => {
      state.loading = false
      state.blog = action.payload.data.data
    })
    builder.addCase(BlogService.getDetailBlog.rejected, state => {
      state.loading = false
    })
  }
})

export default blogSlice.reducer
