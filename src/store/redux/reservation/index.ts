import { createSlice } from '@reduxjs/toolkit'
import { HYDRATE } from 'next-redux-wrapper'
import { DetailPackage, Reservation } from './types'
import { ReservationService } from '@/store/services'

const initialState = {
  loading: false,
  reservation: undefined as Reservation | undefined,
  detailPackage: undefined as DetailPackage | undefined,
}

export const reservationSlice = createSlice({
  name: 'reservationSlice',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(HYDRATE, (state, action) => {
      return {
        ...state,
        ...action
      }
    })

    // Get Reservation
    builder.addCase(ReservationService.getReservation.pending, state => {
      state.loading = true
    })
    builder.addCase(ReservationService.getReservation.fulfilled, (state, action) => {
      state.loading = false
      state.reservation = action.payload.data.data
    })
    builder.addCase(ReservationService.getReservation.rejected, state => {
      state.loading = false
    })

    // Get Detail Package
    builder.addCase(ReservationService.getDetailPackage.pending, state => {
      state.loading = true
    })
    builder.addCase(ReservationService.getDetailPackage.fulfilled, (state, action) => {
      state.loading = false
      state.detailPackage = action.payload.data.data
    })
    builder.addCase(ReservationService.getDetailPackage.rejected, state => {
      state.loading = false
    })
  }
})

export default reservationSlice.reducer
