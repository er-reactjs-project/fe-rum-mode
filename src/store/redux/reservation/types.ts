export interface Package {
  id: string
  title: string
  banner: string
  short_description: string
}

export interface PackageImage {
  id: string
  image: string
  package_id: string
}

export interface DetailPackage extends Package {
  description: string
  images: PackageImage[]
}

export interface Reservation {
  id: string
  title: string
  banner: string
  packages: Package[]
}





