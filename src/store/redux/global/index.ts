import { createSlice } from "@reduxjs/toolkit"


export const globalSlice = createSlice({
    name: 'dashboard',
    initialState: {
        lang: 'en'
    },
    reducers: {
        setLanguage: (state, action) => {
            state.lang = action.payload
        },
    },
})

export const { setLanguage } = globalSlice.actions
export default globalSlice.reducer
