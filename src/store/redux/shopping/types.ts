export interface FashionListTypes {
  id: string;
  main_image: string;
  name: string;
}

export interface FashionTypes {
  id: string;
  banner: string;
  description_1: string;
  description_2: string;
  items: FashionListTypes[];
}

export interface FashionImageTypes {
  id: number | string;
  image: string;
}

export interface FashionDetailTypes {
  id: string;
  banner: string;
  description_1: string;
  description_2: string;
  fb_link: string;
  ig_link: string;
  tiktok_link: string;
  website_link: string;
  images: FashionImageTypes[];
}


export interface AssistantContactType {
  id: string;
  contact: string;
}
export interface ShoppingAssistantType {
  id: string;
  banner: string;
  description_1: string;
  description_2: string;
  description_3: string;
  contacts: AssistantContactType[];
}


export interface ShoppingEstoreType {
  id: string;
  link: string;
}
