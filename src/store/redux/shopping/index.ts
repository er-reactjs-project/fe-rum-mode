import { createSlice } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';
import { ShoppingService } from '@/store/services';
import { FashionTypes, FashionDetailTypes, ShoppingAssistantType, ShoppingEstoreType } from './types';

const initialState = {
  loading: false,
  fashion: undefined as FashionTypes | undefined,
  fashionDetail: undefined as FashionDetailTypes | undefined,
  assistant: undefined as ShoppingAssistantType | undefined,
  estore: undefined as ShoppingEstoreType | undefined,
};

export const shoppingSlice = createSlice({
  name: 'shoppingSlice',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(HYDRATE, (state, action) => {
      return {
        ...state,
        ...action
      };
    });

    // Get Fashion List
    builder.addCase(ShoppingService.getFashionList.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(ShoppingService.getFashionList.fulfilled, (state, action) => {
      state.loading = false;
      state.fashion = action.payload.data.data;
    });
    builder.addCase(ShoppingService.getFashionList.rejected, (state) => {
      state.loading = false;
    });

    // Get Fashion Detail
    builder.addCase(ShoppingService.getFashionDetail.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(ShoppingService.getFashionDetail.fulfilled, (state, action) => {
      state.loading = false;
      state.fashionDetail = action.payload.data.data;
    });
    builder.addCase(ShoppingService.getFashionDetail.rejected, (state) => {
      state.loading = false;
    });

    // Get Shopping Assistant
    builder.addCase(ShoppingService.getShoppingAssistant.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(ShoppingService.getShoppingAssistant.fulfilled, (state, action) => {
      state.loading = false;
      state.assistant = action.payload.data.data;
    });
    builder.addCase(ShoppingService.getShoppingAssistant.rejected, (state) => {
      state.loading = false;
    });

    // Get Estore
    builder.addCase(ShoppingService.getEstore.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(ShoppingService.getEstore.fulfilled, (state, action) => {
      state.loading = false;
      state.estore = action.payload.data.data;
    });
    builder.addCase(ShoppingService.getEstore.rejected, (state) => {
      state.loading = false;
    });
  }
});

export default shoppingSlice.reducer;
