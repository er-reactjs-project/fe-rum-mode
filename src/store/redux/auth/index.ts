import { createSlice } from '@reduxjs/toolkit';
import { AuthService } from '@/store/services';
import { setToken } from '@/libs/local-storage/token';
import { setUser } from '@/libs/local-storage/user';
import { User } from '../user/types';

const initialState = {
  loading: false,
  user: undefined as User | undefined
};

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    // Register
    builder.addCase(AuthService.register.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(AuthService.register.fulfilled, (state) => {
      state.loading = false;
    });
    builder.addCase(AuthService.register.rejected, (state) => {
      state.loading = false;
    });

    // Login
    builder.addCase(AuthService.login.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(AuthService.login.fulfilled, (state, action) => {
      state.loading = false;
      const user: User = action.payload.data.data.user;
      state.user = user;
      setUser(user);

      const accessToken = action.payload.data.data.access_token;
      setToken(accessToken);
    });
    builder.addCase(AuthService.login.rejected, (state) => {
      state.loading = false;
    });

    // Get Verification Code
    builder.addCase(AuthService.getVerificationCode.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(AuthService.getVerificationCode.fulfilled, (state) => {
      state.loading = false;
    });
    builder.addCase(AuthService.getVerificationCode.rejected, (state) => {
      state.loading = false;
    });

    // Forgot Password
    builder.addCase(AuthService.forgotPassword.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(AuthService.forgotPassword.fulfilled, (state) => {
      state.loading = false;
    });
    builder.addCase(AuthService.forgotPassword.rejected, (state) => {
      state.loading = false;
    });
  }
});

export default authSlice.reducer;
