export interface User {
  id: string;
  fullname: string;
  email: string;
  phone_number: string;
  address: string;
  avatar: string;
  role: string;
  birth_date: string;
  created_at: string;
  updated_at: string;
}






