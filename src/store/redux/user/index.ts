import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { UserService } from '@/store/services';
import { User } from './types';

const initialState = {
  loading: false,
  loadingAvatar: false,
  user: undefined as User | undefined
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUserData (state, action: PayloadAction<User | undefined>) {
      state.user = action.payload
    },
  },
  extraReducers: (builder) => {
    // Get Profile
    builder.addCase(UserService.getProfile.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(UserService.getProfile.fulfilled, (state, action) => {
      state.loading = false;
      state.user = action.payload.data.data;
    });
    builder.addCase(UserService.getProfile.rejected, (state) => {
      state.loading = false;
    });
    // Update Profile
    builder.addCase(UserService.updateProfile.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(UserService.updateProfile.fulfilled, (state) => {
      state.loading = false;
    });
    builder.addCase(UserService.updateProfile.rejected, (state) => {
      state.loading = false;
    });
    // Update Avatar
    builder.addCase(UserService.updateAvatar.pending, (state) => {
      state.loadingAvatar = true;
    });
    builder.addCase(UserService.updateAvatar.fulfilled, (state) => {
      state.loadingAvatar = false;
    });
    builder.addCase(UserService.updateAvatar.rejected, (state) => {
      state.loadingAvatar = false;
    });
  }
});

export const { setUserData } = userSlice.actions
export default userSlice.reducer;
