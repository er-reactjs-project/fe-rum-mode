export interface CareerTypes {
  description: string;
  email: string;
  id: string;
  title: string;
}
