import { createSlice } from '@reduxjs/toolkit';
import { CareerService } from '@/store/services';
import { CareerTypes } from './types';

const initialState = {
  loading: false,
  career: [] as CareerTypes[]
};

export const CareerSlice = createSlice({
  name: 'career',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    // Register
    builder.addCase(CareerService.getCareer.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(CareerService.getCareer.fulfilled, (state, action) => {
      state.loading = false;
      state.career = action.payload.data.data;
    });
    builder.addCase(CareerService.getCareer.rejected, (state) => {
      state.loading = false;
    });
  }
});

export default CareerSlice.reducer;
