export interface SpecialOffer {
  id: string;
  title: string;
}

export interface SpecialOfferItem {
  id: string;
  title: string;
  image: string;
  description: string;
}





