import { createSlice } from '@reduxjs/toolkit'
import { HYDRATE } from 'next-redux-wrapper'
import { SpecialOfferService } from '@/store/services'
import { Pagination } from '../types'
import { SpecialOffer, SpecialOfferItem } from './types'

const initialState = {
  loading: false,
  specialOffer: undefined as SpecialOffer | undefined,
  listOffer: [] as SpecialOfferItem[],
  paging: {
    page: 1,
    page_size: 5,
    total_data: 0,
    total_page: 0,
  } as Pagination
}

export const specialOfferSlice = createSlice({
  name: 'specialOfferSlice',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(HYDRATE, (state, action) => {
      return {
        ...state,
        ...action
      }
    })

    // Get Special Offer
    builder.addCase(SpecialOfferService.getSpecialOffer.pending, state => {
      state.loading = true
    })
    builder.addCase(SpecialOfferService.getSpecialOffer.fulfilled, (state, action) => {
      state.loading = false
      state.specialOffer = action.payload.data.data
    })
    builder.addCase(SpecialOfferService.getSpecialOffer.rejected, state => {
      state.loading = false
    })

    // Get List Offer
    builder.addCase(SpecialOfferService.getListOfferItems.pending, state => {
      state.loading = true
    })
    builder.addCase(SpecialOfferService.getListOfferItems.fulfilled, (state, action) => {
      state.loading = false
      state.listOffer = action.payload.data.data
      state.paging = action.payload.data.paging
    })
    builder.addCase(SpecialOfferService.getListOfferItems.rejected, state => {
      state.loading = false
    })
  }
})

export default specialOfferSlice.reducer
