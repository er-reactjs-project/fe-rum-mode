export interface ContactsItemsTypes {
  id: string;
  name: string;
  phone_number: string;
}

export interface ContactUsTypes {
  id: string;
  title: string;
  contacts: { [key: string]: ContactsItemsTypes[]};
}
