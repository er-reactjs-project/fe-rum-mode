import { createSlice } from '@reduxjs/toolkit';
import { ContactUsService } from '@/store/services';
import { ContactUsTypes } from './types';

const initialState = {
  loading: false,
  contactUs: undefined as ContactUsTypes | undefined
};

export const ContactUsSlice = createSlice({
  name: 'contactUs',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    // Register
    builder.addCase(ContactUsService.getContactUs.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(
      ContactUsService.getContactUs.fulfilled,
      (state, action) => {
        state.loading = false;
        state.contactUs = action.payload.data.data;
      }
    );
    builder.addCase(ContactUsService.getContactUs.rejected, (state) => {
      state.loading = false;
    });
  }
});

export default ContactUsSlice.reducer;
