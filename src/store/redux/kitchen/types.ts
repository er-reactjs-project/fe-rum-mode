export interface KitchenStore {
  id: string
  name: string
  location: string
  image: string
  description: string
}

export interface DetailKitchenStore extends KitchenStore {
  category: string
  open_hour: string
  close_hour: string
  link: string
  menu: string
  tags: string[]
}

export interface ModeKitchenPartners {
  id: string
  image: string
}

export interface ModeKitchen {
  id: string
  logo: string
  description: string
  partners: ModeKitchenPartners[]
  stores: {[index: string]: KitchenStore[]}
}





