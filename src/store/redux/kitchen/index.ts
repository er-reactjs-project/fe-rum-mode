import { createSlice } from '@reduxjs/toolkit'
import { HYDRATE } from 'next-redux-wrapper'
import { KitchenService } from '@/store/services'
import { DetailKitchenStore, KitchenStore, ModeKitchen } from './types'

const initialState = {
  loading: false,
  kitchen: undefined as ModeKitchen | undefined,
  listStore: [] as KitchenStore[],
  detailStore: undefined as DetailKitchenStore | undefined,
}

export const kitchenSlice = createSlice({
  name: 'kitchenSlice',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(HYDRATE, (state, action) => {
      return {
        ...state,
        ...action
      }
    })

    // Get Mode Kitchen
    builder.addCase(KitchenService.getModeKitchen.pending, state => {
      state.loading = true
    })
    builder.addCase(KitchenService.getModeKitchen.fulfilled, (state, action) => {
      state.loading = false
      state.kitchen = action.payload.data.data
    })
    builder.addCase(KitchenService.getModeKitchen.rejected, state => {
      state.loading = false
    })

    // Get All Store
    builder.addCase(KitchenService.getAllStore.pending, state => {
      state.loading = true
    })
    builder.addCase(KitchenService.getAllStore.fulfilled, (state, action) => {
      state.loading = false
      state.listStore = action.payload.data.data
    })
    builder.addCase(KitchenService.getAllStore.rejected, state => {
      state.loading = false
    })

    // Get Detail Store
    builder.addCase(KitchenService.getDetailStore.pending, state => {
      state.loading = true
    })
    builder.addCase(KitchenService.getDetailStore.fulfilled, (state, action) => {
      state.loading = false
      state.detailStore = action.payload.data.data
    })
    builder.addCase(KitchenService.getDetailStore.rejected, state => {
      state.loading = false
    })
  }
})

export default kitchenSlice.reducer
