export interface AboutHistory {
  id: string
  description: string
  year: number
}

export interface About {
  id: string
  banner: string
  description: string
  histories: AboutHistory[]
}
