import { createSlice } from '@reduxjs/toolkit'
import { HYDRATE } from 'next-redux-wrapper'
import { About } from './types'
import { AboutService } from '@/store/services'

const initialState = {
  loading: false,
  about: undefined as About | undefined
}

export const aboutSlice = createSlice({
  name: 'aboutSlice',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(HYDRATE, (state, action) => {
      return {
        ...state,
        ...action
      }
    })

    // Get About
    builder.addCase(AboutService.getAbout.pending, state => {
      state.loading = true
    })
    builder.addCase(AboutService.getAbout.fulfilled, (state, action) => {
      state.loading = false
      state.about = action.payload.data.data
    })
    builder.addCase(AboutService.getAbout.rejected, state => {
      state.loading = false
    })
  }
})

export default aboutSlice.reducer
