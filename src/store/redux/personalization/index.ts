import { createSlice } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';
import { PersonalizationService } from '@/store/services';
import { Personalization } from './types';

const initialState = {
  loading: false,
  personalization: undefined as Personalization | undefined
};

export const personalizationSlice = createSlice({
  name: 'personalizationSlice',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(HYDRATE, (state, action) => {
      return {
        ...state,
        ...action
      };
    });

    // Get personlization
    builder.addCase(
      PersonalizationService.getPersonalization.pending,
      (state) => {
        state.loading = true;
      }
    );
    builder.addCase(
      PersonalizationService.getPersonalization.fulfilled,
      (state, action) => {
        state.loading = false;
        state.personalization = action.payload.data.data;
      }
    );
    builder.addCase(
      PersonalizationService.getPersonalization.rejected,
      (state) => {
        state.loading = false;
      }
    );
  }
});

export default personalizationSlice.reducer;
