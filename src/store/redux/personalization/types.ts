export interface Personalization {
  id: string
  primary_color: string
  secondary_color: string
  topbar_logo: string
  footer_logo: string
  open_hour: string
  font_category: string
  font_family: string
  font_link: string
  font_category_menu: string
  font_family_menu: string
  font_link_menu: string
}





