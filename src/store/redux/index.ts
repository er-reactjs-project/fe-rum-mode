import { configureStore } from '@reduxjs/toolkit';
import authReducer from './auth';
import aboutReducer from './about';
import shoppingReducer from './shopping';
import discoverReducer from './discover';
import reservationReducer from './reservation';
import kitchenReducer from './kitchen';
import blogReducer from './blog';
import specialOfferReducer from './special-offer';
import globalReducer from './global';
import ContactUsreducer from './contact-us';
import CareerReducer from './career';
import UserReducer from './user';
import personalizationReducer from './personalization';
import landingPageReducer from './landing-page';

export const store = configureStore({
  reducer: {
    auth: authReducer,
    global: globalReducer,
    about: aboutReducer,
    shopping: shoppingReducer,
    discover: discoverReducer,
    reservation: reservationReducer,
    kitchen: kitchenReducer,
    blog: blogReducer,
    specialOffer: specialOfferReducer,
    contactUs: ContactUsreducer,
    career: CareerReducer,
    user: UserReducer,
    personalization: personalizationReducer,
    landing: landingPageReducer
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false
    })
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
