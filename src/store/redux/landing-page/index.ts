import { createSlice } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';
import { LandingPageService } from '@/store/services';
import { LandingPageTypes } from './types';

const initialState = {
  loading: false,
  landingPageData: undefined as LandingPageTypes | undefined
};

export const landingPageSlice = createSlice({
  name: 'landingPageSlice',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(HYDRATE, (state, action) => {
      return {
        ...state,
        ...action
      };
    });

    // Get Landing Page
    builder.addCase(LandingPageService.getLandingPage.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(
      LandingPageService.getLandingPage.fulfilled,
      (state, action) => {
        state.loading = false;
        state.landingPageData = action.payload.data.data;
      }
    );
    builder.addCase(LandingPageService.getLandingPage.rejected, (state) => {
      state.loading = false;
    });
  }
});

export default landingPageSlice.reducer;
