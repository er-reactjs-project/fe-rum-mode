export interface KeenSliderAssetsTypes {
  id: number,
  asset: string;
  asset_type: string;
}
export interface LandingPageTypes {
  banners: KeenSliderAssetsTypes[];
  event_images: KeenSliderAssetsTypes[];
  todo_images: KeenSliderAssetsTypes[];
  event_title: string;
  header: string;
  id: string;
  superiority_image: string;
  todo_title: string;
  link_wa?: string,
  link_ig?: string,
  link_tiktok?: string,
  link_location?: string,
}
