export interface DiscoverBanners {
  id: string
  banner: string
}

export interface Discover {
  id: string
  menu: string
  description: string
  banners: DiscoverBanners[]
}

export interface Facility {
  id: string
  icon: string
  name: string
}

export interface FacilityImage {
  id: string
  image: string
}

export interface DetailFacility extends Facility {
  description: string
  images: FacilityImage[]
}


