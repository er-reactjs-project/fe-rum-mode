import { createSlice } from '@reduxjs/toolkit'
import { HYDRATE } from 'next-redux-wrapper'
import { DetailFacility, Discover, Facility } from './types'
import { DiscoverService } from '@/store/services'

const initialState = {
  loading: false,
  discover: undefined as Discover | undefined,
  loadingFacilities: false,
  facilities: [] as Facility[],
  facility: undefined as DetailFacility | undefined,
}

export const discoverSlice = createSlice({
  name: 'discoverSlice',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(HYDRATE, (state, action) => {
      return {
        ...state,
        ...action
      }
    })

    // Get Discover By Menu
    builder.addCase(DiscoverService.getDiscoverByMenu.pending, state => {
      state.loading = true
    })
    builder.addCase(DiscoverService.getDiscoverByMenu.fulfilled, (state, action) => {
      state.loading = false
      state.discover = action.payload.data.data
    })
    builder.addCase(DiscoverService.getDiscoverByMenu.rejected, state => {
      state.loading = false
    })

    // Get Facilities
    builder.addCase(DiscoverService.getFacilities.pending, state => {
      state.loadingFacilities = true
    })
    builder.addCase(DiscoverService.getFacilities.fulfilled, (state, action) => {
      state.loadingFacilities = false
      state.facilities = action.payload.data.data
    })
    builder.addCase(DiscoverService.getFacilities.rejected, state => {
      state.loadingFacilities = false
    })

    // Get Detail Facility
    builder.addCase(DiscoverService.getDetailFacility.pending, state => {
      state.loading = true
    })
    builder.addCase(DiscoverService.getDetailFacility.fulfilled, (state, action) => {
      state.loading = false
      state.facility = action.payload.data.data
    })
    builder.addCase(DiscoverService.getDetailFacility.rejected, state => {
      state.loading = false
    })
  }
})

export default discoverSlice.reducer
