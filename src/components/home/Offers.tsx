import React from 'react';
import Link from 'next/link';
import Card from '../shared/Card';
import { IoIosArrowForward } from 'react-icons/io';
import Wrapper from '../shared/Wrapper';
import { useAppSelector } from '@/hooks/store';
import Skeleton from '../shared/Skeleton';

const HomeOffers = () => {
  // const { loading, listOffer } = useAppSelector((state) => state.specialOffer);
  const { listOffer, loading } = useAppSelector((state) => state.specialOffer);

  // bg-[#343385]
  return (
    <div>
      <div className="py-8 bg-[--primary] flex items-center justify-center gap-6">
        <hr className="w-[100px]" />
        <p className={`text-3xl text-white text-center`}>
          Special Offers
        </p>
        <hr className="w-[100px]" />
      </div>
      <div className="pb-6 pt-[50px] flex gap-6 justify-center flex-col bg-[--primary-alpha]">
        <Wrapper className="xs:px-24 sm:px-36">
          <div className="grid items-center justify-center xs:grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-6">
            {loading &&
              [...Array(4)].map((_, i) => (
                <div
                  key={i}
                  className="flex flex-col items-start lg:w-full xs:w-72 xs:mb-5"
                >
                  <Skeleton
                    loading={loading}
                    type="image"
                    className="h-[250px] mb-3"
                  />
                  <Skeleton loading={loading} />
                </div>
              ))}

            {!loading &&
              listOffer &&
              listOffer
                .slice(0, 4)
                .map((item, itemIndex) => (
                  <Card key={itemIndex} title={item.title} image={item.image} />
                ))}
          </div>
          <div className="text-white flex items-center justify-end gap-1 cursor-pointer mt-6">
            <Link href="whats-on/special-offers" className="font-bold text-2xl">
              See More
            </Link>
            <IoIosArrowForward className="text-2xl" />
          </div>
        </Wrapper>
      </div>
    </div>
  );
};

export default HomeOffers;
