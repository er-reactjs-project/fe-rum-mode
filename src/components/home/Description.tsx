import React from 'react';
import { useAppSelector } from '@/hooks/store';
import Skeleton from '../shared/Skeleton';

const HomeDescription = () => {
  const { loading, landingPageData } = useAppSelector((state) => state.landing);

  return (
    <div>
      <div className="py-8 bg-[--primary] flex items-center justify-center gap-6">
        {loading && (
          <Skeleton loading={loading} type="rectangle" className="!w-1/4 h-8" />
        )}

        {!loading && landingPageData && (
          <>
            <hr className="w-[100px]" />
            <div dangerouslySetInnerHTML={{ __html: landingPageData.header }} />
            <hr className="w-[100px]" />
          </>
        )}
      </div>
      <div className="w-full aspect-[36/5]">
        {loading && <Skeleton loading={loading} type="image" fullHeight={true} />}
        {!loading && landingPageData && (
          <div
            style={{
              backgroundImage: `url(${landingPageData.superiority_image})`
            }}
            className="w-full h-full bg-cover bg-center"
          ></div>
        )}
      </div>
    </div>
  );
};

export default HomeDescription;
