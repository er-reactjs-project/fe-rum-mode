import React from 'react';
import { useAppSelector } from '@/hooks/store';
import Skeleton from '../shared/Skeleton';

import HomeSlider from './Slider';

const HomeEvent = () => {
  const { loading, landingPageData } = useAppSelector((state) => state.landing);

  return (
    <div id="events">
      <div className="py-8 bg-[--primary] flex items-center justify-center gap-6">
        {loading && (
          <Skeleton loading={loading} type="rectangle" className="!w-1/4 h-8" />
        )}

        {!loading && landingPageData && (
          <>
            <hr className="w-[100px]" />
            <div
              dangerouslySetInnerHTML={{ __html: landingPageData.event_title }}
            />
            <hr className="w-[100px]" />
          </>
        )}
      </div>
      <div className="lg:h-[80vh] 2xl:h-[80vh] min-[1700px]:h-[90vh] min-[1800px]:h-[85vh] min-[2000px]:h-[80vh] min-[2100px]:h-[75vh] min-[2400px]:h-[65vh] min-[2700px]:h-[60vh] min-[3200px]:h-[48vh] min-[4000px]:h-[40vh] min-[4800px]:h-[33vh] min-[5400px]:h-[30vh] min-[6400px]:h-[25vh] flex flex-col">
        {loading && (
          <div className="flex-grow">
            <Skeleton loading={loading} type="image" fullHeight={true} />
          </div>
        )}

        {!loading && landingPageData && (
          <HomeSlider sliderData={landingPageData.event_images} />
        )}
      </div>
    </div>
  );
};

export default HomeEvent;
