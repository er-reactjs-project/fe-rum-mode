import React, { useState } from 'react';

import { useKeenSlider } from 'keen-slider/react';

import {
  IoIosArrowDropleftCircle,
  IoIosArrowDroprightCircle
} from 'react-icons/io';
import IframeVideo from '../shared/IframeVideo';
import { KeenSliderAssetsTypes } from '@/store/redux/landing-page/types';

interface SliderProps {
  sliderData: KeenSliderAssetsTypes[];
}

const HomeSlider = ({ sliderData = [] }: SliderProps) => {
  const [loaded, setLoaded] = useState(false);

  const [sliderRef, sliderInstanceRef] = useKeenSlider(
    {
      loop: true,
      slides: sliderData.length,
      created: () => setLoaded(true)
    },
    [
      (slider) => {
        let timeout: ReturnType<typeof setTimeout>;
        let mouseOver = false;
        function clearNextTimeout() {
          clearTimeout(timeout);
        }
        function nextTimeout() {
          clearTimeout(timeout);
          if (mouseOver) return;

          const data = sliderData[slider.track.details.rel];
          let timeoutTime = 3000;
          if (['video', 'ext-yt'].includes(data.asset_type)) {
            timeoutTime = 60000;
          }

          timeout = setTimeout(() => {
            slider.next();
          }, timeoutTime);
        }
        slider.on('created', () => {
          slider.container.addEventListener('mouseover', () => {
            mouseOver = true;
            clearNextTimeout();
          });
          slider.container.addEventListener('mouseout', () => {
            mouseOver = false;
            nextTimeout();
          });
          nextTimeout();
        });
        slider.on('dragStarted', clearNextTimeout);
        slider.on('animationEnded', nextTimeout);
        slider.on('updated', nextTimeout);
      }
    ]
  );

  return (
    <>
      <>
        <div className="relative flex-grow">
          <div ref={sliderRef} className="keen-slider h-full">
            {sliderData?.map((item: KeenSliderAssetsTypes, idx: number) =>
              item.asset_type === 'image' ? (
                <div
                  key={idx}
                  className="keen-slider__slide flex justify-center items-center 2xs:aspect-[16/7] lg:h-[20vh]"
                >
                  <div
                    style={{ backgroundImage: `url(${item.asset})` }}
                    className={`w-full h-full bg-cover bg-center`}
                  />
                </div>
              ) : item.asset_type === 'ext-yt' ? (
                <div
                  key={idx}
                  className="keen-slider__slide flex justify-center items-center 2xs:aspect-[16/7] lg:h-[20vh]"
                >
                  <IframeVideo src={item.asset} />
                </div>
              ) : (
                <div
                  key={idx}
                  className="keen-slider__slide flex justify-center items-center 2xs:aspect-[16/7] lg:h-[20vh]"
                >
                  <video
                    preload="auto"
                    autoPlay
                    loop
                    muted
                    className="absolute top-0 bottom-0 left-0 right-0 w-full object-cover"
                  >
                    <source className="w-full" src={item.asset} />
                  </video>
                </div>
              )
            )}
          </div>
          {loaded && sliderInstanceRef.current && (
            <>
              <IoIosArrowDropleftCircle
                onClick={() => sliderInstanceRef.current?.prev()}
                className="absolute top-[45%] text-[40px] left-1"
              />
              <IoIosArrowDroprightCircle
                onClick={() => sliderInstanceRef.current?.next()}
                className="absolute top-[45%] text-[40px] left-auto right-1"
              />
            </>
          )}
        </div>
      </>
    </>
  );
};

export default HomeSlider;
