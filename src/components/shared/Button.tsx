import React from 'react';

interface Props
  extends React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > {
  icon?: React.ReactNode;
  iconPosition?: 'right' | 'left';
  color?: 'primary' | 'secondary' | 'secondary-bordered';
}

const Button = (props: Props) => {
  const {
    children,
    className,
    icon,
    iconPosition = 'left',
    color = 'primary',
    ...rest
  } = props;
  return (
    <button
      className={`flex items-center py-3 px-[34px] rounded-[100px] ${
        color === 'primary'
          ? 'text-white bg-[--primary]'
          : color === 'secondary'
            ? 'bg-white text-[--primary]'
            : color === 'secondary-bordered'
              ? 'bg-white text-[--primary] border-[2px] border-[--primary]'
              : ''
      } ${className}`}
      {...rest}
    >
      {iconPosition === 'left' && icon}
      {children}
      {iconPosition === 'right' && icon}
    </button>
  );
};

export default Button;
