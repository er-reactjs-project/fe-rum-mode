import React from 'react';
import Head from 'next/head';

type MetaHeadType = {
  title: string;
};

export default function MetaHead({ title }: MetaHeadType) {
  return (
    <Head>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>{title}</title>
      <link rel="shortcut icon" type="image" href="/favicon.svg" />
    </Head>
  );
}
