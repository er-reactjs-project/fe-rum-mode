/* eslint-disable @next/next/no-img-element */
import React from 'react';

// import Image from 'next/image';
import Link from 'next/link';

import { SiWhatsapp, SiInstagram, SiTiktok } from 'react-icons/si';
import { IoLocationOutline } from 'react-icons/io5';

import { useAppSelector } from '@/hooks/store';

const Footer = () => {
  const { landingPageData } = useAppSelector((state) => state.landing);
  const { personalization } = useAppSelector((state) => state.personalization);

  return (
    <footer className="grid xl:grid-cols-2 lg:grid-cols-3 p-6 items-start">
      <div className="flex justify-between items-start gap-[44px] text-[--primary]">
        <div id="information" className="flex flex-col items-start gap-4">
          <p className="font-bold text-lg">Information</p>
          <div className="flex flex-col items-start gap-[6px]">
            <Link href="/information/about-us">About Us</Link>
            <Link href="/information/help-and-rules">Help & Rules</Link>
            <Link href="/information/maps">Maps</Link>
            <Link href="/information/careers">Careers</Link>
          </div>
        </div>

        <div id="whats-on" className="flex flex-col items-start gap-4">
          <p className="font-bold text-lg">What&apos;s on</p>
          <div className="flex flex-col items-start gap-[6px]">
            <Link href="/#events">Event</Link>
            <Link href="/whats-on/special-offers">Special Offers</Link>
          </div>
        </div>

        <div id="getting-here" className="flex flex-col items-start gap-4">
          <p className="font-bold text-lg">Getting Here</p>
          <div className="flex flex-col items-start gap-[6px]">
            <Link href="/getting-here/contact-us">Contact Us</Link>
            <Link href="/getting-here/faq">FAQ</Link>
            <Link href="/getting-here/access">Access</Link>
          </div>
        </div>

        <div id="map" className="flex-grow hidden xl:block">
          <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3961.063986311253!2d107.59710637354351!3d-6.882938667346926!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e695940813ad%3A0x55102548df571383!2sRumah%20Mode%20Factory%20Outlet!5e0!3m2!1sid!2sid!4v1703732807836!5m2!1sid!2sid"
            width="100%"
            height="auto"
            style={{ border: 0 }}
            allowFullScreen={false}
            loading="lazy"
            referrerPolicy="no-referrer-when-downgrade"
          ></iframe>
        </div>
      </div>

      <div
        id="map"
        className="flex-grow flex lg:aspect-video 2xs:aspect-square mt-6 mb-6 lg:mt-0 lg:ml-10 xl:hidden"
      >
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3961.063986311253!2d107.59710637354351!3d-6.882938667346926!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e695940813ad%3A0x55102548df571383!2sRumah%20Mode%20Factory%20Outlet!5e0!3m2!1sid!2sid!4v1703732807836!5m2!1sid!2sid"
          width="100%"
          height="auto"
          style={{ border: 0 }}
          allowFullScreen={false}
          loading="lazy"
          referrerPolicy="no-referrer-when-downgrade"
        ></iframe>
      </div>

      <div className="flex xl:justify-between lg:justify-end sm:justify-between 2xs:justify-center 2xs:flex-wrap lg:pl-[44px] lg:mt-0 md:mt-5">
        <div className="flex md:gap-4 2xs:gap-6 2xs:mb-5 xl:w-auto lg:w-full sm:w-1/2 2xs:w-full lg:justify-end sm:justify-start 2xs:justify-center">
          <Link
            href={landingPageData?.link_wa ? landingPageData?.link_wa : '/'}
            target="_blank"
            className="w-10 h-10 lg:w-12 lg:h-12 rounded-full bg-[--primary] flex justify-center items-center text-white text-lg lg:text-2xl shadow-white shadow-sm"
          >
            <SiWhatsapp />
          </Link>
          <Link
            href={landingPageData?.link_ig ? landingPageData?.link_ig : '/'}
            target="_blank"
            className="w-10 h-10 lg:w-12 lg:h-12 rounded-full bg-[--primary] flex justify-center items-center text-white text-lg lg:text-2xl shadow-white shadow-sm"
          >
            <SiInstagram />
          </Link>
          <Link
            href={
              landingPageData?.link_tiktok ? landingPageData?.link_tiktok : '/'
            }
            target="_blank"
            className="w-10 h-10 lg:w-12 lg:h-12 rounded-full bg-[--primary] flex justify-center items-center text-white text-lg lg:text-2xl shadow-white shadow-sm"
          >
            <SiTiktok />
          </Link>
          <Link
            href={
              landingPageData?.link_location
                ? landingPageData?.link_location
                : '/'
            }
            target="_blank"
            className="w-10 h-10 lg:w-12 lg:h-12 rounded-full bg-[--primary] flex justify-center items-center text-white text-lg lg:text-2xl shadow-white shadow-sm"
          >
            <IoLocationOutline />
          </Link>
        </div>

        <div className="flex flex-col items-end">
          <img
            src={personalization? personalization.footer_logo : "/assets/images/logo_blue.svg"}
            alt="footer logo"
            width={233}
            height={108}
          />
          <p className="text-[--primary]">© COPYRIGHT 2020</p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
