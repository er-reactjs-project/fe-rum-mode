/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useRef, useState } from 'react';

interface Props {
    src: string;
}

const IframeVideo = ({ src }: Props) => {
  const [load, setLoad] = useState(false);
  const videoRef: any = useRef();

  useEffect(() => {
    const observer = new IntersectionObserver((entries) => {
      if (entries[0].isIntersecting) {
        setLoad(true);
        observer.disconnect();
      }
    });

    observer.observe(videoRef.current);

    return () => {
      if (videoRef.current) {
        observer.unobserve(videoRef.current);
      }
    };
  }, []);

  return (
    <div ref={videoRef} className='video-yt-wrapper'>
      {load ? (
        <iframe
        width={"100%"}
        height={"100%"}
        className="absolute top-0 bottom-0 left-0 right-0 w-full h-full object-cover"
        title="YouTube video player"
        src={src}
        allowFullScreen
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
      ></iframe>
      ) : (
        <div>Loading...</div>
      )}
    </div>
  );
};

export default IframeVideo;
