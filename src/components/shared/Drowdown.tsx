import React from 'react';

import Link from 'next/link';
import { Menu, Transition } from '@headlessui/react';

import { IoIosArrowDown } from 'react-icons/io';

interface Child {
  text: string;
  href?: string;
}
interface Props {
  title: string;
  isLink?: boolean;
  child: Child[];
}

const Dropdown = (props: Props) => {
  const { title, child, isLink = false } = props;

  return (
    <Menu>
      {({ open }) => (
        <>
          <Menu.Button
            className={`flex flex-row gap-[10px] items-center text-[--primary] text-lg py-3 ${
              open ? 'text-secondary' : ''
            }`}
          >
            {title}
            <IoIosArrowDown
              className={`${
                open ? 'rotate-180' : 'rotate-0'
              } duration-100 transform ease-out`}
            />
          </Menu.Button>
          <Transition
            show={open}
            enter="transition duration-100 ease-out"
            enterFrom="transform scale-95 opacity-0"
            enterTo="transform scale-100 opacity-100"
            leave="transition duration-75 ease-out"
            leaveFrom="transform scale-100 opacity-100"
            leaveTo="transform scale-95 opacity-0"
            className="absolute bg-white opacity-80 p-4 z-50"
          >
            <Menu.Items static className="flex flex-col">
              {child.map((item) => (
                <Menu.Item key={item.text}>
                  {isLink ? (
                    <Link
                      href={item.href!}
                      className="text-lg text-[--primary] hover:text-secondary"
                    >
                      {item.text}
                    </Link>
                  ) : (
                    <p className="text-lg text-[--primary] hover:text-secondary">
                      {item.text}
                    </p>
                  )}
                </Menu.Item>
              ))}
            </Menu.Items>
          </Transition>
        </>
      )}
    </Menu>
  );
};

export default Dropdown;
