import React from 'react';

interface WrapperProps {
  children?: React.ReactNode;
  className?: String;
  outerClassName?: String;
  backgroundColor?: String;
}

export default function Wrapper({
  children,
  className,
  outerClassName,
  backgroundColor = 'bg-transparent'
}: WrapperProps) {
  return (
    <>
      <div
        className={`flex justify-center items-center flex-grow w-full ${outerClassName} ${backgroundColor}`}
      >
        <div
          className={`px-4 md:px-[36px] w-full md:w-[85%] lg:w-full max-w-[1200px] ${className}`}
        >
          {children}
        </div>
      </div>
    </>
  );
}
