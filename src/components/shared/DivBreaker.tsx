const DivBreaker = () => {
  return (
    <div className="h-[204px] xs:h-[212px] sm:h-[210px] md:h-[170px] lg:h-[160px] min-h-[204px] xs:min-h-[212px] sm:min-h-[210px] md:min-h-[170px] lg:min-h-[160px]"></div>
  );
};

export default DivBreaker;
