import React, { ComponentPropsWithRef } from 'react';

// Use the div element props
type DivProps = ComponentPropsWithRef<'div'>;

interface SkeletonParagraphProps {
  rows?: number;
  width?: number | string;
}

interface SkeletonProps extends DivProps {
  children?: React.ReactNode;
  loading: boolean;
  type?: 'paragraph' | 'rectangle' | 'image' 
  paragraph?: SkeletonParagraphProps;
  fullHeight?: boolean;
}

const Skeleton = (props: SkeletonProps) => {
  const {
    children,
    className = '',
    loading = true,
    type,
    paragraph = { rows: 3 },
    fullHeight = false
  } = props;

  const Paragraph = () => {
    return (
      <div className="w-full flex flex-col gap-2">
        <div className="h-4 w-1/4 bg-gray-300 rounded"></div>
        {Array.apply(0, Array(paragraph.rows)).map(function (_, i) {
          const rows = paragraph.rows!;
          return rows > 2 && i == rows - 1 ? (
            <div key={i} className="h-4 w-1/2 bg-gray-300 rounded"></div>
          ) : (
            <div key={i} className="h-4 bg-gray-300 rounded"></div>
          );
        })}
      </div>
    );
  };

  const Rectangle = () => {
    return (
      <div className="w-full flex flex-col gap-2 h-full">
        <div className="h-full bg-gray-300 rounded"></div>
      </div>
    );
  };

  const SkeletonImage = () => {
    return (
      <div className="w-full flex flex-col gap-2 h-full">
        <div className="h-full bg-gray-300 rounded grid place-items-center">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={2}
            stroke="currentColor"
            className="h-12 w-12 text-gray-500"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="m2.25 15.75 5.159-5.159a2.25 2.25 0 0 1 3.182 0l5.159 5.159m-1.5-1.5 1.409-1.409a2.25 2.25 0 0 1 3.182 0l2.909 2.909m-18 3.75h16.5a1.5 1.5 0 0 0 1.5-1.5V6a1.5 1.5 0 0 0-1.5-1.5H3.75A1.5 1.5 0 0 0 2.25 6v12a1.5 1.5 0 0 0 1.5 1.5Zm10.5-11.25h.008v.008h-.008V8.25Zm.375 0a.375.375 0 1 1-.75 0 .375.375 0 0 1 .75 0Z"
            />
          </svg>
        </div>
      </div>
    );
  };

  const getClassName = () => {
    var defaultClass = 'w-full';
    if (loading) {
      defaultClass += ' animate-pulse';
    }

    if (fullHeight) {
      defaultClass += ' h-full';
    }

    defaultClass += ` ${className}`;
    return defaultClass;
  };

  const renderSkeleton = () => {
    switch(type) {
        case 'image':
            return <SkeletonImage />
        case 'rectangle':
            return <Rectangle />
        case 'paragraph':
            return <Paragraph />
        default:
            return <Paragraph />
    }
  }

  return (
    <div {...props} className={getClassName()}>
      {loading ? renderSkeleton() : children}
    </div>
  );
};

export default Skeleton;

// type Props = {
//   className?: string;
//   size: number;
// };

// const SkeletonCircle = (props: Props) => {
//   const className = props.className ?? 'rounded-full flex-shrink-0 bg-gray-300';
//   return (
//     <div
//       className={className}
//       style={{ height: props.size, width: props.size }}
//     />
//   );
// };
