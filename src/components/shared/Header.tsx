/* eslint-disable @next/next/no-img-element */
import React, { useState } from 'react';

// import Image from 'next/image';

import { FaRegUserCircle } from 'react-icons/fa';
import Link from 'next/link';
import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { setLanguage } from '@/store/redux/global';

const Header = () => {
  const dispatch = useAppDispatch();
  const { lang } = useAppSelector((state) => state.global);
  const { personalization } = useAppSelector((state) => state.personalization);
  const [openLanguageFilter, setOpenLanguageFilter] = useState<boolean>(false);

  return (
    <header className="px-[34px] py-5 text-white flex justify-between items-start bg-[--primary]">
      <Link href={'/'} className="">
        <img
          src={personalization? personalization.topbar_logo : "/assets/images/logo_white.svg"}
          alt="topbar logo"
          width={160}
          height={0}
        />
      </Link>
      <div className="flex flex-col gap-4 items-end max-[529px]:w-full">
        <p className="text-sm max-w-[204px] md:max-w-none">
          Open Hours :{' '}
          {personalization
            ? personalization.open_hour
            : 'Senin - Kamis 09.30 - 20.30 | Jumat & Minggu 09.30 - 21.00 | Sabtu 09.30 - 21.30'}
        </p>
        <div className="flex items-center gap-[34px] max-[529px]:w-full max-[529px]:justify-between">
          <div className="w-16 h-full flex items-center ">
            <button
              onClick={() => setOpenLanguageFilter(!openLanguageFilter)}
              className="w-full"
            >
              <div className="w-full flex flex-row gap-2">
                {lang === 'id' ? (
                  <>
                    <div className="w-6 h-6 bg-cover bg-indonesian-flag" /> IDN
                  </>
                ) : (
                  <>
                    <div className="w-6 h-6 bg-cover bg-english-flag" /> ENG
                  </>
                )}
              </div>
            </button>
            {openLanguageFilter && (
              <div className="flex flex-col absolute top-24 md:top-24 sm:top-32 xs:top-[134px] z-[999999999] overflow-hidden rounded-md shadow-xl mt-2 border-[1px] border-white">
                <button
                  onClick={() => {
                    dispatch(setLanguage('id'));
                    setOpenLanguageFilter(false);
                  }}
                  className="flex gap-2 item-center w-[144px] px-4 py-2 bg-neutral-02-background text-black hover:text-white text-left hover:bg-[--primary]"
                >
                  <div className="w-6 h-6 bg-cover bg-indonesian-flag" />{' '}
                  Indonesian
                </button>
                <button
                  onClick={() => {
                    dispatch(setLanguage('en'));
                    setOpenLanguageFilter(false);
                  }}
                  className="flex gap-2 item-center w-[144px] px-4 py-2 bg-neutral-02-background text-black hover:text-white text-left hover:bg-[--primary]"
                >
                  <div className="w-6 h-6 bg-cover bg-english-flag" /> English
                </button>
              </div>
            )}
          </div>
          <Link href="/blog">
            <p className="text-lg">Blog</p>
          </Link>
          <div className="flex items-center gap-4">
            <Link href="/membership">
              <p className="text-lg">Membership</p>
            </Link>
            <Link href="/profile">
              <FaRegUserCircle className="text-[40px]" />
            </Link>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
