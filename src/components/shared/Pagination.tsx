/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react';

import {
  MdKeyboardDoubleArrowLeft,
  MdKeyboardDoubleArrowRight,
  MdKeyboardArrowLeft,
  MdKeyboardArrowRight
} from 'react-icons/md';

interface Props {
  current: number;
  totalData: number;
  totalPage: number;
  limit: number;
  onClick: (next: number) => void;
  enableGoToPage?: boolean;
}

const Pagination = (props: Props) => {
  const {
    current,
    totalData,
    totalPage,
    limit,
    onClick,
    enableGoToPage = false
  } = props;
  const [goToPage, setGoToPage] = useState<number>();

  return (
    <div className="flex flex-row gap-12 items-center">
      <div className="flex flex-row gap-2">
        <button onClick={() => onClick(1)}>
          <MdKeyboardDoubleArrowLeft className="size-6" />
        </button>
        <button onClick={() => onClick(current - 1)} disabled={current === 1}>
          <MdKeyboardArrowLeft
            className={`size-6 ${current === 1 ? 'text-gray-400' : ''}`}
          />
        </button>

        <div>
          {[...Array(totalPage).keys()].map((item) => (
            <button
              key={item}
              className={`text-2xl size-6 ${
                current === item + 1 ? 'text-black' : 'text-gray-400'
              }`}
              onClick={() => onClick(item + 1)}
            >
              {item + 1}
            </button>
          ))}
        </div>

        <button
          onClick={() => onClick(current + 1)}
          disabled={current === totalPage}
        >
          <MdKeyboardArrowRight
            className={`size-6 ${current === totalPage ? 'text-gray-400' : ''}`}
          />
        </button>
        <button onClick={() => onClick(totalPage)}>
          <MdKeyboardDoubleArrowRight className="size-6" />
        </button>
      </div>
      {enableGoToPage && (
        <div className="flex flex-row gap-3 items-center">
          <div className="text-lg font-semibold text-neutral-08">
            Go to page
          </div>
          <input
            className="py-3 px-2 border-neutral-08 rounded-lg w-[3.75rem] h-7 border-[1px]"
            type="number"
            min={1}
            max={totalPage}
            value={goToPage}
            onChange={(e) => {
              let value = parseInt(e.target.value)
              if (value < 1) value = 1
              if (value > totalPage) value = totalPage
              setGoToPage(value)
            }}
          />
          <button onClick={() => onClick(goToPage ?? 1)}>
            <div className="flex flex-row gap-0 items-center">
              <div className="text-lg font-semibold text-neutral-08">Go</div>
              <MdKeyboardArrowRight className="size-6" />
            </div>
          </button>
        </div>
      )}
    </div>
  );
};

export default Pagination;
