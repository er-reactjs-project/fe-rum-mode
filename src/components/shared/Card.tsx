import React from 'react';

interface Props {
  image: string;
  title: string;
  subtitle?: string[];
  theme?: 'blue' | 'white';
  width?: number;
  height?: number;
  textCenter?: boolean;
}

const Card = (props: Props) => {
  const {
    image,
    title,
    subtitle,
    theme = 'blue',
    width = 500,
    // height = 500,
    textCenter = true
  } = props;

  return (
    <div className="flex flex-col justify-center self-stretch w-full">
      <div
        className={`bg-cover w-full lg:w-[${width}] aspect-square`}
        style={{
          backgroundImage: `url(${image})`
        }}
      />
      <div
        className={`px-3 py-6 w-full max-w-[${width}] flex flex-col gap-2 flex-grow ${
          textCenter ? 'items-center' : ''
        } ${theme === 'blue' ? 'bg-[--primary]' : 'bg-white'}`}
      >
        <p
          className={`font-bold text-xl tracking-[2.4px] ${
            textCenter ? 'text-center' : ''
          } ${theme === 'blue' ? 'text-white' : 'text-[--primary]'}`}
        >
          {title}
        </p>
        {subtitle?.map((item) => (
          <div
            key={item}
            className="line-clamp-[2]"
            dangerouslySetInnerHTML={{
              __html: item
            }}
          />
        ))}
      </div>
    </div>
  );
};

export default Card;
