import React from 'react';

interface Props {
  placeHolder?: string;
  value?: string;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
  iconPosition?: 'right' | 'left';
}

const SearchInput = (props: Props) => {
  const {
    placeHolder = 'Search',
    value,
    onChange,
    iconPosition = 'left'
  } = props;

  return (
    <div className="relative">
      <span
        className={`absolute inset-y-0 flex items-center ${
          iconPosition === 'left' ? 'left-0 pl-2' : 'right-0 pr-2'
        }`}
      >
        <button
          type="submit"
          className="p-1 focus:outline-none focus:shadow-outline"
        >
          <svg
            fill="none"
            stroke="currentColor"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            viewBox="0 0 24 24"
            className="w-6 h-6 text-[--primary]"
          >
            <path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
          </svg>
        </button>
      </span>
      <input
        type="search"
        className="py-2 text-sm rounded-full pl-10 w-full focus:outline-none"
        placeholder={placeHolder}
        value={value}
        onChange={onChange}
      />
    </div>
  );
};

export default SearchInput;
