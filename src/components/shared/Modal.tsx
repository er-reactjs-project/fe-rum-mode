import React from 'react';

interface ModalProps {
  children: React.ReactNode;
  open: boolean;
  onClose: () => void;
  className?: string;
}

const Modal = (props: ModalProps) => {
  const { children,  open, onClose , className} = props;

  return (
    // backdrop
    <div
      onClick={onClose}
      className={`
            fixed inset-0 flex justify-center items-center transition-colors
            ${open ? 'visible bg-black/40' : 'invisible'}
          `}
    >
      {/* modal */}
      <div
        onClick={(e) => e.stopPropagation()}
        className={`
              bg-white rounded-xl shadow p-6 transition-all
              ${open ? 'scale-100 opacity-100' : 'scale-125 opacity-0'}
              ${className ?? ''}
            `}
      >
        <button
          onClick={onClose}
          className="absolute top-2 right-2 p-1 rounded-lg text-gray-400 bg-white hover:bg-gray-50 hover:text-gray-600"
        >
          <span className="text-black opacity-7 h-6 w-6 text-xl block py-0 rounded-full">
            x
          </span>
        </button>
        {children}
      </div>
    </div>
  );
};

export default Modal;
