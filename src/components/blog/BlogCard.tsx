import { Blog } from '@/store/redux/blog/types';
import Link from 'next/link';
import { formatDate1 } from '@/libs/helper';

interface BlogCardComponentProps {
  data: Blog;
}

export default function BlogCardComponent(props: BlogCardComponentProps) {
  const { data } = props;

  return (
    <>
      <Link href={`/blog/${data.id}`}>
        <div
          className="bg-cover w-full aspect-[291/194] rounded-xl"
          style={{ backgroundImage: `url(${data.banner})` }}
        />

        <div className="p-4 flex flex-col justify-between gap-3">
          {/* <div className="text-2xl text-[--primary] font-bold">
            {data.title}
          </div> */}
          <div
            className="line-clamp-[1]"
            dangerouslySetInnerHTML={{
              __html: data.title
            }}
          />
          <div
            className="line-clamp-2"
            dangerouslySetInnerHTML={{
              __html: data.content
            }}
          />
          <div className="flex items-center gap-2 text-sm text-neutral-05">
            <div>{formatDate1(data.created_at!)}</div>
            <div className="w-1 h-1 rounded-full bg-neutral-05" />
            <div>{data.read_time_minute} mins read</div>
          </div>
        </div>
      </Link>
    </>
  );
}
