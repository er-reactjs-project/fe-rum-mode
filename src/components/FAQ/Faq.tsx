import React from 'react';

import Wrapper from '../shared/Wrapper';
import { Disclosure, Transition } from '@headlessui/react';

import { MdKeyboardArrowDown } from 'react-icons/md';

const FaqContent = () => {
  return (
    <Wrapper className="flex flex-col md:flex-row pt-8 pb-12 gap-6 md:gap-[100px]">
      <p className="text-3xl font-bold text-[--primary]">FAQs</p>
      <div className="w-full">
        <p className="text-[--primary] text-2xl font-bold border-b border-[--primary] pb-4">
          Pertanyaan Utama
        </p>
        {[...Array(5).keys()].map((item) => (
          <Disclosure
            key={item}
            as="div"
            className="border-b border-[--primary]"
          >
            {({ open }) => (
              <>
                <Disclosure.Button className="py-4 text-[--primary] w-full flex items-center justify-between text-left">
                  Lorem Ipsum
                  <MdKeyboardArrowDown
                    className={`size-6 duration-100 transform ${
                      open ? 'rotate-180 ' : 'rotate-0'
                    }`}
                  />
                </Disclosure.Button>
                <Transition
                  show={open}
                  enter="transition duration-100 ease-out"
                  enterFrom="transform scale-95 opacity-0"
                  enterTo="transform scale-100 opacity-100"
                  leave="transition duration-75 ease-out"
                  leaveFrom="transform scale-100 opacity-100"
                  leaveTo="transform scale-95 opacity-0"
                >
                  <Disclosure.Panel static className="pb-6">
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    Consequuntur numquam doloremque cupiditate reprehenderit nam
                    illum consectetur non earum ducimus iusto ea harum quisquam
                    ratione dolorum, veritatis qui perferendis aliquid
                    explicabo.
                  </Disclosure.Panel>
                </Transition>
              </>
            )}
          </Disclosure>
        ))}
      </div>
    </Wrapper>
  );
};

export default FaqContent;
