import React from 'react';

import SearchInput from '../shared/SearchInput';

const FaqBanner = () => {
  return (
    <div className="bg-[url(/assets/images/banner/faq_banner.png)] bg-cover bg-no-repeat flex-grow">
      <div className="w-2/5 mx-auto pb-8 flex flex-col justify-end h-full">
        <SearchInput placeHolder="Cari FAQ" iconPosition="right" />
      </div>
    </div>
  );
};

export default FaqBanner;
