/* eslint-disable @next/next/no-img-element */
import React, { useState } from 'react';

import { useKeenSlider } from 'keen-slider/react';

import {
  IoIosArrowDropleftCircle,
  IoIosArrowDroprightCircle
} from 'react-icons/io';
// import Image from 'next/image';
import { DiscoverBanners } from '@/store/redux/discover/types';
import Skeleton from '../shared/Skeleton';

interface Props {
  items: string[] | DiscoverBanners[];
}

const Carousel = (props: Props) => {
  const { items } = props;

  const [currentSlide, setCurrentSlide] = useState(0);
  const [loaded, setLoaded] = useState(false);

  const [sliderRef, instanceRef] = useKeenSlider<HTMLDivElement>({
    loop: true,
    renderMode: 'performance',
    mode: 'free',
    slides: {
      origin: 'center',
      perView: 'auto',
      spacing: 20
    },
    created: () => setLoaded(true),
    slideChanged: (slider) => setCurrentSlide(slider.track.details.rel)
  });

  return (
    <div className="relative pt-12">
      <div ref={sliderRef} className="keen-slider items-center">
        {items.map((item, index) => (
          <div
            key={index}
            className={`keen-slider__slide text-white text-center aspect-[3/4]
            ${
              currentSlide === index
                ? 'xl:w-[30%] xl:min-w-[30%] xl:max-w-[30%] lg:w-[45%] lg:min-w-[45%] lg:max-w-[45%] md:w-[60%] md:min-w-[60%] md:max-w-[60%] sm:w-[80%] sm:min-w-[80%] sm:max-w-[80%] xs:w-[95%] xs:min-w-[95%] xs:max-w-[95%]'
                : 'xl:w-[25%] xl:min-w-[25%] xl:max-w-[25%] lg:w-[45%] lg:min-w-[45%] lg:max-w-[45%] md:w-[60%] md:min-w-[60%] md:max-w-[60%] sm:w-[80%] sm:min-w-[80%] sm:max-w-[80%] xs:w-[95%] xs:min-w-[95%] xs:max-w-[95%]'
            }`}
          >
            {loaded ? (
              <img
                src={typeof item == 'string' ? item : item.banner}
                alt={`img-${index + 1}`}
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: '100%' }}
              />
            ) : (
              <Skeleton loading={!loaded} type="image" fullHeight={true} />
            )}
          </div>
        ))}
      </div>
      {loaded && instanceRef.current && (
        <>
          <IoIosArrowDropleftCircle
            onClick={() => instanceRef.current?.prev()}
            className="absolute text-white top-1/2 text-[40px] left-1"
          />
          <IoIosArrowDroprightCircle
            onClick={() => instanceRef.current?.next()}
            className="absolute text-white top-1/2 text-[40px] left-auto right-1"
          />
        </>
      )}
    </div>
  );
};

export default Carousel;
