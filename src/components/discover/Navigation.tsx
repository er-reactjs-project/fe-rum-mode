import React, { Fragment } from 'react';

import Link from 'next/link';

const menuItem = [
  { text: 'store', href: 'store' },
  { text: 'food court', href: 'food-court' },
  { text: 'kids playground', href: 'kids-playground' },
  { text: 'clinic', href: 'clinic' },
  { text: 'ekakarya', href: 'ekakarya' },
  { text: 'gallery', href: 'gallery' },
  { text: 'facilities', href: 'facilities' }
];

interface Props {
  active:
    | 'store'
    | 'food court'
    | 'kids playground'
    | 'clinic'
    | 'ekakarya'
    | 'gallery'
    | 'facilities';
}

const Navigation = (props: Props) => {
  const { active } = props;

  return (
    <div className="pt-6 pb-12">
      <div className="grid grid-cols-2 md:grid-cols-4 lg:grid-cols-7 justify-center">
        {menuItem.map((item) => (
          <Fragment key={item.text}>
            {active === item.text ? (
              <p className="text-[--primary] font-bold border-t-4 border-[--primary] text-lg px-4 pt-6 pb-3 text-center">
                {item.text.toUpperCase()}
              </p>
            ) : (
              <Link
                href={`/discover/${item.href}`}
                className={`text-[--primary] text-lg px-4 pt-6 pb-3 text-center  ${
                  active === item.text
                    ? 'font-bold border-t-4 border-[--primary]'
                    : ''
                }`}
              >
                {item.text.toUpperCase()}
              </Link>
            )}
          </Fragment>
        ))}
      </div>
    </div>
  );
};

export default Navigation;
