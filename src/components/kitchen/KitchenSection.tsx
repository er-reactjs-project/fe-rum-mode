/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import Wrapper from '../shared/Wrapper';

import { IoIosArrowForward } from 'react-icons/io';
import Skeleton from '../shared/Skeleton';
import { KitchenStore } from '@/store/redux/kitchen/types';
import StoreCard from './StoreCard';
import { cleanQueryParam } from '@/libs/helper';

interface KitchenSectionProps {
  loading: boolean;
  id: string;
  title: string;
  category: string;
  data: KitchenStore[];
}

const KitchenSection = (props: KitchenSectionProps) => {
  const { loading, id, title, category, data } = props;
  const [pageSize] = useState<number>(3);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [pageLength, setPageLength] = useState<number>(1);

  useEffect(() => {
    if (data && data.length > 0) {
      setCurrentPage(1);
      setPageLength(Math.ceil(data.length / pageSize));
    }
  }, [data]);

  const renderItem = () => {
    const startIndex = (currentPage - 1) * pageSize;
    const endIndex =
      data.length < currentPage * pageSize
        ? data.length
        : currentPage * pageSize;

    return data
      .slice(startIndex, endIndex)
      .map((item, idx) => (
        <StoreCard category={category} key={idx} item={item} />
      ));
  };

  return (
    <div id={id} className="flex pt-8 pb-4 flex-col items-start gap-6">
      <div className="flex py-[10px] md:pl-[50px] items-center w-full md:w-[450px] bg-[--primary]">
        <p className="font-bold text-3xl text-white">{title.toUpperCase()}</p>
      </div>
      <Wrapper className="mt-5 flex gap-2 self-stretch flex-col justify-center">
        <div className="grid md:grid-cols-2 lg:grid-cols-3 gap-5 md:gap-12">
          {loading &&
            [...Array(pageSize)].map((_, i) => (
              <div key={i} className="flex flex-col items-start">
                <Skeleton
                  loading={loading}
                  type="image"
                  className="w-[300px] h-[400px]"
                />
                <div className="flex md:w-[300px] flex-col py-4 gap-3">
                  <Skeleton loading={loading} />
                </div>
              </div>
            ))}
          {!loading && data && renderItem()}
        </div>

        {!loading && data && (
          <div className="px-[70px] flex justify-end items-start gap-4 self-stretch">
            {[...Array(pageLength).keys()].map((item) => (
              <button
                key={item + 1}
                className={`rounded-full size-11 text-white ${
                  currentPage === item + 1 ? 'bg-[--primary]' : 'bg-neutral-03'
                }`}
                onClick={() => setCurrentPage(item + 1)}
              >
                {item + 1}
              </button>
            ))}

            <Link
              href={`/kitchen/store?category=${cleanQueryParam(category)}`}
              className="rounded-3xl h-11 flex items-center gap-2 px-4 py-3 bg-[--primary] text-white"
            >
              <p className="font-bold text-xl">See All</p>
              <IoIosArrowForward />
            </Link>
          </div>
        )}
      </Wrapper>
    </div>
  );
};

export default KitchenSection;
