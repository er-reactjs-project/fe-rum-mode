/* eslint-disable no-unused-vars */
import { NextRouter, useRouter } from "next/router"
import { useEffect, useState } from "react"

export const useRedirectToAfterLoad = (destination: (r: NextRouter) => Promise<string>) => {
    const router = useRouter()

    const [isRouterReady, setIsRouterReady] = useState(false)
    const [isLoading, setIsLoading] = useState(true)
    const [isRedirecting, setIsRedirecting] = useState(false)
    const [path, setPath] = useState<string>()

    useEffect(() => {
        router.isReady && setIsRouterReady(true);
    }, [router.isReady])

    useEffect(() => {
        if (!isRouterReady) return

        destination(router).then(redirectTo => {
            setPath(redirectTo);
        });
        
        const shouldRedirect = path && router.pathname !== path ? true : false;
        if (shouldRedirect) router.push(path!);

        setIsRedirecting(shouldRedirect);
        setIsLoading(false);
    }, [destination, isRouterReady, router, path])

    return { isLoading, isRedirecting };
}