export const toIDR = (value: number, fractionDigits: number = 2) => {
  return value.toLocaleString('in-ID', { style: 'currency', currency: 'IDR', minimumFractionDigits: fractionDigits })
}

export const formatDate1 = (dateString: string, locale: string = "id-ID") => {
  // 2 March 2024
  return new Date(dateString).toLocaleDateString(locale, { year: 'numeric', month: 'long', day: 'numeric' })
}

export const formatDateISO = (dateString: string) => {
  // 2024-03-20
  return new Date(dateString).toISOString().split("T", 1)[0];
}

export const formatTime1 = (dateTimeString: string) => {
  // 24:00
  const date = new Date(dateTimeString);
  const hour = ("0" + date.getHours()).slice(-2);
  const minute = ("0" + date.getMinutes()).slice(-2);
  return `${hour}:${minute}`
}

export const cleanQueryParam = (value: string) => {
  if (value.includes("&")) {
    value = value.replace("&", "-")
  }
  return value
}

export const arrayRange = (start: number, end: number) => Array.from({ length: (end - start) }, (v, k) => k + start);