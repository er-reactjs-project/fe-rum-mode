export function setToken(token: string) {
  localStorage.setItem('accessToken', token)
}

export function getToken() {
  return localStorage.getItem('accessToken')
}

export function clearToken() {
  return localStorage.removeItem('accessToken')
}
