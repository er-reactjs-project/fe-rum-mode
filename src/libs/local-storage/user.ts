import { User } from "@/store/redux/user/types"

export function setUser(data: User) {
  localStorage.setItem('user', JSON.stringify(data))
}

export function getUser(): string | null {
  const data = localStorage.getItem('user')
  if (data) {
    return JSON.parse(data)
  }
  return data
}

export function clearUser() {
  return localStorage.removeItem('user')
}
