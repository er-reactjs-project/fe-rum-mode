export function setRememberMe(page: string, data: Record<string, any>) {
    localStorage.setItem(`remember-${page}`, JSON.stringify(data))
}

export function getRememberMe(page: string): Record<string, any> | null {
    const data = localStorage.getItem(`remember-${page}`)
    if (data) {
        return JSON.parse(data)
    }
    return null
}

export function clearRememberMe(page: string) {
    return localStorage.removeItem(`remember-${page}`)
}