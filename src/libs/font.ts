import localFont from 'next/font/local';
import { Glory, Playball, Lato  } from 'next/font/google';
// import * as fs from 'fs';

export const glory = Glory({ subsets: ['latin'], variable: '--font-glory' });

export const playBall = Playball({
  subsets: ['latin'],
  variable: '--font-play-ball',
  weight: '400'
});

const lato = Lato({
  subsets: ['latin'], variable: '--font-lato',
  weight: '400'
});

const adobeGothicStd = localFont({ src: '../../public/assets/fonts/Adobe Gothic Std/AdobeGothicStd-Bold.otf' });
const arial = localFont({ src: '../../public/assets/fonts/Arial/arial.ttf' });
const cambria = localFont({ 
  src: [
    { path: '../../public/assets/fonts/Cambria/Cambria-01.ttf' },
    { path: '../../public/assets/fonts/Cambria/CambriaMath-02.ttf' },
  ] 
});
const franklinGothic = localFont({ src: '../../public/assets/fonts/Franklin Gothic/FRAMDCN.otf' });
const magnoliaScript = localFont({ src: '../../public/assets/fonts/Magnolia Script/Magnolia Script.otf' });
const pantomNarrowTrial = localFont({ src: '../../public/assets/fonts/Pantom Narrow Trial/PantonNarrow-Trial-Regular.ttf' });
const raillinc = localFont({ src: '../../public/assets/fonts/Raillinc/Raillinc.otf' });
const skyland = localFont({ src: '../../public/assets/fonts/Skyland/Skyland.otf' });

const fonts: Record<string, any> = {
  'Adobe Gothic Std': adobeGothicStd,
  'Arial': arial,
  'Cambria': cambria,
  'Franklin Gothic': franklinGothic,
  'Glory': glory,
  'Lato': lato,
  'Magnolia Script': magnoliaScript,
  'Pantom Narrow Trial': pantomNarrowTrial,
  'Playball': playBall,
  'Raillinc': raillinc,
  'Skyland': skyland,
}
export default fonts;
