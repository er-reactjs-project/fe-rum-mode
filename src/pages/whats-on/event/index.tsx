import { NextPageWithLayout } from '../../_app';
import MainLayout from '@/layout/MainLayout';

const Event: NextPageWithLayout = () => {
  return (
    <>
      <></>
    </>
  );
};

Event.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default Event;
