/* eslint-disable react-hooks/exhaustive-deps */
import { NextPageWithLayout } from '../../_app';
import MainLayout from '@/layout/MainLayout';

import { useState } from 'react';

import MetaHead from '@/components/shared/MetaHead';
import DivBreaker from '@/components/shared/DivBreaker';
import Card from '@/components/shared/Card';
import Pagination from '@/components/shared/Pagination';
import Wrapper from '@/components/shared/Wrapper';

import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { SpecialOfferService } from '@/store/services';
import { useEffect } from 'react';
import Skeleton from '@/components/shared/Skeleton';

const SpecialOffers: NextPageWithLayout = () => {
  const dispatch = useAppDispatch();
  const { loading, specialOffer, listOffer, paging } = useAppSelector(
    (state) => state.specialOffer
  );
  const { lang } = useAppSelector((state) => state.global);
  const [currentPage, setCurrentPage] = useState(1);
  const pageSize = 8;

  useEffect(() => {
    const payload = { lang: lang };
    dispatch(SpecialOfferService.getSpecialOffer(payload));
  }, [lang]);

  useEffect(() => {
    const payload = {
      limit: pageSize,
      page: currentPage,
      lang: lang
    };
    dispatch(SpecialOfferService.getListOfferItems(payload));
  }, [lang, currentPage]);

  return (
    <>
      <MetaHead title="Offers | Rumah Mode" />
      <DivBreaker />
      <Wrapper className="py-8 flex items-center justify-center gap-6">
        {loading && (
          <Skeleton
            loading={loading}
            type="rectangle"
            className="h-8 !w-1/2 flex items-center"
          />
        )}
        {!loading && specialOffer && (
          <div
            dangerouslySetInnerHTML={{
              __html: specialOffer.title
            }}
          />
        )}
      </Wrapper>
      <Wrapper className="grid md:grid-cols-2 lg:grid-cols-4 py-8 gap-8">
        {loading &&
          [...Array(pageSize)].map((_, i) => (
            <div key={i} className="flex flex-col items-start">
              <Skeleton
                loading={loading}
                type="image"
                className="h-[250px] mb-3"
              />
              <Skeleton loading={loading} />
            </div>
          ))}
        {!loading &&
          listOffer.map((data, i) => (
            <Card
              key={i}
              width={1000}
              title={data.title}
              image={data.image}
              subtitle={[data.description]}
              textCenter={false}
            />
          ))}
      </Wrapper>
      <Wrapper className="flex justify-center items-center">
        <Pagination
          totalData={paging.total_data}
          totalPage={paging.total_page}
          limit={pageSize}
          current={currentPage}
          onClick={setCurrentPage}
        />
      </Wrapper>
    </>
  );
};

SpecialOffers.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default SpecialOffers;
