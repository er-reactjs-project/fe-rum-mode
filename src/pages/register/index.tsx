import { useState, Fragment } from 'react';
import { NextPageWithLayout } from '../_app';
import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { useRouter } from 'next/navigation';
import { AuthService } from '@/store/services';

import MetaHead from '@/components/shared/MetaHead';
import Wrapper from '@/components/shared/Wrapper';
import DivBreaker from '@/components/shared/DivBreaker';
import Button from '@/components/shared/Button';
import Link from 'next/link';
import { useFormik } from 'formik';
import PasswordInput from '@/components/shared/PasswordInput';
import LoadingComponent from '@/components/shared/Loading';
import { Dialog, Transition } from '@headlessui/react';

import { MdOutlineArrowRightAlt } from 'react-icons/md';
import ProtectedLayout from '@/layout/ProtectedLayout';

import CustomDatePicker from '@/components/shared/CustomDatePicker';
import { getRememberMe, setRememberMe } from '@/libs/local-storage/remember';

const Register: NextPageWithLayout = () => {
  const dispatch = useAppDispatch();
  const route = useRouter();
  const { loading } = useAppSelector((state) => state.auth);
  const [remember, setRemember] = useState(false);

  const [isOpen, setIsOpen] = useState(false);
  const [isSuccess, setIsSuccess] = useState(true);
  const [responseMessage, setResponseMessage] = useState('');

  const closeModal = () => setIsOpen(false);
  const openModal = () => setIsOpen(true);

  const getInitialValues = () => {
    let data = {
      fullname: '',
      phone_number: '',
      email: '',
      birth_date: undefined as Date | undefined,
      address: '',
      password: ''
    };

    const saved = getRememberMe('register');
    if (saved) {
      data.fullname = saved.fullname;
      data.phone_number = saved.phone_number;
      data.email = saved.email;
      data.birth_date = saved.birth_date;
      data.address = saved.address;
      data.password = saved.password;
    }
    return data;
  }

  const formik = useFormik({
    initialValues: getInitialValues(),
    onSubmit: async (values) => {
      let newValues: Record<string, any> = {};
      Object.assign(newValues, values);
      newValues['birth_date'] = values.birth_date
        ?.toISOString()
        .split('T', 1)[0];

      dispatch(AuthService.register(newValues)).then((res: any) => {
        if (res?.error) {
          setResponseMessage(res.payload.error);
          setIsSuccess(false);
          openModal();
        } else {
          setIsSuccess(true);
          openModal();
        }
      });
    }
  });

  const handleRememberMe = (e: any) => {
    setRemember(e.target.checked);
    setRememberMe('register', formik.values);
  }

  return (
    <>
      <MetaHead title="Register | Rumah Mode" />
      <div className="flex flex-col">
        <DivBreaker />
        <div className="flex-grow bg-register-banner bg-cover">
          <Wrapper>
            <div className="p-[34px] bg-white rounded flex flex-col max-w-[350px] my-24 items-start gap-2">
              <div className="flex flex-col items-start gap-6 w-full">
                <div className="flex flex-col items-start gap-1">
                  <p className="font-bold text-[--primary] text-2xl">Hallo,</p>
                  <p className="text-[--primary] text-xl">Silakan Daftar</p>
                </div>
                <form onSubmit={formik.handleSubmit} className="w-full">
                  <div className="flex flex-col items-start gap-4 w-full">
                    <label className="flex flex-col items-start gap-1 w-full">
                      <p className="text-[--primary] font-bold text-xl">Nama</p>
                      <input
                        type="text"
                        name="fullname"
                        className="p-4 border-[--primary] rounded w-full border-2"
                        placeholder="Masukkan nama lengkap"
                        value={formik.values.fullname}
                        onChange={formik.handleChange}
                      />
                    </label>
                    <label className="flex flex-col items-start gap-1 w-full">
                      <p className="text-[--primary] font-bold text-xl">
                        Nomor Handphone
                      </p>
                      <input
                        type="text"
                        name="phone_number"
                        className="p-4 border-[--primary] rounded w-full border-2"
                        placeholder="Masukkan nomor handphone"
                        value={formik.values.phone_number}
                        onChange={formik.handleChange}
                      />
                    </label>
                    <label className="flex flex-col items-start gap-1 w-full">
                      <p className="text-[--primary] font-bold text-xl">Email</p>
                      <input
                        type="email"
                        name="email"
                        className="p-4 border-[--primary] rounded w-full border-2"
                        placeholder="Masukkan email"
                        value={formik.values.email}
                        onChange={formik.handleChange}
                      />
                    </label>
                    <label className="flex flex-col items-start gap-1 w-full">
                      <p className="text-[--primary] font-bold text-xl">
                        Tanggal Lahir
                      </p>
                      <div className="custom-date-picker">
                        <CustomDatePicker
                          className="p-4 border-[--primary] rounded w-full border-2"
                          name="birth_date"
                          dateFormat="dd/MM/yyyy"
                          placeholderText="Masukkan Tanggal Lahir"
                          shouldCloseOnSelect={true}
                          showMonthYearDropdown={true}
                          minDate={new Date(1970, 1, 1)}
                          maxDate={new Date()}
                          selected={formik.values.birth_date}
                          onChange={(date) =>
                            formik.setFieldValue('birth_date', date)
                          }
                        />
                      </div>
                    </label>
                    <label className="flex flex-col items-start gap-1 w-full">
                      <p className="text-[--primary] font-bold text-xl">
                        Domisili
                      </p>
                      <textarea
                        name="address"
                        className="p-4 border-[--primary] rounded w-full border-2"
                        placeholder="Masukkan domisili"
                        value={formik.values.address}
                        onChange={formik.handleChange}
                      />
                    </label>
                    <label className="flex flex-col items-start gap-1 w-full">
                      <p className="text-[--primary] font-bold text-xl">
                        Kata Sandi
                      </p>
                      <PasswordInput
                        iconPosition="right"
                        placeHolder="Masukkan kata sandi"
                        value={formik.values.password}
                        onChange={formik.handleChange}
                      />
                    </label>
                    <div className="flex justify-between items-center w-full">
                      <label className="flex flex-row items-center gap-2">
                        <input
                          type="checkbox"
                          checked={remember}
                          onChange={handleRememberMe}
                        />
                        <p className="text-[--primary] font-semibold text-lg">
                          Ingat Saya
                        </p>
                      </label>
                      <Link
                        href={'/forgot-password'}
                        className="text-[--primary] text-lg"
                      >
                        Lupa kata sandi?
                      </Link>
                    </div>
                    <Button
                      type="submit"
                      className="rounded-none flex-col w-full text-center"
                    >
                      {loading ? <LoadingComponent /> : 'Register'}
                    </Button>
                  </div>
                </form>
              </div>
            </div>
          </Wrapper>
        </div>
      </div>
      <div className="bg-[--primary] pt-8 pb-12 px-6 flex flex-col gap-3 items-center">
        <p className="font-bold text-2xl text-white">
          LIHAT KEUNTUNGAN SETELAH MENJADI MEMBER KAMI!
        </p>
        <Button className="!rounded py-3 px-6 font-bold bg-white ">
          <Link
            href="/membership"
            className="text-[--primary] font-bold text-xl"
          >
            CEK SEKARANG
          </Link>
        </Button>
      </div>

      {/* Dialogue */}
      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={() => closeModal()}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black/25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex min-h-full items-center justify-center p-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                  <Dialog.Title
                    as="h3"
                    className="text-lg font-medium leading-6 text-[--primary] "
                  >
                    {isSuccess ? 'Register Success' : 'Register Failed'}
                  </Dialog.Title>
                  <div className="mt-2">
                    <p className="text-sm text-gray-500">
                      {isSuccess
                        ? 'Your account has been successfully created. You re now part of our community.'
                        : responseMessage}
                    </p>
                  </div>
                  <div className="mt-4 flex justify-end">
                    {isSuccess ? (
                      <button
                        type="button"
                        onClick={() => {
                          closeModal();
                          route.push('/login');
                        }}
                        className="text-sm flex gap-2 items-center"
                      >
                        <p>Got to login page</p>{' '}
                        <MdOutlineArrowRightAlt className="mt-1" />
                      </button>
                    ) : (
                      <button
                        type="button"
                        onClick={() => {
                          closeModal();
                        }}
                        className="text-sm flex gap-2 items-center"
                      >
                        <p>Close</p>{' '}
                      </button>
                    )}
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
};

Register.getLayout = (page) => <ProtectedLayout>{page}</ProtectedLayout>;

export default Register;
