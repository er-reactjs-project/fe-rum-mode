import { NextPageWithLayout } from '../../_app';
import MainLayout from '@/layout/MainLayout';

import Image from 'next/image';

import MetaHead from '@/components/shared/MetaHead';
import DivBreaker from '@/components/shared/DivBreaker';
import Wrapper from '@/components/shared/Wrapper';

import { RiCustomerService2Fill } from 'react-icons/ri';
import { FaRegClock } from 'react-icons/fa';
import { IoIosCloseCircleOutline } from 'react-icons/io';

const Help: NextPageWithLayout = () => {
  return (
    <>
      <MetaHead title="Help & Rule | Rumah Mode" />
      <DivBreaker />
      <Wrapper backgroundColor="bg-[url(/assets/images/help/bg_help.png)] bg-[--primary] text-white bg-cover bg-no-repeat">
        <p className="flex items-start pt-16 text-3xl">HELP & RULES</p>

        <div className="pt-8 flex items-start">
          <div className="bg-white w-full items-start rounded-3xl flex flex-col p-6 gap-6">
            <p className="text-[--primary] font-bold text-2xl">
              HOW CAN I HELP YOU?
            </p>
            <a href={`https://wa.me/+62134567890`}>
              <div className="flex flex-col items-center">
                <RiCustomerService2Fill className="text-black size-[50px]" />
                <p className="text-[--primary] text-xl">Contact</p>
              </div>
            </a>
          </div>
        </div>

        <div className="pt-8 flex items-start pb-[50px]">
          <div className="bg-white w-full items-start rounded-3xl flex flex-col p-6 gap-6">
            <p className="text-[--primary] font-bold text-2xl">RULES</p>
            <div className="flex flex-col gap-8">
              <div className="flex flex-row gap-6 items-center">
                <div className="min-h-[40px] min-w-[40px]">
                  <FaRegClock className="text-black size-[40px]" />
                </div>
                <p className="text-[--primary] text-xl">
                  Kami hanya melayani selama jam oprasional
                </p>
              </div>
              <div className="flex flex-row gap-6 items-center">
                <Image
                  src="/assets/images/trash_icon.svg"
                  alt="trash icon"
                  width={40}
                  height={40}
                  className="min-h-[40px] min-w-[40px]"
                />
                <p className="text-[--primary] text-xl">
                  Buanglah sampah pada tempatnya yang sudah disediakan
                </p>
              </div>
              <div className="flex flex-row gap-6 items-center">
                <div className="min-h-[40px] min-w-[40px]">
                  <IoIosCloseCircleOutline className="text-black size-[40px]" />
                </div>
                <p className="text-[--primary] text-xl">
                  Pembatalan Reservasi dapat dilakukan dengan menghubungi
                  Whatsapp
                </p>
              </div>
              <div className="flex flex-row gap-6 items-center">
                <Image
                  src="/assets/images/nofood_icon.svg"
                  alt="trash icon"
                  width={40}
                  height={40}
                  className="min-h-[40px] min-w-[40px]"
                />
                <p className="text-[--primary] text-xl">
                  Dilarang membawa makanan / minuman dari luar
                </p>
              </div>
              <div className="flex flex-row gap-6 items-center">
                <Image
                  src="/assets/images/nodrink_icon.svg"
                  alt="trash icon"
                  width={40}
                  height={40}
                  className="min-h-[40px] min-w-[40px]"
                />
                <p className="text-[--primary] text-xl">
                  Dilarang membawa minuman keras dan benda tajam
                </p>
              </div>
            </div>
          </div>
        </div>
      </Wrapper>
      {/* <div className="bg-[url(/assets/images/help/bg_help.png)] ">
        
      </div> */}
    </>
  );
};

Help.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default Help;
