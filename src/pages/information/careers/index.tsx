import { useState, useEffect } from 'react';
import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { CareerService } from '@/store/services';
import { NextPageWithLayout } from '../../_app';
import Link from 'next/link';

import MainLayout from '@/layout/MainLayout';
import MetaHead from '@/components/shared/MetaHead';
import Wrapper from '@/components/shared/Wrapper';
import DivBreaker from '@/components/shared/DivBreaker';
import Skeleton from '@/components/shared/Skeleton';

import { IoIosArrowUp } from 'react-icons/io';

const Careers: NextPageWithLayout = () => {
  const dispatch = useAppDispatch();
  const { lang } = useAppSelector((state) => state.global);
  const { loading, career } = useAppSelector((state) => state.career);
  const [selectedSection, setSelectedSection] = useState(0);

  useEffect(() => {
    const payload = { lang: lang };
    dispatch(CareerService.getCareer(payload));
  }, [lang, dispatch]);

  return (
    <>
      <MetaHead title="Careers | Rumah Mode" />
      <DivBreaker />
      <Wrapper
        backgroundColor="bg-[--primary]"
        className="min-h-[100vh] flex flex-col items-center pt-[34px] pb-[50px] gap-[24px]"
      >
        <p className="flex items-start text-3xl text-white w-full text-left mb-[10px]">
          Careers
        </p>

        {loading &&
          [...Array(5)].map((_, i) => (
            <Skeleton
              key={i}
              loading={loading}
              type="rectangle"
              className="h-[78px]"
            />
          ))}

        {!loading ? (
          career.length !== 0 ? (
            career.map((careerItem, indexCareer) => (
              <div
                key={indexCareer}
                onClick={() => {
                  if (indexCareer === selectedSection) {
                    setSelectedSection(999999);
                  } else {
                    setSelectedSection(indexCareer);
                  }
                }}
                className={`bg-white w-full items-start rounded-3xl flex flex-col p-6 gap-6 overflow-hidden ${
                  indexCareer === selectedSection
                    ? 'duration-[4s] max-h-[3500px]'
                    : 'duration-[1s] max-h-[78px]'
                }`}
              >
                <div className="w-full text-[--primary] font-bold text-2xl flex justify-between items-center">
                  <p>{careerItem.title}</p>
                  <IoIosArrowUp
                    className={`rotate-0 duration-500 ${
                      indexCareer === selectedSection ? 'rotate-180' : ''
                    }`}
                  />
                </div>
                <Skeleton loading={loading}>
                  {career && (
                    <div
                      dangerouslySetInnerHTML={{
                        __html: careerItem.description
                      }}
                    />
                  )}
                </Skeleton>
                <div className="w-full flex justify-end">
                  <Link
                    className="py-[12px] px-6 bg-[--primary] text-white rounded"
                    href={`mailto:${careerItem.email}`}
                  >
                    {careerItem.email}
                  </Link>
                </div>
              </div>
            ))
          ) : (
            <h1 className="mt-24 w-full text-2xl text-white text-center font-bold">
              Data not found
            </h1>
          )
        ) : (
          <></>
        )}
      </Wrapper>
    </>
  );
};

Careers.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default Careers;
