/* eslint-disable react-hooks/exhaustive-deps */
import { NextPageWithLayout } from '../../_app';
import MainLayout from '@/layout/MainLayout';

import MetaHead from '@/components/shared/MetaHead';
import DivBreaker from '@/components/shared/DivBreaker';

import Wrapper from '@/components/shared/Wrapper';
import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { AboutService } from '@/store/services';
import { useEffect } from 'react';
import Skeleton from '@/components/shared/Skeleton';

const AboutUs: NextPageWithLayout = () => {
  const dispatch = useAppDispatch();
  const { loading, about } = useAppSelector((state) => state.about);
  const { lang } = useAppSelector((state) => state.global);

  useEffect(() => {
    const payload = {
      lang: lang
    };
    dispatch(AboutService.getAbout(payload));
  }, [lang]);

  return (
    <>
      <MetaHead title="About Us | Rumah Mode" />
      <div className="lg:h-[100vh] 2xl:h-[100vh] min-[1700px]:h-[90vh] min-[1800px]:h-[85vh] min-[2000px]:h-[80vh] min-[2100px]:h-[75vh] min-[2400px]:h-[65vh] min-[2700px]:h-[60vh] min-[3200px]:h-[48vh] min-[4000px]:h-[40vh] min-[4800px]:h-[33vh] min-[5400px]:h-[30vh] min-[6400px]:h-[25vh] flex flex-col">
        <DivBreaker />
        {loading && (
          <div className="flex-grow">
            <Skeleton loading={loading} type="image" fullHeight={true} />
          </div>
        )}
        {!loading && about && (
          <div
            className="flex-grow bg-center bg-cover aspect-[16/7] "
            style={{ backgroundImage: `url(${about?.banner})` }}
          ></div>
        )}
      </div>
      <Wrapper
        backgroundColor="bg-[--primary]"
        className="flex flex-col items-center text-white pt-[34px] pb-[50px]"
      >
        <Skeleton loading={loading}>
          {about && (
            <div dangerouslySetInnerHTML={{ __html: about.description }} />
          )}
        </Skeleton>
      </Wrapper>
      <Wrapper backgroundColor="bg-[--primary]" className="pb-[50px]">
        <Skeleton loading={loading} paragraph={{ rows: 10 }}>
          {about && (
            <div>
              {about?.histories.map((val, idx) => (
                <div className="flex gap-6" key={idx}>
                  <div
                    className={`flex-grow flex flex-col items-end gap-1 ${
                      idx % 2 == 0 ? 'opacity-0' : ''
                    }`}
                  >
                    <p className="text-[32px] text-white mt-[-16px]">
                      {val.year}
                    </p>
                    <div
                      dangerouslySetInnerHTML={{ __html: val.description }}
                    />
                  </div>
                  <div>
                    <div className="w-[3px] h-full bg-white">
                      <div className="w-4 h-4 bg-white rounded-full ml-[-6px]"></div>
                    </div>
                  </div>
                  <div
                    className={`flex-grow flex flex-col gap-1 ${
                      idx! % 2 !== 0 ? 'opacity-0' : ''
                    }`}
                  >
                    <p className="text-[32px] text-white mt-[-16px]">
                      {val.year}
                    </p>
                    <div
                      dangerouslySetInnerHTML={{ __html: val.description }}
                    />
                  </div>
                </div>
              ))}
            </div>
          )}
        </Skeleton>
      </Wrapper>
    </>
  );
};

AboutUs.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default AboutUs;
