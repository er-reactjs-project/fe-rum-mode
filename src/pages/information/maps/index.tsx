import { NextPageWithLayout } from '../../_app';
import MainLayout from '@/layout/MainLayout';

import MetaHead from '@/components/shared/MetaHead';
import DivBreaker from '@/components/shared/DivBreaker';

const Maps: NextPageWithLayout = () => {
  return (
    <>
      <MetaHead title="Maps | Rumah Mode" />
      <DivBreaker />
      <div className="flex w-full aspect-[2/1] bg-rumod-maps bg-cover"></div>
    </>
  );
};

Maps.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default Maps;
