import React from 'react';
import { NextPageWithLayout } from '../_app';
import ProtectedLayout from '@/layout/ProtectedLayout';

import MetaHead from '@/components/shared/MetaHead';
import DivBreaker from '@/components/shared/DivBreaker';
import Wrapper from '@/components/shared/Wrapper';
import Button from '@/components/shared/Button';
import Link from 'next/link';

const Membership: NextPageWithLayout = () => {
  const membershipBenefitDymmyData = [
    {
      background: 'bg-membership-benefit-1',
      title: 'Diskon dan Penawaran Khusus',
      description:
        'Nikmati diskon eksklusif dan penawaran istimewa untuk produk fashion, makanan, dan layanan lainnya.'
    },
    {
      background: 'bg-membership-benefit-2',
      title: 'Akses Awal ke Produk Baru',
      description:
        'Dapatkan kesempatan menjadi yang pertama menggunakan produk terbaru sebelum tersedia untuk umum.'
    },
    {
      background: 'bg-membership-benefit-3',
      title: 'Program Hadiah',
      description: 'Kumpulkan....'
    },
    {
      background: 'bg-membership-benefit-4',
      title: 'Layanan Pelanggan Prioritas (?)',
      description:
        'Dapatkan bantuan layanan pelanggan dengan cepat dan efisien sebagai anggota prioritas.'
    },
    {
      background: 'bg-membership-benefit-5',
      title: 'Undangan ke Acara Khusus',
      description:
        'Terima undangan eksklusif ke acara peluncuran produk, pameran mode, dan acara khusus lainnya.'
    },
    {
      background: 'bg-membership-benefit-6',
      title: 'Layanan Pengiriman Prioritas (?)',
      description:
        'Menikmati layanan pengiriman cepat dan diutamakan untuk pesanan Anda sebagai anggota prioritas.'
    }
  ];
  return (
    <>
      <MetaHead title="Membership | Rumah Mode" />
      <div className="sm:h-[100vh] 2xl:h-[100vh] min-[1700px]:h-[90vh] min-[1800px]:h-[85vh] min-[2000px]:h-[80vh] min-[2100px]:h-[75vh] min-[2400px]:h-[65vh] min-[2700px]:h-[60vh] min-[3200px]:h-[48vh] min-[4000px]:h-[40vh] min-[4800px]:h-[33vh] min-[5400px]:h-[30vh] min-[6400px]:h-[25vh] flex flex-col">
        <DivBreaker />
        <div className="flex-grow bg-membership-banner bg-center bg-cover">
          <Wrapper
            outerClassName="h-full"
            className="h-full w-full flex flex-col items-center justify-center gap-6"
          >
            <div className="text-neutral-01 text-[32px] text-center font-bold">
              Berbagai Penawaran Eksklusif Hanya Untuk Member Rumah Mode
            </div>
            <div className="text-neutral-01 text-2xl text-center font-medium">
              Belanja produk fashion dan menikmati makanan kesukaan kamu jadi
              lebih seru dan menguntungkan dengan mendapatkan poin!
            </div>
            <Link href={'/login'}>              
              <Button color="secondary" className="font-bold text-2xl">
                Masuk Sekarang
              </Button>
            </Link>
          </Wrapper>
        </div>
      </div>
      <Wrapper
        backgroundColor="bg-neutral-02-background"
        className="text-[--primary]"
      >
        <div className="my-8 text-2xl text-center font-bold">
          JOIN DAN DAPATKAN BENEFIT INI
        </div>
        <div className="mb-8 flex flex-col gap-6">
          {membershipBenefitDymmyData?.map((benefit, benefitIndex) => (
            <div key={benefitIndex} className="flex gap-4 items-center">
              <div
                className={`aspect-[37/35] min-w-[98px] md:min-w-[148px] ${benefit?.background} bg-cover`}
              />
              <div>
                <div className="text-2xl font-bold">{benefit?.title}</div>
                <div className="text-xl font-medium">
                  {benefit?.description}
                </div>
              </div>
            </div>
          ))}
        </div>
      </Wrapper>
    </>
  );
};

Membership.getLayout = (page) => <ProtectedLayout>{page}</ProtectedLayout>;

export default Membership;
