/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import { NextPageWithLayout } from '../_app';
import Link from 'next/link';
import MainLayout from '@/layout/MainLayout';

import MetaHead from '@/components/shared/MetaHead';
import DivBreaker from '@/components/shared/DivBreaker';
import Wrapper from '@/components/shared/Wrapper';
import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { ReservationService } from '@/store/services';
import { useEffect } from 'react';
import Skeleton from '@/components/shared/Skeleton';

interface PackagesItemProps {
  id?: string;
  image?: string;
  title?: string;
  desc?: string;
}

const ReservationPackage: NextPageWithLayout = () => {
  const dispatch = useAppDispatch();
  const { loading, reservation } = useAppSelector((state) => state.reservation);
  const { lang } = useAppSelector((state) => state.global);

  useEffect(() => {
    const payload = {
      lang: lang
    };
    dispatch(ReservationService.getReservation(payload));
  }, [lang]);

  const PackagesItem = ({
    id = '',
    image = '',
    title = '',
    desc = ''
  }: PackagesItemProps) => {
    return (
      <>
        <Link href={`/reservation/packages/${id}`}>
          <div className="w-full flex flex-col rounded-[34px] overflow-hidden">
            <Skeleton
              loading={loading}
              type="image"
              className={image ? '' : 'h-28'}
            >
              <div
                className="aspect-[2/1]"
                style={{ backgroundImage: `url(${image})` }}
              />
            </Skeleton>
            <div className="p-6 flex flex-col gap-4 bg-neutral-01">
              <Skeleton loading={loading}>
                {reservation && (
                  <>
                    <div dangerouslySetInnerHTML={{ __html: title }} />
                    <div dangerouslySetInnerHTML={{ __html: desc }} />
                  </>
                )}
              </Skeleton>
            </div>
          </div>
        </Link>
      </>
    );
  };

  return (
    <>
      <MetaHead title="Reservation | Rumah Mode" />
      <div className="sm:h-[100vh] 2xl:h-[100vh] min-[1700px]:h-[90vh] min-[1800px]:h-[85vh] min-[2000px]:h-[80vh] min-[2100px]:h-[75vh] min-[2400px]:h-[65vh] min-[2700px]:h-[60vh] min-[3200px]:h-[48vh] min-[4000px]:h-[40vh] min-[4800px]:h-[33vh] min-[5400px]:h-[30vh] min-[6400px]:h-[25vh] flex flex-col">
        <DivBreaker />
        {loading && (
          <div className="flex-grow">
            <Skeleton loading={loading} type="image" fullHeight={true} />
          </div>
        )}
        {!loading && reservation && (
          <div
            className="flex-grow bg-center bg-cover aspect-[16/9] md:aspect-none "
            style={{ backgroundImage: `url(${reservation?.banner})` }}
          >
            <div className="h-full w-full backdrop-brightness-50">
              <div className="h-full flex flex-col text-center items-center justify-center">
                <p className="text-neutral-01 text-5xl font-bold">
                  Plan your event at Rumah Mode
                </p>
                <div className="flex mt-2">
                  <p className="inline text-neutral-01 text-3xl mr-2">
                    Reach us through Whatsapp
                  </p>
                  <div className="w-10 aspect-[1/1] bg-cover bg-center bg-whatsapp-logo" />
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
      <Wrapper
        backgroundColor="bg-[--primary]"
        className="flex flex-col items-center pt-[34px] pb-[50px]"
      >
        <Skeleton loading={loading}>
          {reservation && (
            <div dangerouslySetInnerHTML={{ __html: reservation.title }} />
          )}
        </Skeleton>
        <div className="w-full grid lg:grid-cols-2 gap-[50px] my-[50px]">
          {loading &&
            [...Array(4)].map((_, i) => (
              <React.Fragment key={i}>
                <PackagesItem />
              </React.Fragment>
            ))}
          {!loading &&
            reservation &&
            reservation.packages.map((item, idx) => (
              <React.Fragment key={idx}>
                <PackagesItem
                  id={item.id}
                  image={item.banner}
                  title={item.title}
                  desc={item.short_description}
                />
              </React.Fragment>
            ))}
        </div>
      </Wrapper>
    </>
  );
};

ReservationPackage.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default ReservationPackage;
