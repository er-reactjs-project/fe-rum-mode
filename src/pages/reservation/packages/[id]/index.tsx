import React from 'react';
import { useRouter } from 'next/router';

import { NextPageWithLayout } from '../../../_app';
import MainLayout from '@/layout/MainLayout';

import MetaHead from '@/components/shared/MetaHead';
import DivBreaker from '@/components/shared/DivBreaker';
import Wrapper from '@/components/shared/Wrapper';
import Button from '@/components/shared/Button';

import { MdKeyboardArrowLeft } from 'react-icons/md';
import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { useEffect } from 'react';
import { ReservationService } from '@/store/services';
import Skeleton from '@/components/shared/Skeleton';
import Link from 'next/link';

const ReservationPackageDetail: NextPageWithLayout = () => {
  const router = useRouter();
  const dispatch = useAppDispatch();
  const { loading, detailPackage } = useAppSelector(
    (state) => state.reservation
  );
  const { lang } = useAppSelector((state) => state.global);

  useEffect(() => {
    if (router.query.id) {
      const payload = {
        id: router.query.id,
        lang: lang
      };

      dispatch(ReservationService.getDetailPackage(payload));
    }
  }, [router, dispatch, lang]);

  return (
    <>
      <MetaHead title="Reservation Package Detail | Rumah Mode" />
      <div className="sm:h-[100vh] 2xl:h-[100vh] min-[1700px]:h-[90vh] min-[1800px]:h-[85vh] min-[2000px]:h-[80vh] min-[2100px]:h-[75vh] min-[2400px]:h-[65vh] min-[2700px]:h-[60vh] min-[3200px]:h-[48vh] min-[4000px]:h-[40vh] min-[4800px]:h-[33vh] min-[5400px]:h-[30vh] min-[6400px]:h-[25vh] flex flex-col">
        <DivBreaker />
        {loading && (
          <div className="flex-grow">
            <Skeleton loading={loading} type="image" fullHeight={true} />
          </div>
        )}
        {!loading && detailPackage && (
          <div
            className="flex-grow bg-center bg-cover aspect-[16/9] md:aspect-none"
            style={{ backgroundImage: `url(${detailPackage.banner})` }}
          ></div>
        )}
      </div>

      <div className="bg-neutral-02-background pt-8">
        <Link href="/reservation">
          <Button
            className="py-3 px-[50px] gap-4 font-bold rounded-none"
            icon={<MdKeyboardArrowLeft className="size-6" />}
            // onClick={router.back}
          >
            Back
          </Button>
        </Link>
      </div>

      <Wrapper
        backgroundColor="bg-neutral-02-background"
        className="flex flex-col items-center pt-[34px] pb-[50px]"
      >
        <Skeleton loading={loading} paragraph={{ rows: 5 }}>
          {detailPackage && (
            <>
              <div dangerouslySetInnerHTML={{ __html: detailPackage.title }} />
              <div
                dangerouslySetInnerHTML={{
                  __html: detailPackage.short_description
                }}
              />
            </>
          )}
        </Skeleton>
        <div className="mt-[34px] w-full flex flex-col lg:flex-row gap-[34px]">
          <div className="w-full lg:w-[250px] flex flex-col gap-6">
            {loading && (
              <div className="bg-cover w-full lg:w-[250px] aspect-square">
                <Skeleton loading={loading} type="image" fullHeight={true} />
              </div>
            )}
            {!loading &&
              detailPackage &&
              detailPackage.images.map((item, idx) => (
                <div
                  key={idx}
                  className="bg-cover w-full lg:w-[250px] aspect-square"
                  style={{
                    backgroundImage: `url(${item.image})`
                  }}
                />
              ))}
          </div>

          <div className="flex-grow flex flex-col justify-between h-full">
            <Skeleton loading={loading} paragraph={{ rows: 12 }}>
              {detailPackage && (
                <>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: detailPackage.description
                    }}
                  />
                </>
              )}
            </Skeleton>
            <div className="mt-8 lg:mt-16 flex justify-center lg:justify-end">
              <Button className="text-xl font-bold">Book Now</Button>
            </div>
          </div>
        </div>
      </Wrapper>
    </>
  );
};

ReservationPackageDetail.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default ReservationPackageDetail;
