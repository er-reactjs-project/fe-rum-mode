/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import { useRouter } from 'next/router';
import { NextPageWithLayout } from '../../_app';
import MainLayout from '@/layout/MainLayout';

import MetaHead from '@/components/shared/MetaHead';
import DivBreaker from '@/components/shared/DivBreaker';
import Wrapper from '@/components/shared/Wrapper';
import BlogCardComponent from '@/components/blog/BlogCard';

import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { BlogService } from '@/store/services';
import { useEffect } from 'react';
import Skeleton from '@/components/shared/Skeleton';
import { formatDate1 } from '@/libs/helper';

const BlogDetail: NextPageWithLayout = () => {
  const router = useRouter();
  const dispatch = useAppDispatch();
  const { loading, blog } = useAppSelector((state) => state.blog);
  const { lang } = useAppSelector((state) => state.global);

  useEffect(() => {
    const id = router.query.id;
    if (id) {
      const payload = { id: id, lang: lang };
      dispatch(BlogService.getDetailBlog(payload));
    }
  }, [lang, router]);

  return (
    <>
      <MetaHead title="Blog Detail | Rumah Mode" />
      <DivBreaker />
      <Wrapper
        backgroundColor="bg-neutral-02-background"
        className="flex flex-col items-center pt-8 pb-12"
      >
        <Skeleton
          loading={loading}
          type="rectangle"
          className="w-2/3 h-5 mt-5 mb-7 flex items-center justify-center"
        >
          {blog && (
            <div
              className="line-clamp-[1]"
              dangerouslySetInnerHTML={{
                __html: blog.title
              }}
            />
            // <div className="text-[32px] text-[--primary] font-bold">
            //   {blog.title}
            // </div>
          )}
        </Skeleton>

        <Skeleton
          loading={loading}
          type="rectangle"
          className="w-1/3 h-5 flex items-center justify-center gap-2 text-2xl text-neutral-05 font-medium"
        >
          {blog && (
            <>
              <div>{formatDate1(blog.created_at!)}</div>
              <div className="w-1 h-1 rounded-full bg-neutral-05" />
              <div>{blog.read_time_minute} mins read</div>
            </>
          )}
        </Skeleton>

        <div className="w-full flex flex-col my-8 items-center">
          <Skeleton
            loading={loading}
            type="image"
            className={loading ? 'h-[512px]' : ''}
          >
            {blog && (
              <div
                className="w-full aspect-[1240/641] bg-cover"
                style={{ backgroundImage: `url(${blog.banner})` }}
              />
            )}
          </Skeleton>

          <Skeleton
            loading={loading}
            paragraph={{ rows: 20 }}
            className="pt-6 flex flex-col"
          >
            {blog && <div dangerouslySetInnerHTML={{ __html: blog.content }} />}
          </Skeleton>
        </div>

        {/*  */}
        <div className="w-full text-2xl text-[--primary] font-bold mb-6 lg:mb-4">
          Baca Juga :
        </div>
        <div className="w-full grid md:grid-cols-2 lg:grid-cols-4 gap-6">
          {loading &&
            [...Array(4)].map((_, i) => (
              <div key={i} className="flex flex-col items-start">
                <Skeleton
                  loading={loading}
                  type="image"
                  className="h-[250px] mb-3"
                />
                <Skeleton loading={loading} />
              </div>
            ))}
          {!loading &&
            blog &&
            blog.other_blogs.map((item, index) => (
              <React.Fragment key={index}>
                <BlogCardComponent data={item} />
              </React.Fragment>
            ))}
        </div>
      </Wrapper>
    </>
  );
};

BlogDetail.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default BlogDetail;
