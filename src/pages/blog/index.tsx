/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState } from 'react';
import { NextPageWithLayout } from '../_app';
import MainLayout from '@/layout/MainLayout';
import Link from 'next/link';

import MetaHead from '@/components/shared/MetaHead';
import DivBreaker from '@/components/shared/DivBreaker';
import Wrapper from '@/components/shared/Wrapper';
import BlogCardComponent from '@/components/blog/BlogCard';
import Pagination from '@/components/shared/Pagination';

import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { BlogService } from '@/store/services';
import { useEffect } from 'react';
import Skeleton from '@/components/shared/Skeleton';
import { formatDate1 } from '@/libs/helper';

const Blog: NextPageWithLayout = () => {
  const dispatch = useAppDispatch();
  const { loading, listBlog, paging } = useAppSelector((state) => state.blog);
  const { lang } = useAppSelector((state) => state.global);
  const [currentPage, setCurrentPage] = useState(1);
  const pageSize = 4;

  useEffect(() => {
    const payload = {
      limit: pageSize,
      page: currentPage,
      lang: lang
    };
    dispatch(BlogService.getListBlog(payload));
  }, [lang, currentPage]);

  return (
    <>
      <MetaHead title="Blog | Rumah Mode" />
      <DivBreaker />
      <Wrapper backgroundColor="bg-neutral-02-background" className="my-8">
        <div className="text-[32px] text-[--primary] font-bold">BLOG</div>
        <Link href={`/blog/${listBlog ? listBlog.main.id : ''}`}>
          <div className="my-6 flex flex-col lg:flex-row rounded-xl overflow-hidden border-2 border-neutral-08">
            <Skeleton
              loading={loading}
              type="image"
              className="max-w-[600px] min-w-[600px]"
            >
              {listBlog && (
                <div
                  className="w-full h-full bg-cover bg-center aspect-[600/310]"
                  style={{ backgroundImage: `url(${listBlog?.main.banner})` }}
                />
              )}
            </Skeleton>

            <div className="p-6 flex flex-col justify-between w-full">
              <Skeleton loading={loading} paragraph={{ rows: 12 }}>
                {listBlog && (
                  <>
                    <div
                      className="line-clamp-[1]"
                      dangerouslySetInnerHTML={{
                        __html: listBlog.main.title
                      }}
                    />
                    <div
                      className="line-clamp-[8]"
                      dangerouslySetInnerHTML={{
                        __html: listBlog.main.content
                      }}
                    />
                  </>
                )}
              </Skeleton>

              <Skeleton
                loading={loading}
                type="rectangle"
                className="min-h-5 mt-2 flex items-center gap-2 text-lg text-neutral-05"
              >
                {listBlog && (
                  <>
                    <div>{formatDate1(listBlog.main.created_at!)}</div>
                    <div className="w-1 h-1 rounded-full bg-neutral-05" />
                    <div>{listBlog.main.read_time_minute} mins read</div>
                  </>
                )}
              </Skeleton>
            </div>
          </div>
        </Link>

        <div className="grid md:grid-cols-2 lg:grid-cols-4 gap-6">
          {loading &&
            [...Array(pageSize)].map((_, i) => (
              <div key={i} className="flex flex-col items-start">
                <Skeleton
                  loading={loading}
                  type="image"
                  className="h-[250px] mb-3"
                />
                <Skeleton loading={loading} />
              </div>
            ))}
          {!loading &&
            listBlog &&
            listBlog.records?.map((blog, index) => (
              <React.Fragment key={index}>
                <BlogCardComponent data={blog} />
              </React.Fragment>
            ))}
        </div>

        <Wrapper className="flex justify-center items-center mt-10">
          <Pagination
            totalData={paging.total_data}
            totalPage={paging.total_page}
            limit={pageSize}
            current={currentPage}
            onClick={setCurrentPage}
            enableGoToPage
          />
        </Wrapper>
      </Wrapper>
    </>
  );
};

Blog.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default Blog;
