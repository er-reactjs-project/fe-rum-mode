import { Fragment, useState } from 'react';
import { NextPageWithLayout } from '../_app';
import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { AuthService } from '@/store/services';
import Image from 'next/image';

import MetaHead from '@/components/shared/MetaHead';
import Wrapper from '@/components/shared/Wrapper';
import DivBreaker from '@/components/shared/DivBreaker';
import Button from '@/components/shared/Button';
import Link from 'next/link';
import { useFormik } from 'formik';
import PasswordInput from '@/components/shared/PasswordInput';
import LoadingComponent from '@/components/shared/Loading';
import { Dialog, Transition } from '@headlessui/react';
import { useRouter } from 'next/router';
import ProtectedLayout from '@/layout/ProtectedLayout';
import { getRememberMe, setRememberMe } from '@/libs/local-storage/remember';

const Login: NextPageWithLayout = () => {
  const router = useRouter();
  const dispatch = useAppDispatch();
  const { loading } = useAppSelector((state) => state.auth);
  const [remember, setRemember] = useState(false);

  const [isOpen, setIsOpen] = useState(false);
  const [isSuccess, setIsSuccess] = useState(true);
  const [responseMessage, setResponseMessage] = useState('');

  const closeModal = () => setIsOpen(false);
  const openModal = () => setIsOpen(true);

  const getInitialValues = () => {
    let data = {
      email_or_phone: '',
      password: ''
    };

    const saved = getRememberMe('login');
    if (saved) {
      data.email_or_phone = saved.email_or_phone;
      data.password = saved.password;
    }
    return data;
  }

  const formik = useFormik({
    initialValues: getInitialValues(),
    onSubmit: async (values) => {
      dispatch(AuthService.login(values)).then((res: any) => {
        if (res?.error) {
          setResponseMessage(res.payload?.error);
          setIsSuccess(false);
          openModal();
          return;
        }

        setIsSuccess(true);
        openModal();
        setTimeout(() => {
          closeModal();
          router.push('/profile');
        }, 2000);
      });
    }
  });

  const handleRememberMe = (e: any) => {
    setRemember(e.target.checked);
    setRememberMe('login', formik.values);
  }

  return (
    <>
      <MetaHead title="Login | Rumah Mode" />
      <div className="flex flex-col">
        <DivBreaker />
        <div className="flex-grow bg-login-banner bg-cover">
          <Wrapper>
            <div className="p-[34px] bg-white rounded flex flex-col max-w-[350px] my-24 items-start gap-2">
              <div className="flex flex-col items-start gap-6 w-full">
                <div className="flex flex-col items-start gap-1">
                  <p className="font-bold text-[--primary] text-2xl">
                    Hallo Pelanggan Setia,
                  </p>
                  <p className="text-[--primary] text-xl">Silakan Masuk</p>
                </div>
                <form onSubmit={formik.handleSubmit} className="w-full">
                  <div className="flex flex-col items-start gap-4 w-full">
                    <label className="flex flex-col items-start gap-1 w-full">
                      <p className="text-[--primary] font-bold text-xl">
                        Email / No HP
                      </p>
                      <input
                        type="text"
                        name="email_or_phone"
                        className="p-4 border-[--primary] rounded w-full border-2"
                        placeholder="Masukkan email / no hp"
                        value={formik.values.email_or_phone}
                        onChange={formik.handleChange}
                      />
                    </label>
                    <label className="flex flex-col items-start gap-1 w-full">
                      <p className="text-[--primary] font-bold text-xl">
                        Kata Sandi
                      </p>
                      <PasswordInput
                        iconPosition="right"
                        placeHolder="Masukkan kata sandi"
                        value={formik.values.password}
                        onChange={formik.handleChange}
                      />
                    </label>
                    <div className="flex justify-between items-center w-full">
                      <label className="flex flex-row items-center gap-2">
                        <input
                          type="checkbox"
                          checked={remember}
                          onChange={handleRememberMe}
                        />
                        <p className="text-[--primary] font-semibold text-lg">
                          Ingat Saya
                        </p>
                      </label>
                      <Link
                        href={'/forgot-password'}
                        className="text-[--primary] text-lg"
                      >
                        Lupa kata sandi?
                      </Link>
                    </div>
                    <Button
                      type="submit"
                      className="rounded-none flex-col w-full text-center"
                    >
                      {loading ? <LoadingComponent /> : 'Masuk'}
                    </Button>
                  </div>
                </form>
              </div>
            </div>
          </Wrapper>
        </div>
      </div>
      <div className="bg-[--primary] pt-8 pb-12 px-6 flex flex-col items-center">
        <div className="p-6 flex items-center gap-6 flex-col md:flex-row rounded-lg bg-white">
          <div className="bg-[--primary] p-8">
            <Image
              src="/assets/images/logo_white.svg"
              alt="white logo"
              width={320}
              height={150}
            />
          </div>
          <div className="flex flex-col md:flex-row items-center gap-6 flex-grow">
            <div className="flex flex-col flex-grow gap-4 items-start">
              <p className="font-bold text-xl text-[--primary]">
                Tetap Bersama Kami
              </p>
              <p className="text-lg text-[--primary]">
                Ketika Anda mendaftar untuk menerima email, Anda akan
                mendapatkan <br /> penawaran khusus, berita terkini, dan banyak
                lagi
              </p>
            </div>
            <Button className="!rounded py-3 px-6 font-bold">
              <Link href="/register">Mendaftar</Link>
            </Button>
          </div>
        </div>
      </div>

      {/* Dialogue */}
      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={() => closeModal()}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black/25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex min-h-full items-center justify-center p-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                  <Dialog.Title
                    as="h3"
                    className="text-lg font-medium leading-6 text-[--primary] "
                  >
                    {isSuccess ? 'Login Success' : 'Login Failed'}
                  </Dialog.Title>
                  <div className="mt-2">
                    <p className="text-sm text-gray-500">
                      {isSuccess ? '' : responseMessage}
                    </p>
                  </div>
                  <div className="mt-4 flex justify-end">
                    <button
                      type="button"
                      onClick={() => {
                        closeModal();
                      }}
                      className="text-sm flex gap-2 items-center"
                    >
                      <p>Close</p>{' '}
                    </button>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
};

Login.getLayout = (page) => <ProtectedLayout>{page}</ProtectedLayout>;

export default Login;
