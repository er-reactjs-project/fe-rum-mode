/* eslint-disable no-unused-vars */
import type { AppProps } from 'next/app';
import type { ReactElement, ReactNode } from 'react';
import type { NextPage } from 'next';

// redux
import { Provider } from 'react-redux';
import { store } from '@/store/redux';

// style
import '@/styles/globals.css';
import 'keen-slider/keen-slider.min.css';

export type NextPageWithLayout<P = {}, IP = P> = NextPage<P, IP> & {
  getLayout?: (page: ReactElement) => ReactNode;
};

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout;
};

export default function App({ Component, pageProps }: AppPropsWithLayout) {
  const getLayout = Component.getLayout ?? ((page) => page);

  return (
    <Provider store={store}>
      <main>
        {getLayout(<Component {...pageProps} />)}
      </main>
    </Provider>
  );
}
