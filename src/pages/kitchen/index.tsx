/* eslint-disable @next/next/no-img-element */
/* eslint-disable react-hooks/exhaustive-deps */
import { NextPageWithLayout } from '../_app';
import MainLayout from '@/layout/MainLayout';

// import Image from 'next/image';

import MetaHead from '@/components/shared/MetaHead';
import DivBreaker from '@/components/shared/DivBreaker';
import Wrapper from '@/components/shared/Wrapper';
import KitchenSection from '@/components/kitchen/KitchenSection';

import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { KitchenService } from '@/store/services';
import { useEffect } from 'react';
import Skeleton from '@/components/shared/Skeleton';

const Kitchen: NextPageWithLayout = () => {
  const dispatch = useAppDispatch();
  const { loading, kitchen } = useAppSelector((state) => state.kitchen);
  const { lang } = useAppSelector((state) => state.global);

  useEffect(() => {
    const payload = {
      lang: lang
    };
    dispatch(KitchenService.getModeKitchen(payload));
  }, [lang]);

  const storeSections = [
    { id: 'food', category: 'food' },
    { id: 'dessert', category: 'dessert - beverages' },
    { id: 'traditional-snack', category: 'traditional snack' },
    { id: 'oleh-oleh', category: 'oleh - oleh bandung' }
  ];

  return (
    <>
      <MetaHead title="Kitchen | Rumah Mode" />
      <DivBreaker />
      <Wrapper className="flex justify-center items-center pt-8 gap-4">
        <Skeleton
          loading={loading}
          type="image"
          className={`${loading ? 'h-[200px] ' : 'h-auto'} !w-[250px]`}
        >
          {kitchen && (
            <img
              src={kitchen.logo}
              alt="Logo Mode Kitchen"
              width={250}
              height={204}
              // priority
            />
          )}
        </Skeleton>
      </Wrapper>
      {storeSections.map((item) => (
        <KitchenSection
          loading={loading}
          key={item.id}
          id={item.id}
          title={
            item.category === 'dessert - beverages'
              ? item.category.replace('-', '&')
              : item.category
          }
          category={item.category}
          data={kitchen ? kitchen.stores[item.category] : []}
        />
      ))}

      <Wrapper className="flex flex-col justify-center items-center my-7">
        <p className="font-bold text-lg mb-5">Available On:</p>
        <Skeleton
          loading={loading}
          type="image"
          className={`${loading ? 'h-24 ' : 'h-auto'}`}
        >
          {kitchen && (
            <div className="flex flex-row flex-wrap gap-12 justify-center">
            {kitchen.partners.map((item) => (
              <img
                key={item.id}
                src={item.image}
                alt={`Partner ${item.id}`}
                sizes="100vw"
                style={{
                  minWidth: '10%',
                  height: 'auto'
                }}
                height={40}
                width={200}
              />
            ))}
          </div>
          )}
        </Skeleton>
      </Wrapper>
    </>
  );
};

Kitchen.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default Kitchen;
