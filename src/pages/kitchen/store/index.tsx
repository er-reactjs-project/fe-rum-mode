/* eslint-disable @next/next/no-img-element */
/* eslint-disable react-hooks/exhaustive-deps */
import { NextPageWithLayout } from '../../_app';
import MainLayout from '@/layout/MainLayout';

// import Image from 'next/image';

import MetaHead from '@/components/shared/MetaHead';
import DivBreaker from '@/components/shared/DivBreaker';
import Button from '@/components/shared/Button';
import Wrapper from '@/components/shared/Wrapper';
import StoreCard from '@/components/kitchen/StoreCard';

import { IoIosArrowBack } from 'react-icons/io';

import { useSearchParams } from 'next/navigation';
import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { KitchenService } from '@/store/services';
import { useEffect } from 'react';
import Skeleton from '@/components/shared/Skeleton';
import Link from 'next/link';

const Store: NextPageWithLayout = () => {
  const searchParams = useSearchParams();
  const dispatch = useAppDispatch();
  const { loading, listStore, kitchen } = useAppSelector(
    (state) => state.kitchen
  );
  const { lang } = useAppSelector((state) => state.global);

  const category = searchParams.get('category');

  useEffect(() => {
    if (category) {
      const payload = {
        lang: lang,
        category: category
      };
      dispatch(KitchenService.getAllStore(payload));
    }
  }, [lang, category]);

  useEffect(() => {
    if (!kitchen) {
      const payload = { lang: lang };
      dispatch(KitchenService.getModeKitchen(payload));
    }
  }, [lang, kitchen]);

  const getTitle = () => {
    var val = '';
    if (category) {
      if (category === 'dessert - beverages') {
        val = category.replace('-', '&').toUpperCase();
      } else {
        val = category.toUpperCase();
      }
    }
    return val;
  };

  return (
    <>
      <MetaHead title={`${getTitle()} | Rumah Mode`} />
      <DivBreaker />
      <div className="flex pt-8 flex-col items-start">
        <Link href="/kitchen">
          <Button
            className="px-[50px] py-3 items-center font-bold text-2xl gap-4 rounded-none"
            icon={<IoIosArrowBack className="size-6" />}
          >
            Back
          </Button>
        </Link>
      </div>
      <div className="mt-5 flex py-[10px] md:pl-[50px] items-center w-full md:w-[450px] bg-[--primary]">
        <p className="font-bold text-3xl text-white">{getTitle()}</p>
      </div>
      <Wrapper className="mt-14 pb-4">
        <div className="grid md:grid-cols-2 lg:grid-cols-3 gap-4 md:gap-12 min-h-60">
          {loading &&
            [...Array(3)].map((_, i) => (
              <div key={i} className="flex flex-col items-start">
                <Skeleton
                  loading={loading}
                  type="image"
                  className="w-[300px] h-[400px]"
                />
                <div className="flex md:w-[300px] flex-col py-4 gap-3">
                  <Skeleton loading={loading} />
                </div>
              </div>
            ))}
          {!loading &&
            listStore &&
            listStore.map((item) => <StoreCard key={item.id} item={item} category={category?? ''} />)}
        </div>

        <div className="flex flex-col justify-center items-center my-8 w-full">
          <p className="font-bold text-lg mb-5">Available On:</p>
          <Skeleton
            loading={loading}
            type="image"
            className={`${loading ? 'h-24 ' : 'h-auto'}`}
          >
            {kitchen && (
              <div className="flex flex-row flex-wrap gap-12 justify-center">
                {kitchen.partners.map((item) => (
                  <img
                    key={item.id}
                    src={item.image}
                    alt={`Partner ${item.id}`}
                    sizes="100vw"
                    style={{
                      minWidth: '10%',
                      height: 'auto'
                    }}
                    height={40}
                    width={200}
                  />
                ))}
              </div>
            )}
          </Skeleton>
        </div>
      </Wrapper>
    </>
  );
};

Store.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default Store;
