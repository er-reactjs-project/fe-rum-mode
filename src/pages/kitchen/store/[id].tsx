/* eslint-disable @next/next/no-img-element */
/* eslint-disable react-hooks/exhaustive-deps */
import { NextPageWithLayout } from '../../_app';
import MainLayout from '@/layout/MainLayout';

import { useRouter } from 'next/router';
// import Image from 'next/image';

import MetaHead from '@/components/shared/MetaHead';
import Button from '@/components/shared/Button';
import DivBreaker from '@/components/shared/DivBreaker';

import { MdKeyboardArrowLeft } from 'react-icons/md';
import { FiDownload } from 'react-icons/fi';
// import { FaCircle } from 'react-icons/fa';

import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { KitchenService } from '@/store/services';
import { useEffect } from 'react';
import Skeleton from '@/components/shared/Skeleton';
import Link from 'next/link';
import Wrapper from '@/components/shared/Wrapper';

interface TagProps {
  loading: boolean;
  title: string;
  content: string;
  type?: 'text' | 'link';
}

const Tag = (props: TagProps) => {
  const { loading, title, content, type = 'text' } = props;
  return loading ? (
    <Skeleton loading={loading} />
  ) : (
    <div className="flex flex-col items-start gap-1 self-stretch">
      <p className="text-2xl font-bold text-[--primary]">{title}</p>
      {type === 'link' ? (
        <Link href={content} target="_blank">
          <p className="text-xl text-[--primary]">{content}</p>
        </Link>
      ) : (
        <p className="text-xl text-[--primary]">{content}</p>
      )}
    </div>
  );
};

const Description: NextPageWithLayout = () => {
  const router = useRouter();
  const storeId = router.query.id;
  const dispatch = useAppDispatch();
  const { loading, detailStore, kitchen } = useAppSelector(
    (state) => state.kitchen
  );
  const { lang } = useAppSelector((state) => state.global);

  useEffect(() => {
    if (storeId) {
      const payload = { id: storeId, lang: lang };
      dispatch(KitchenService.getDetailStore(payload));
    }
  }, [lang, storeId]);

  useEffect(() => {
    if (!kitchen) {
      const payload = { lang: lang };
      dispatch(KitchenService.getModeKitchen(payload));
    }
  }, [lang, kitchen]);

  return (
    <>
      <MetaHead
        title={`${detailStore ? detailStore.name + ' |' : ''} Rumah Mode`}
      />
      <DivBreaker />
      <div className="pt-8">
        <Link href="/kitchen">
          <Button
            className="py-3 px-[50px] gap-4 font-bold rounded-none"
            icon={<MdKeyboardArrowLeft className="size-6" />}
          >
            Back
          </Button>
        </Link>
      </div>
      <div className="flex flex-col min-[1160px]:flex-row items-start mt-7 pt-[34px] pr-10 pb-[50px] pl-10 md:px-[100px] md:gap-[50px]">
        <div className="flex flex-col items-start gap-6 max-w-[444px]">
          <Skeleton
            loading={loading}
            type="image"
            className={`lg:w-[300px] xs:w-[200px] ${
              loading ? 'h-[200px] ' : 'h-auto'
            }`}
          >
            {detailStore && (
              <div
                className="bg-cover aspect-square"
                style={{
                  backgroundImage: `url(${detailStore.image})`
                }}
              />
            )}
          </Skeleton>

          <Skeleton loading={loading} type="rectangle" className="h-10">
            {detailStore && (
              <p className="font-bold text-[--primary] text-3xl">
                {detailStore.name}
              </p>
            )}
          </Skeleton>

          <div className="flex flex-col items-start gap-1 self-stretch">
            <Skeleton loading={loading}>
              <p className="text-2xl font-bold text-[--primary]">Description</p>
              {detailStore && (
                <div
                  dangerouslySetInnerHTML={{ __html: detailStore.description }}
                />
              )}
            </Skeleton>
          </div>

          <Tag
            loading={loading}
            title="Location"
            content={detailStore ? detailStore.location : ''}
          />
          <Tag
            loading={loading}
            title="Open Hours"
            content={
              detailStore
                ? detailStore.open_hour + ' - ' + detailStore.close_hour
                : ''
            }
          />
          <Tag
            loading={loading}
            title="Link"
            type="link"
            content={detailStore ? detailStore.link : ''}
          />
        </div>
        <div className="flex flex-col gap-4 items-start w-full">
          <div className="flex flex-row justify-between self-stretch items-center">
            <p className="text-2xl font-bold text-[--primary]">Menu</p>
            <Link
              href={detailStore ? detailStore.menu : '#'}
              target="_blank"
              rel="noopener noreferrer"
              download={true}
            >
              <Button
                className="py-2 px-4 rounded-lg gap-2 font-bold"
                icon={<FiDownload />}
                iconPosition="right"
              >
                Download
              </Button>
            </Link>
          </div>
          <div className="w-full">
            <Skeleton
              loading={loading}
              type="image"
              className={`${loading ? 'h-[900px] !w-[750px]' : 'h-auto'}`}
            >
              {detailStore && (
                <object
                  data={detailStore.menu}
                  type="application/pdf"
                  width={750}
                  aria-labelledby="PDF document"
                  className="w-full aspect-[9/12.5]"
                >
                  <p>Your browser does not support PDFs. Download the PDF</p>
                </object>
              )}
            </Skeleton>
          </div>
        </div>
      </div>
      <Wrapper className="flex justify-center items-center pt-8 gap-4 mb-8">
        <Skeleton
          loading={loading}
          type="image"
          className={`${loading ? 'h-[200px] ' : 'h-auto'} !w-[250px]`}
        >
          {kitchen && (
            <img
              src={kitchen.logo}
              alt="Logo Mode Kitchen"
              width={250}
              height={204}
              // priority
            />
          )}
        </Skeleton>
      </Wrapper>
    </>
  );
};

Description.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default Description;
