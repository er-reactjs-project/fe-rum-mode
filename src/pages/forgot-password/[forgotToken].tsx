import { Fragment, useState } from 'react';
import { NextPageWithLayout } from '../_app';
import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { AuthService } from '@/store/services';

import MainLayout from '@/layout/MainLayout';
import MetaHead from '@/components/shared/MetaHead';
import Wrapper from '@/components/shared/Wrapper';
import DivBreaker from '@/components/shared/DivBreaker';
import Button from '@/components/shared/Button';
import { useFormik } from 'formik';
import LoadingComponent from '@/components/shared/Loading';
import PasswordInput from '@/components/shared/PasswordInput';
import { Dialog, Transition } from '@headlessui/react';
import { useRouter } from 'next/router';

import { MdOutlineArrowRightAlt } from 'react-icons/md';

const NextForgotPassword: NextPageWithLayout = () => {
  const router = useRouter();
  const forgotToken = router.query.forgotToken;
  const dispatch = useAppDispatch();
  const { loading } = useAppSelector((state) => state.auth);

  const [isOpen, setIsOpen] = useState(false);
  const [isSuccess, setIsSuccess] = useState(true);
  const [responseMessage, setResponseMessage] = useState('');

  const closeModal = () => setIsOpen(false);
  const openModal = () => setIsOpen(true);

  const formik = useFormik({
    initialValues: {
      password: ''
    },
    onSubmit: async (values) => {
      dispatch(
        AuthService.forgotPassword({
          code: forgotToken,
          new_password: values.password,
        })
      ).then((res: any) => {
        if (res?.error) {
          setIsSuccess(false);
          setResponseMessage(res.payload.error);
        } else {
          setIsSuccess(true);
        }

        openModal();
      });
    }
  });

  return (
    <>
      <MetaHead title="Forgot Password | Rumah Mode" />
      <div className="flex flex-col">
        <DivBreaker />
        <div className="flex-grow bg-login-banner bg-cover">
          <Wrapper>
            <div className="p-[34px] bg-white rounded flex flex-col max-w-[350px] my-24 items-start gap-2">
              <div className="flex flex-col items-start gap-6 w-full">
                <div className="flex flex-col items-start gap-1">
                  <p className="font-bold text-[--primary] text-2xl">
                    Forgot Password
                  </p>
                </div>
                <form onSubmit={formik.handleSubmit} className="w-full">
                  <div className="flex flex-col items-start gap-4 w-full">
                    <label className="flex flex-col items-start gap-1 w-full">
                      <p className="text-[--primary] font-bold text-xl">
                        Kata Sandi Baru
                      </p>
                      <PasswordInput
                        iconPosition="right"
                        placeHolder="Masukkan kata sandi"
                        value={formik.values.password}
                        onChange={formik.handleChange}
                      />
                    </label>
                    <Button
                      type="submit"
                      className="rounded-none flex-col w-full text-center"
                    >
                      {loading ? <LoadingComponent /> : 'Submit'}
                    </Button>
                  </div>
                </form>
              </div>
            </div>
          </Wrapper>
        </div>
      </div>
      

      {/* Dialogue */}
      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={() => closeModal()}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black/25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex min-h-full items-center justify-center p-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                  <Dialog.Title
                    as="h3"
                    className="text-lg font-medium leading-6 text-[--primary] "
                  >
                    {isSuccess
                      ? 'Forgot Password Success'
                      : 'Forgot Password Failed'}
                  </Dialog.Title>
                  <div className="mt-2">
                    <p className="text-sm text-gray-500">
                      {isSuccess
                        ? 'Your account has been successfully created. You re now part of our community.'
                        : responseMessage}
                    </p>
                  </div>
                  <div className="mt-4 flex justify-end">
                    {isSuccess ? (
                      <button
                        type="button"
                        onClick={() => {
                          closeModal();
                          router.push('/login');
                        }}
                        className="text-sm flex gap-2 items-center"
                      >
                        <p>Got to login page</p>{' '}
                        <MdOutlineArrowRightAlt className="mt-1" />
                      </button>
                    ) : (
                      <button
                        type="button"
                        onClick={() => {
                          closeModal();
                        }}
                        className="text-sm flex gap-2 items-center"
                      >
                        <p>Close</p>{' '}
                      </button>
                    )}
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
};

NextForgotPassword.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default NextForgotPassword;
