import { Fragment, useState } from 'react';
import { NextPageWithLayout } from '../_app';
import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { AuthService } from '@/store/services';
import Image from 'next/image';

import ProtectedLayout from '@/layout/ProtectedLayout';
import MetaHead from '@/components/shared/MetaHead';
import Wrapper from '@/components/shared/Wrapper';
import DivBreaker from '@/components/shared/DivBreaker';
import Button from '@/components/shared/Button';
import Link from 'next/link';
import { useFormik } from 'formik';
import LoadingComponent from '@/components/shared/Loading';
import { Dialog, Transition } from '@headlessui/react';

const ForgotPassword: NextPageWithLayout = () => {
  const dispatch = useAppDispatch();
  const { loading } = useAppSelector((state) => state.auth);

  const [isOpen, setIsOpen] = useState(false);
  const [modalTitle, setModalTitle] = useState('');
  const [responseMessage, setResponseMessage] = useState('');

  const closeModal = () => setIsOpen(false);
  const openModal = () => setIsOpen(true);

  const formik = useFormik({
    initialValues: {
      email: ''
    },
    onSubmit: async (values) => {
      dispatch(AuthService.getVerificationCode(values)).then((res: any) => {
        if (res?.error) {
          setModalTitle('Failed to send link to your email');
          setResponseMessage(res.payload.error);
        } else {
          setModalTitle('Link has been sent to your email');
        }

        openModal();
      });
    }
  });

  return (
    <>
      <MetaHead title="Forgot Password | Rumah Mode" />
      <div className="flex flex-col">
        <DivBreaker />
        <div className="flex-grow bg-login-banner bg-cover">
          <Wrapper>
            <div className="p-[34px] bg-white rounded flex flex-col max-w-[350px] my-24 items-start gap-2">
              <div className="flex flex-col items-start gap-6 w-full">
                <div className="flex flex-col items-start gap-1">
                  <p className="font-bold text-[--primary] text-2xl">
                    Forgot Password
                  </p>
                </div>
                <form onSubmit={formik.handleSubmit} className="w-full">
                  <div className="flex flex-col items-start gap-4 w-full">
                    <label className="flex flex-col items-start gap-1 w-full">
                      <p className="text-[--primary] font-bold text-xl">
                        Email / No HP
                      </p>
                      <input
                        type="text"
                        name="email"
                        className="p-4 border-[--primary] rounded w-full border-2"
                        placeholder="Masukkan email / no hp"
                        value={formik.values.email}
                        onChange={formik.handleChange}
                      />
                    </label>
                    <Button
                      type="submit"
                      className="rounded-none flex-col w-full text-center"
                    >
                      {loading ? <LoadingComponent /> : 'Submit'}
                    </Button>
                  </div>
                </form>
              </div>
            </div>
          </Wrapper>
        </div>
      </div>
      <div className="bg-[--primary] pt-8 pb-12 px-6 flex flex-col items-center">
        <div className="p-6 flex items-center gap-6 flex-col md:flex-row rounded-lg bg-white">
          <div className="bg-[--primary] p-8">
            <Image
              src="/assets/images/logo_white.svg"
              alt="white logo"
              width={320}
              height={150}
            />
          </div>
          <div className="flex flex-col md:flex-row items-center gap-6 flex-grow">
            <div className="flex flex-col flex-grow gap-4 items-start">
              <p className="font-bold text-xl text-[--primary]">
                Tetap Bersama Kami
              </p>
              <p className="text-lg text-[--primary]">
                Ketika Anda mendaftar untuk menerima email, Anda akan
                mendapatkan <br /> penawaran khusus, berita terkini, dan banyak
                lagi
              </p>
            </div>
            <Button className="!rounded py-3 px-6 font-bold">
              <Link href="/register">Mendaftar</Link>
            </Button>
          </div>
        </div>
      </div>
      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={() => closeModal()}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black/25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex min-h-full items-center justify-center p-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                  <Dialog.Title
                    as="h3"
                    className="text-lg font-medium leading-6 text-[--primary] "
                  >
                    {modalTitle}
                  </Dialog.Title>
                  <div className="mt-2">
                    <p className="text-sm text-gray-500">{responseMessage}</p>
                  </div>
                  <div className="mt-4 flex justify-end">
                    <button
                      type="button"
                      onClick={() => {
                        closeModal();
                      }}
                      className="text-sm flex gap-2 items-center"
                    >
                      <p>Close</p>{' '}
                    </button>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
};

ForgotPassword.getLayout = (page) => <ProtectedLayout>{page}</ProtectedLayout>;

export default ForgotPassword;
