import { NextPageWithLayout } from '../_app';
import ProtectedLayout from '@/layout/ProtectedLayout';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useRef, useState } from 'react';

import { playBall } from '@/libs/font';

import MetaHead from '@/components/shared/MetaHead';
import DivBreaker from '@/components/shared/DivBreaker';
import Wrapper from '@/components/shared/Wrapper';

import { RiArrowRightSLine } from 'react-icons/ri';
import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { setUserData } from '@/store/redux/user';
import { UserService } from '@/store/services';
import LoadingComponent from '@/components/shared/Loading';

const Profile: NextPageWithLayout = () => {
  const router = useRouter();
  const dispatch = useAppDispatch();
  const fileUploadRef: any = useRef();

  const { user, loadingAvatar } = useAppSelector((state) => state.user);
  const [reloadingPage, setReloadingPage] = useState(false);

  const handleLogout = () => {
    router.push('/login');
    dispatch(setUserData(undefined));
    localStorage.removeItem('user');
    localStorage.removeItem('accessToken');
  };

  const handleChangePhoto = (event: any) => {
    var file = event.target.files[0];
    var formData = new FormData();
    formData.append('file', file);
    dispatch(UserService.updateAvatar(formData)).then((res: any) => {
      if (res && res.payload.status === 200) {
        setReloadingPage(true);
        router.reload();
      }
    });
  };

  return (
    <>
      <MetaHead title="Profile | Rumah Mode" />
      <DivBreaker />
      <Wrapper
        backgroundColor="bg-[--primary]"
        className="pt-8 pb-12 flex flex-col items-center gap-8"
      >
        <div className="text-2xl text-neutral-01 font-bold ">
          A BIT ABOUT ME
        </div>
        <div className="w-full flex justify-center items-center flex-col lg:flex-row gap-12">
          <div className="flex flex-col">
            <div className="bg-neutral-01 p-8">
              <div className="flex items-center flex-col gap-6">
                {loadingAvatar || reloadingPage ? (
                  <div className="w-[200px] min-w-[200px] aspect-square bg-cover rounded-full flex justify-center align-middle">
                    <LoadingComponent />
                  </div>
                ) : (
                  <div
                    className="w-[200px] min-w-[200px] aspect-square bg-cover rounded-full"
                    style={{
                      backgroundImage:
                        user && user.avatar
                          ? `url(${user.avatar})`
                          : "url('/assets/images/profile/dummy-profile.svg')"
                    }}
                  />
                )}
                <div
                  className="text-xl font-medium text-[--primary] cursor-pointer"
                  onClick={() => fileUploadRef?.current.click()}
                >
                  <input
                    type="file"
                    id="file"
                    ref={fileUploadRef}
                    onChange={handleChangePhoto}
                    style={{ display: 'none' }}
                  />
                  Change Photo Profile
                </div>
              </div>
              <div className="mt-5 flex flex-col items-center">
                <div className={`${playBall.className} text-xl`}>My Name</div>
                <div className="text-[--primary] mt-4 px-6 py-[10px] font-bold border-[2px] border-[--primary] rounded-lg">
                  {user ? user.fullname : ''}
                </div>
              </div>
            </div>
            <button
              type="button"
              className="mt-10 text-neutral-01 px-6 py-[10px] font-bold border-[2px] bg-[--primary] rounded-lg border-neutral-01"
              onClick={handleLogout}
            >
              Logout
            </button>
          </div>
          <div className="w-full flex-grow grid md:grid-cols-2 gap-12">
            <div className="w-full rounded-xl overflow-hidden">
              <div className="p-6 box-border flex flex-col items-center gap-4 bg-neutral-01">
                <div className="w-[150px] aspect-square rounded-full bg-my-profile-icon bg-cover" />
                <div className="w-full text-2xl text-[--primary] font-bold">
                  My Profile
                </div>
                <Link
                  className="w-full flex items-center gap-2 text-lg text-[--primary] font-medium"
                  href="/profile/detail"
                >
                  <div>More</div>
                  <RiArrowRightSLine />
                </Link>
              </div>
              <div className="w-full h-4 bg-[#FF8B66]"></div>
            </div>
            {/*  */}
            <div className="w-full rounded-xl overflow-hidden">
              <div className="p-6 box-border flex flex-col items-center gap-4 bg-neutral-01">
                <div className="w-[150px] aspect-square rounded-full bg-my-poin-icon bg-cover" />
                <div className="w-full text-2xl text-[--primary] font-bold">
                  My Poin
                </div>
                <Link
                  className="w-full flex items-center gap-2 text-lg text-[--primary] font-medium"
                  href="/profile/detail"
                >
                  <div>More</div>
                  <RiArrowRightSLine />
                </Link>
              </div>
              <div className="w-full h-4 bg-[#32BEA6]"></div>
            </div>
            {/*  */}
            <div className="w-full rounded-xl overflow-hidden">
              <div className="p-6 box-border flex flex-col items-center gap-4 bg-neutral-01">
                <div className="w-[150px] aspect-square rounded-full bg-my-history-icon bg-cover" />
                <div className="w-full text-2xl text-[--primary] font-bold">
                  My History
                </div>
                <Link
                  className="w-full flex items-center gap-2 text-lg text-[--primary] font-medium"
                  href="/profile/detail"
                >
                  <div>More</div>
                  <RiArrowRightSLine />
                </Link>
              </div>
              <div className="w-full h-4 bg-[#B730AA]"></div>
            </div>
            {/*  */}
            <div className="w-full rounded-xl overflow-hidden">
              <div className="p-6 box-border flex flex-col items-center gap-4 bg-neutral-01">
                <div className="w-[150px] aspect-square rounded-full bg-my-reward-icon bg-cover" />
                <div className="w-full text-2xl text-[--primary] font-bold">
                  My Reward
                </div>
                <Link
                  className="w-full flex items-center gap-2 text-lg text-[--primary] font-medium"
                  href="/profile/detail"
                >
                  <div>More</div>
                  <RiArrowRightSLine />
                </Link>
              </div>
              <div className="w-full h-4 bg-[#E3A600]"></div>
            </div>
          </div>
        </div>
      </Wrapper>
    </>
  );
};

Profile.getLayout = (page) => <ProtectedLayout>{page}</ProtectedLayout>;

export default Profile;
