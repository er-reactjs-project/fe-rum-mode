import { NextPageWithLayout } from '../../_app';
import ProtectedLayout from '@/layout/ProtectedLayout';

import MetaHead from '@/components/shared/MetaHead';
import DivBreaker from '@/components/shared/DivBreaker';
import Wrapper from '@/components/shared/Wrapper';
import Button from '@/components/shared/Button';
import { Fragment, useRef, useState } from 'react';
import Modal from '@/components/shared/Modal';
import { useFormik } from 'formik';
import Link from 'next/link';
import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { formatDate1, formatTime1 } from '@/libs/helper';
import { useRouter } from 'next/router';
import { UserService } from '@/store/services';
import LoadingComponent from '@/components/shared/Loading';
import CustomDatePicker from '@/components/shared/CustomDatePicker';
import { Dialog, Transition } from '@headlessui/react';

const ProfileDetail: NextPageWithLayout = () => {
  const router = useRouter();
  const dispatch = useAppDispatch();
  const fileUploadRef: any = useRef();
  const { user, loadingAvatar, loading } = useAppSelector(
    (state) => state.user
  );
  const [reloadingPage, setReloadingPage] = useState(false);
  const [showModalForm, setShowModalForm] = useState(false);

  const [isOpen, setIsOpen] = useState(false);
  const [isSuccess, setIsSuccess] = useState(true);
  const [responseMessage, setResponseMessage] = useState('');

  const openModal = () => setIsOpen(true);
  const closeModal = () => {
    setIsOpen(false);
    if (isSuccess) {
      setTimeout(() => router.reload(), 500);
    }
  }

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      fullname: user ? user.fullname : '',
      phone_number: user ? user.phone_number : '',
      email: user ? user.email : '',
      birth_date: user ? new Date(user.birth_date) : undefined,
      address: user ? user.address : ''
    },
    onSubmit: async (values) => {
      let newValues: Record<string, any> = {};
      Object.assign(newValues, values);
      newValues['birth_date'] = values.birth_date
        ?.toISOString()
        .split('T', 1)[0];

      dispatch(UserService.updateProfile(newValues)).then((res: any) => {
        formik.resetForm();
        setShowModalForm(false);
        if (res?.error) {
          setResponseMessage(res.payload.error);
          setIsSuccess(false);
          openModal();
        } else {
          setIsSuccess(true);
          openModal();
        }
      });
    }
  });

  const handleChangePhoto = (event: any) => {
    var file = event.target.files[0];
    var formData = new FormData();
    formData.append('file', file);
    dispatch(UserService.updateAvatar(formData)).then((res: any) => {
      if (res && res.payload.status === 200) {
        setReloadingPage(true);
        router.reload();
      }
    });
  };

  return (
    <>
      <MetaHead title="Profile Detail | Rumah Mode" />
      <DivBreaker />
      <Wrapper backgroundColor="bg-[--primary]" className="pt-[90px] pb-[139px]">
        <div className="p-8 box-border rounded-xl bg-neutral-02 w-full flex flex-col gap-12 md:gap-6">
          <div className="flex flex-col lg:flex-row gap-[100px]">
            <div className="flex items-center flex-col gap-6">
              {loadingAvatar || reloadingPage ? (
                <div className="w-[200px] min-w-[200px] aspect-square bg-cover rounded-full flex justify-center align-middle">
                  <LoadingComponent />
                </div>
              ) : (
                <div
                  className="w-[200px] min-w-[200px] aspect-square bg-cover rounded-full"
                  style={{
                    backgroundImage:
                      user && user.avatar
                        ? `url(${user.avatar})`
                        : "url('/assets/images/profile/dummy-profile.svg')"
                  }}
                />
              )}
              <div
                className="text-xl font-medium text-[--primary] cursor-pointer"
                onClick={() => fileUploadRef?.current.click()}
              >
                <input
                  type="file"
                  id="file"
                  ref={fileUploadRef}
                  onChange={handleChangePhoto}
                  style={{ display: 'none' }}
                />
                Change Photo Profile
              </div>
            </div>
            <div className="flex flex-col gap-8">
              <div className="flex flex-col lg:flex-row lg:gap-2">
                <div className="flex gap-4 justify-between w-fit lg:min-w-[255px] text-xl font-medium">
                  <div>Nama</div>
                  <div>:</div>
                </div>
                <div className="text-xl text-[--primary] font-bold">
                  {user ? user.fullname : ''}
                </div>
              </div>
              <div className="flex flex-col lg:flex-row lg:gap-2">
                <div className="flex gap-4 justify-between w-fit lg:min-w-[255px] text-xl font-medium">
                  <div>Nomor Handphone</div>
                  <div>:</div>
                </div>
                <div className="text-xl text-[--primary] font-bold">
                  {user ? user.phone_number : ''}
                </div>
              </div>
              <div className="flex flex-col lg:flex-row lg:gap-2">
                <div className="flex gap-4 justify-between w-fit lg:min-w-[255px] text-xl font-medium">
                  <div>Email</div>
                  <div>:</div>
                </div>
                <div className="text-xl text-[--primary] font-bold">
                  {user ? user.email : ''}
                </div>
              </div>
              <div className="flex flex-col lg:flex-row lg:gap-2">
                <div className="flex gap-4 justify-between w-fit lg:min-w-[255px] text-xl font-medium">
                  <div>Tanggal Lahir</div>
                  <div>:</div>
                </div>
                <div className="text-xl text-[--primary] font-bold">
                  {user ? formatDate1(user.birth_date) : ''}
                </div>
              </div>
              <div className="flex flex-col lg:flex-row lg:gap-2">
                <div className="flex gap-4 justify-between w-fit lg:min-w-[255px] text-xl font-medium">
                  <div>Alamat</div>
                  <div>:</div>
                </div>
                <div className="text-xl text-[--primary] font-bold">
                  {user ? user.address : ''}
                </div>
              </div>
            </div>
          </div>
          <div className="flex gap-4 justify-between md:items-center flex-col md:flex-row">
            <div className="text-lg text-neutral-04">
              Last edit : {user ? formatDate1(user.updated_at) : ''} (
              {user ? formatTime1(user.updated_at) : ''})
            </div>
            <div className="flex justify-center lg:justify-normal gap-6">
              <Button
                type="button"
                className="rounded-md font-bold"
                onClick={() => setShowModalForm(true)}
              >
                Edit
              </Button>
              <Button
                className="rounded-md font-bold"
                color="secondary-bordered"
              >
                <Link href={'/profile'}>Kembali</Link>
              </Button>
            </div>
          </div>
        </div>
      </Wrapper>

      <Modal
        open={showModalForm}
        onClose={() => setShowModalForm(false)}
        className="w-[20%]"
      >
        <div className="flex flex-col items-start gap-6 w-full">
          <div className="w-full font-bold text-black text-2xl text-center">
            Edit Profile
          </div>
          <form onSubmit={formik.handleSubmit} className="w-full">
            <div className="flex flex-col items-start gap-4 w-full">
              <label className="flex flex-col items-start gap-1 w-full">
                <p className="text-[--primary] font-bold text-xl">Nama</p>
                <input
                  type="text"
                  name="fullname"
                  className="py-2 px-4 border-[--primary] rounded w-full border-2"
                  placeholder="Masukkan nama lengkap"
                  value={formik.values.fullname}
                  onChange={formik.handleChange}
                />
              </label>
              <label className="flex flex-col items-start gap-1 w-full">
                <p className="text-[--primary] font-bold text-xl">
                  Nomor Handphone
                </p>
                <input
                  type="text"
                  name="phone_number"
                  className="py-2 px-4 border-[--primary] rounded w-full border-2"
                  placeholder="Masukkan nomor handphone"
                  value={formik.values.phone_number}
                  onChange={formik.handleChange}
                />
              </label>
              <label className="flex flex-col items-start gap-1 w-full">
                <p className="text-[--primary] font-bold text-xl">Email</p>
                <input
                  type="email"
                  name="email"
                  className="py-2 px-4 border-[--primary] rounded w-full border-2"
                  placeholder="Masukkan email"
                  value={formik.values.email}
                  onChange={formik.handleChange}
                />
              </label>
              <label className="flex flex-col items-start gap-1 w-full">
                <p className="text-[--primary] font-bold text-xl">
                  Tanggal Lahir
                </p>
                <div className="custom-date-picker">
                  <CustomDatePicker
                    className="py-2 px-4 border-[--primary] rounded w-full border-2"
                    name="birth_date"
                    dateFormat="dd/MM/yyyy"
                    shouldCloseOnSelect={true}
                    selected={formik.values.birth_date}
                    onChange={(date) =>
                      formik.setFieldValue('birth_date', date)
                    }
                  />
                </div>
              </label>
              <label className="flex flex-col items-start gap-1 w-full">
                <p className="text-[--primary] font-bold text-xl">Alamat</p>
                <textarea
                  name="address"
                  className="py-2 px-4 border-[--primary] rounded w-full border-2"
                  placeholder="Masukkan alamat"
                  value={formik.values.address}
                  onChange={formik.handleChange}
                />
              </label>
              <Button
                type="submit"
                className="rounded-none flex-col w-full text-center"
              >
                {loading ? <LoadingComponent /> : 'Simpan'}
              </Button>
            </div>
          </form>
        </div>
      </Modal>

      {/* Dialogue */}
      <Transition appear show={isOpen} as={Fragment}>
        <Dialog
          as="div"
          className="relative z-10"
          onClose={() => closeModal()}
        >
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black/25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex min-h-full items-center justify-center p-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                  <Dialog.Title
                    as="h3"
                    className="text-lg font-medium leading-6 text-[--primary] "
                  >
                    {isSuccess ? 'Update Data Success' : 'Update Data Failed'}
                  </Dialog.Title>
                  <div className="mt-2">
                    <p className="text-sm text-gray-500">
                      {isSuccess ? '' : responseMessage}
                    </p>
                  </div>
                  <div className="mt-4 flex justify-end">
                    <button
                      type="button"
                      onClick={() => {
                        closeModal();
                      }}
                      className="text-sm flex gap-2 items-center"
                    >
                      <p>Close</p>{' '}
                    </button>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
};

ProfileDetail.getLayout = (page) => <ProtectedLayout>{page}</ProtectedLayout>;

export default ProfileDetail;
