import { useEffect } from 'react';
import { NextPageWithLayout } from '../../_app';
import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { ShoppingService } from '@/store/services';
import { FashionListTypes } from '@/store/redux/shopping/types';

import Link from 'next/link';
import MainLayout from '@/layout/MainLayout';
import MetaHead from '@/components/shared/MetaHead';
import DivBreaker from '@/components/shared/DivBreaker';
import Wrapper from '@/components/shared/Wrapper';
import Skeleton from '@/components/shared/Skeleton';

const ShoppingFashion: NextPageWithLayout = () => {
  const dispatch = useAppDispatch();
  const { lang } = useAppSelector((state) => state.global);
  const { loading, fashion } = useAppSelector((state) => state.shopping);

  useEffect(() => {
    const payload = { lang: lang };
    dispatch(ShoppingService.getFashionList(payload));
  }, [lang, dispatch]);

  return (
    <>
      <MetaHead title="Fashion | Rumah Mode" />
      <div className="sm:h-[100vh] 2xl:h-[100vh] min-[1700px]:h-[90vh] min-[1800px]:h-[85vh] min-[2000px]:h-[80vh] min-[2100px]:h-[75vh] min-[2400px]:h-[65vh] min-[2700px]:h-[60vh] min-[3200px]:h-[48vh] min-[4000px]:h-[40vh] min-[4800px]:h-[33vh] min-[5400px]:h-[30vh] min-[6400px]:h-[25vh] flex flex-col">
        <DivBreaker />
        {loading && (
          <div className="flex-grow">
            <Skeleton loading={loading} type="image" fullHeight={true} />
          </div>
        )}
        {!loading && fashion && (
          <div
            style={{ backgroundImage: `url(${fashion?.banner})` }}
            className="flex-grow bg-center bg-cover aspect-[16/9] md:aspect-none"
          ></div>
        )}
      </div>
      <Wrapper
        backgroundColor="bg-neutral-02-background"
        className="flex flex-col items-center pt-[34px] pb-[50px]"
      >
        <Skeleton loading={loading} className="flex justify-center">
          {fashion && (
            <div dangerouslySetInnerHTML={{ __html: fashion.description_1 }} />
          )}
        </Skeleton>

        <div className="w-full grid md:grid-cols-2 lg:grid-cols-3 gap-12 my-[50px]">
          {loading &&
            [...Array(3)].map((_, i) => (
              <div key={i} className="flex flex-col items-center gap-2">
                <Skeleton
                  loading={loading}
                  type="image"
                  className="aspect-[17/22]"
                />
                <Skeleton loading={loading} type="rectangle" className="h-12" />
              </div>
            ))}
          {!loading &&
            fashion &&
            fashion.items.map((fashion: FashionListTypes, index: number) => (
              <Link key={index} href={`/shopping/fashion/${fashion?.id}`}>
                <div className="rounded-3xl overflow-hidden">
                  <div
                    style={{ backgroundImage: `url(${fashion?.main_image})` }}
                    className={`w-full aspect-[17/22] bg-cover bg-center`}
                  />
                  <div className="p-6 bg-neutral-02 text-[--primary] text-[32px] text-center">
                    {fashion?.name}
                  </div>
                </div>
              </Link>
            ))}
        </div>
        {/*  */}
        <Skeleton loading={loading} className="flex justify-center">
          {fashion && (
            <div dangerouslySetInnerHTML={{ __html: fashion.description_2 }} />
          )}
        </Skeleton>
      </Wrapper>
    </>
  );
};

ShoppingFashion.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default ShoppingFashion;
