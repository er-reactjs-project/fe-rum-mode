import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { NextPageWithLayout } from '../../../_app';
import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { ShoppingService } from '@/store/services';
import { FashionImageTypes } from '@/store/redux/shopping/types';

import MainLayout from '@/layout/MainLayout';
import MetaHead from '@/components/shared/MetaHead';
import DivBreaker from '@/components/shared/DivBreaker';
import Wrapper from '@/components/shared/Wrapper';
import Skeleton from '@/components/shared/Skeleton';

import { RiFacebookFill, RiTiktokFill } from 'react-icons/ri';
import { SiInstagram } from 'react-icons/si';
import { SlGlobe } from 'react-icons/sl';

const ShoppingFashionBrand: NextPageWithLayout = () => {
  const route = useRouter();
  const dispatch = useAppDispatch();
  const { lang } = useAppSelector((state) => state.global);
  const { loading, fashionDetail } = useAppSelector((state) => state.shopping);
  const id = route.query.id;

  useEffect(() => {
    const payload = { lang: lang };
    if (id) {
      dispatch(ShoppingService.getFashionDetail({ id: id, payload: payload }));
    }
  }, [lang, id, dispatch]);

  return (
    <>
      <MetaHead title="Fashion Brand | Rumah Mode" />
      <div className="sm:h-[100vh] 2xl:h-[100vh] min-[1700px]:h-[90vh] min-[1800px]:h-[85vh] min-[2000px]:h-[80vh] min-[2100px]:h-[75vh] min-[2400px]:h-[65vh] min-[2700px]:h-[60vh] min-[3200px]:h-[48vh] min-[4000px]:h-[40vh] min-[4800px]:h-[33vh] min-[5400px]:h-[30vh] min-[6400px]:h-[25vh] flex flex-col">
        <DivBreaker />
        {loading && (
          <div className="flex-grow">
            <Skeleton loading={loading} type="image" fullHeight={true} />
          </div>
        )}
        {!loading && fashionDetail && (
          <div
            style={{ backgroundImage: `url(${fashionDetail?.banner})` }}
            className="flex-grow bg-center bg-cover"
          ></div>
        )}
      </div>
      <Wrapper className="flex flex-col items-center pt-[34px] pb-[50px]">
        <Skeleton loading={loading} className="flex justify-center">
          {fashionDetail && (
            <div
              dangerouslySetInnerHTML={{ __html: fashionDetail.description_1 }}
            />
          )}
        </Skeleton>

        <div className="mt-10 flex gap-[34px] justify-center">
          <a href={fashionDetail?.fb_link}>
            <div className="w-[58px] h-[58px] bg-[--primary] text-neutral-01 rounded-full text-[28px] flex justify-center items-center">
              <RiFacebookFill />
            </div>
          </a>
          <a href={fashionDetail?.ig_link}>
            <div className="w-[58px] h-[58px] bg-[--primary] text-neutral-01 rounded-full text-[28px] flex justify-center items-center">
              <SiInstagram />
            </div>
          </a>
          <a href={fashionDetail?.tiktok_link}>
            <div className="w-[58px] h-[58px] bg-[--primary] text-neutral-01 rounded-full text-[28px] flex justify-center items-center">
              <RiTiktokFill />
            </div>
          </a>
          <a href={fashionDetail?.website_link}>
            <div className="w-[58px] h-[58px] bg-[--primary] text-neutral-01 rounded-full text-[28px] flex justify-center items-center">
              <SlGlobe />
            </div>
          </a>
        </div>

        <div className="w-full grid lg:grid-cols-2 gap-[50px] my-[50px]">
          {loading &&
            [...Array(2)].map((_, i) => (
              <div key={i} className="flex flex-col items-center gap-2">
                <Skeleton
                  loading={loading}
                  type="image"
                  className="aspect-square"
                />
              </div>
            ))}
          {!loading &&
            fashionDetail &&
            fashionDetail.images.map(
              (item: FashionImageTypes, index: number) => (
                <div
                  key={index}
                  style={{ backgroundImage: `url(${item?.image})` }}
                  className="w-full aspect-square bg-cover bg-center"
                ></div>
              )
            )}
        </div>

        <Skeleton loading={loading} className="flex justify-center">
          {fashionDetail && (
            <div
              dangerouslySetInnerHTML={{ __html: fashionDetail.description_2 }}
            />
          )}
        </Skeleton>
      </Wrapper>
    </>
  );
};

ShoppingFashionBrand.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default ShoppingFashionBrand;
