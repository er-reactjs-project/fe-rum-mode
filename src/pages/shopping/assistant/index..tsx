import { NextPageWithLayout } from '../../_app';
import MainLayout from '@/layout/MainLayout';

import MetaHead from '@/components/shared/MetaHead';
import DivBreaker from '@/components/shared/DivBreaker';
import Wrapper from '@/components/shared/Wrapper';

import { SiWhatsapp } from 'react-icons/si';
import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { useEffect } from 'react';
import { ShoppingService } from '@/store/services';
import Skeleton from '@/components/shared/Skeleton';

const ShoppingAssistant: NextPageWithLayout = () => {
  const dispatch = useAppDispatch();
  const { lang } = useAppSelector((state) => state.global);
  const { loading, assistant } = useAppSelector((state) => state.shopping);

  useEffect(() => {
    const payload = { lang: lang };
    dispatch(ShoppingService.getShoppingAssistant(payload));
  }, [lang, dispatch]);

  return (
    <>
      <MetaHead title="Shopping Assistant | Rumah Mode" />
      <div className="sm:h-[100vh] 2xl:h-[100vh] min-[1700px]:h-[90vh] min-[1800px]:h-[85vh] min-[2000px]:h-[80vh] min-[2100px]:h-[75vh] min-[2400px]:h-[65vh] min-[2700px]:h-[60vh] min-[3200px]:h-[48vh] min-[4000px]:h-[40vh] min-[4800px]:h-[33vh] min-[5400px]:h-[30vh] min-[6400px]:h-[25vh] flex flex-col">
        <DivBreaker />
        {loading && (
          <div className="flex-grow">
            <Skeleton loading={loading} type="image" fullHeight={true} />
          </div>
        )}
        {!loading && assistant && (
          <div
            style={{ backgroundImage: `url(${assistant.banner})` }}
            className="flex-grow bg-center bg-cover aspect-[16/9] md:aspect-none"
          ></div>
        )}
      </div>
      <Wrapper className="flex flex-col items-center pt-[34px] pb-[50px]">
        <Skeleton loading={loading} className="flex justify-center">
          {assistant && (
            <div
              dangerouslySetInnerHTML={{ __html: assistant.description_1 }}
            />
          )}
        </Skeleton>

        <div className="w-full my-[50px] flex flex-col lg:flex-row items-center gap-[50px]">
          <Skeleton loading={loading} className="!max-w-[400px] lg:pl-[50px]">
            {assistant && (
              <div
                dangerouslySetInnerHTML={{ __html: assistant.description_2 }}
              />
            )}
          </Skeleton>
          <div className="hidden lg:flex lg:w-[3px] lg:h-[394px] bg-neutral-08" />
          <div className="w-full lg:flex-grow grid md:grid-cols-2 gap-[44px]">
            {loading &&
              [...Array(4)].map((_, i) => (
                <Skeleton
                  key={i}
                  loading={loading}
                  type="rectangle"
                  className="h-12"
                />
              ))}

            {!loading &&
              assistant &&
              assistant.contacts.map((item, index) => (
                <a key={index} href={`https://wa.me/${item.contact}`}>
                  <div className="w-full flex justify-center items-center gap-4 py-[18px] rounded-[100px] text-2xl text-[--primary] font-medium border-[2.5px] border-[--primary]">
                    <SiWhatsapp />
                    Contact Admin {index >= 9 ? '' : '0'}
                    {index + 1}
                  </div>
                </a>
              ))}
          </div>
        </div>

        <Skeleton loading={loading}>
          {assistant && (
            <div
              dangerouslySetInnerHTML={{ __html: assistant.description_3 }}
            />
          )}
        </Skeleton>
      </Wrapper>
    </>
  );
};

ShoppingAssistant.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default ShoppingAssistant;
