import { useEffect } from 'react';
import { NextPageWithLayout } from './_app';
import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { LandingPageService, SpecialOfferService } from '@/store/services';
import MainLayout from '@/layout/MainLayout';

import MetaHead from '@/components/shared/MetaHead';

import HomeSlider from '@/components/home/Slider';
import HomeDescription from '@/components/home/Description';
import HomeToDo from '@/components/home/ToDo';
import HomeOffers from '@/components/home/Offers';
import HomeEvent from '@/components/home/Event';
import DivBreaker from '@/components/shared/DivBreaker';

import Skeleton from '@/components/shared/Skeleton';

const Home: NextPageWithLayout = () => {
  const dispatch = useAppDispatch();
  const { lang } = useAppSelector((state) => state.global);
  const { loading, landingPageData } = useAppSelector((state) => state.landing);

  useEffect(() => {
    const payload = { lang: lang };
    dispatch(LandingPageService.getLandingPage(payload));
    dispatch(SpecialOfferService.getListOfferItems(payload));
  }, [dispatch, lang]);

  return (
    <>
      <MetaHead title="Home Page" />
      <div className="lg:h-[100vh] 2xl:h-[100vh] min-[1700px]:h-[90vh] min-[1800px]:h-[85vh] min-[2000px]:h-[80vh] min-[2100px]:h-[75vh] min-[2400px]:h-[65vh] min-[2700px]:h-[60vh] min-[3200px]:h-[48vh] min-[4000px]:h-[40vh] min-[4800px]:h-[33vh] min-[5400px]:h-[30vh] min-[6400px]:h-[25vh] flex flex-col">
        <DivBreaker />
        {loading && (
          <div className="flex-grow">
            <Skeleton loading={loading} type="image" fullHeight={true} />
          </div>
        )}

        {!loading && landingPageData && (
          <HomeSlider sliderData={landingPageData.banners} />
        )}
      </div>

      <HomeDescription />
      <HomeToDo />
      <HomeOffers />
      <HomeEvent />
    </>
  );
};

Home.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default Home;
