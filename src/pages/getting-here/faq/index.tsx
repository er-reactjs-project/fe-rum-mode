import { NextPageWithLayout } from '../../_app';
import MainLayout from '@/layout/MainLayout';

import MetaHead from '@/components/shared/MetaHead';
import FaqBanner from '@/components/FAQ/Banner';
import FaqContent from '@/components/FAQ/Faq';
import DivBreaker from '@/components/shared/DivBreaker';

const Faq: NextPageWithLayout = () => {
  return (
    <>
      <MetaHead title="FAQ | Rumah Mode" />
      <div className="sm:h-[100vh] 2xl:h-[100vh] min-[1700px]:h-[90vh] min-[1800px]:h-[85vh] min-[2000px]:h-[80vh] min-[2100px]:h-[75vh] min-[2400px]:h-[65vh] min-[2700px]:h-[60vh] min-[3200px]:h-[48vh] min-[4000px]:h-[40vh] min-[4800px]:h-[33vh] min-[5400px]:h-[30vh] min-[6400px]:h-[25vh] flex flex-col">
        <DivBreaker />
        <FaqBanner />
      </div>
      <FaqContent />
    </>
  );
};

Faq.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default Faq;
