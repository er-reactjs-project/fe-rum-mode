import { NextPageWithLayout } from '../../_app';

import MainLayout from '@/layout/MainLayout';
import MetaHead from '@/components/shared/MetaHead';
import DivBreaker from '@/components/shared/DivBreaker';
import Wrapper from '@/components/shared/Wrapper';

const Access: NextPageWithLayout = () => {
  return (
    <>
      <MetaHead title="Access | Rumah Mode" />
      <DivBreaker />
      <Wrapper
        backgroundColor="bg-[--primary]"
        className="flex flex-col justify-center"
      >
        <div className="text-neutral-01 text-[32px] text-center font-medium mt-8 mb-2">
          ACCESS
        </div>
        <div className="text-neutral-01 text-[32px] text-center font-bold">
          HOW DO YOU GET HERE
        </div>

        <div className="my-8 w-full flex flex-col lg:flex-row rounded-2xl overflow-hidden">
          <div className="flex-grow h-[492px]">
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3961.063986311253!2d107.59710637354351!3d-6.882938667346926!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e695940813ad%3A0x55102548df571383!2sRumah%20Mode%20Factory%20Outlet!5e0!3m2!1sid!2sid!4v1703732807836!5m2!1sid!2sid"
              width="100%"
              height="492px"
              style={{ border: 0 }}
              allowFullScreen={false}
              loading="lazy"
              referrerPolicy="no-referrer-when-downgrade"
            ></iframe>
          </div>
          <div className="min-w-[391px] p-6 box-border flex flex-col gap-6 bg-neutral-02-background text-[--primary]">
            <div>
              <div className="text-2xl font-bold">Street :</div>
              <div className="text-xl">
                Jl. Dr. Setiabudi No. 41, Pasteur, Kec. Sukajadi
              </div>
            </div>
            <div>
              <div className="text-2xl font-bold">Plus Code :</div>
              <div className="text-xl">
                4H8X+RV Pasteur, Bandung City, West Java
              </div>
            </div>
            <div>
              <div className="text-2xl font-bold">Open Hour :</div>
              <div className="flex flex-col gap-2">
                <div className="flex justify-between text-xl pr-8">
                  <div>Sunday</div>
                  <div>00 : 00</div>
                </div>
                <div className="flex justify-between text-xl pr-8">
                  <div>Monday</div>
                  <div>00 : 00</div>
                </div>
                <div className="flex justify-between text-xl pr-8">
                  <div>Teusday</div>
                  <div>00 : 00</div>
                </div>
                <div className="flex justify-between text-xl pr-8">
                  <div>Wednesday</div>
                  <div>00 : 00</div>
                </div>
                <div className="flex justify-between text-xl pr-8">
                  <div>Thurstday</div>
                  <div>00 : 00</div>
                </div>
                <div className="flex justify-between text-xl pr-8">
                  <div>Friday</div>
                  <div>00 : 00</div>
                </div>
                <div className="flex justify-between text-xl pr-8">
                  <div>Saturday</div>
                  <div>00 : 00</div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="text-neutral-01 text-2xl text-center font-medium">
          You can access by :
        </div>

        <div className="w-full grid lg:grid-cols-3 gap-[50px] my-8">
          <div>
            <div className="text-neutral-01 text-2xl font-bold mb-6">
              Online Transport
            </div>
            <div className="grid lg:grid-cols-2 gap-6">
              <div className="rounded-xl bg-neutral-02-background px-6 py-4">
                <div className="w-full aspect-[15/4] bg-cover bg-center bg-gojek-logo" />
              </div>
              <div className="rounded-xl bg-neutral-02-background px-6 py-4">
                <div className="w-full aspect-[15/4] bg-cover bg-center bg-grab-logo" />
              </div>
              <div className="rounded-xl bg-neutral-02-background px-6 py-4">
                <div className="w-full aspect-[15/4] bg-cover bg-center bg-indrive-logo" />
              </div>
              <div className="rounded-xl bg-neutral-02-background px-6 py-4">
                <div className="w-full aspect-[15/4] bg-cover bg-center bg-maxim-logo" />
              </div>
            </div>
          </div>
          <div>
            <div className="text-neutral-01 text-2xl font-bold mb-6">Taxi</div>
            <div className="grid lg:grid-cols-2 gap-6">
              <div className="rounded-xl bg-neutral-02-background px-6 py-4">
                <div className="w-full aspect-[15/4] bg-cover bg-center bg-gojek-logo" />
              </div>
            </div>
          </div>
          <div>
            <div className="text-neutral-01 text-2xl font-bold mb-6">
              Public Transport
            </div>
            <div className="grid lg:grid-cols-2 gap-6">
              <div className="rounded-xl bg-neutral-02-background px-6 py-4">
                <div className="w-full aspect-[15/4] bg-cover bg-center bg-angkot-logo" />
              </div>
              <div className="rounded-xl bg-neutral-02-background px-6 py-4">
                <div className="w-full aspect-[15/4] bg-cover bg-center bg-tmetro-logo" />
              </div>
            </div>
          </div>
        </div>
      </Wrapper>
    </>
  );
};

Access.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default Access;
