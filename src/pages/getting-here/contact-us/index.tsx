import React, { useEffect } from 'react';
import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { ContactUsService } from '@/store/services';
import { NextPageWithLayout } from '../../_app';
import { ContactsItemsTypes } from '@/store/redux/contact-us/types';

import MainLayout from '@/layout/MainLayout';
import MetaHead from '@/components/shared/MetaHead';
import DivBreaker from '@/components/shared/DivBreaker';
import Wrapper from '@/components/shared/Wrapper';
import Skeleton from '@/components/shared/Skeleton';

import { FaWhatsapp } from 'react-icons/fa';

const ContactUs: NextPageWithLayout = () => {
  const dispatch = useAppDispatch();
  const { lang } = useAppSelector((state) => state.global);
  const { loading, contactUs } = useAppSelector((state) => state.contactUs);

  useEffect(() => {
    const payload = { lang: lang };
    dispatch(ContactUsService.getContactUs(payload));
  }, [lang, dispatch]);

  const ContactItem = ({ id, name, phone_number }: ContactsItemsTypes) => {
    return (
      <div id={id} className="flex gap-4 items-center">
        <FaWhatsapp className="size-12" />
        <div className="flex items-start flex-col gap-2">
          <p className="font-bold text-xl">{name}</p>
          <p className="text-lg">{phone_number}</p>
        </div>
      </div>
    );
  };

  return (
    <>
      <MetaHead title="Contact Us | Rumah Mode" />
      <DivBreaker />
      <Wrapper
        backgroundColor="bg-[--primary]"
        className="text-white pt-12 pb-6 py-8"
      >
        <Skeleton loading={loading} className="flex justify-center">
          {contactUs && (
            <div dangerouslySetInnerHTML={{ __html: contactUs?.title }} />
          )}
        </Skeleton>
      </Wrapper>
      <Wrapper
        backgroundColor="bg-[--primary] bg-[url(/assets/images/contact_bg.svg)] bg-no-repeat bg-center bg-[length:511px_652px]"
        className="flex flex-col lg:flex-row flex-wrap gap-12 pb-12 items-start justify-center"
      >
        {loading &&
          [...Array(3)].map((_, i) => (
            <div key={i} className="flex flex-col">
              <Skeleton
                loading={loading}
                type="rectangle"
                className="!w-[300px] h-[400px]"
              />
            </div>
          ))}

        {!loading &&
          contactUs &&
          Object.keys(contactUs.contacts).map((key, index) => (
            <div
              key={index}
              className="flex py-6 px-8 flex-col items-start gap-6 rounded-2xl bg-white text-[--primary]"
            >
              <p className=" font-bold text-2xl">{key}</p>
              {contactUs.contacts[key].map(
                (contact: ContactsItemsTypes, indexContact: number) => (
                  <React.Fragment key={indexContact}>
                    <ContactItem
                      id={contact.id}
                      name={contact.name}
                      phone_number={contact.phone_number}
                    />
                  </React.Fragment>
                )
              )}
            </div>
          ))}
      </Wrapper>
    </>
  );
};

ContactUs.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default ContactUs;
