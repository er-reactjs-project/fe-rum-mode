/* eslint-disable @next/next/no-img-element */
import { NextPageWithLayout } from '../../_app';
import MainLayout from '@/layout/MainLayout';

import { useRouter } from 'next/router';
// import Image from 'next/image';

import MetaHead from '@/components/shared/MetaHead';
import Button from '@/components/shared/Button';
import DivBreaker from '@/components/shared/DivBreaker';

import { MdKeyboardArrowLeft } from 'react-icons/md';

import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { useEffect } from 'react';
import { DiscoverService } from '@/store/services';
import Skeleton from '@/components/shared/Skeleton';
import Wrapper from '@/components/shared/Wrapper';

const DetailFacility: NextPageWithLayout = () => {
  const router = useRouter();
  const dispatch = useAppDispatch();
  const { loading, facility } = useAppSelector((state) => state.discover);
  const { lang } = useAppSelector((state) => state.global);

  useEffect(() => {
    if (router.query.id) {
      const payload = {
        id: router.query.id,
        lang: lang
      };

      dispatch(DiscoverService.getDetailFacility(payload));
    }
  }, [router, dispatch, lang]);

  return (
    <>
      <MetaHead
        title={`${facility ? facility.name : 'Facility'} | Rumah Mode`}
      />
      <DivBreaker />
      <div className="pt-8">
        <Button
          className="py-3 pl-8 pr-12 gap-3 font-bold rounded-none"
          icon={<MdKeyboardArrowLeft className="size-7" />}
          onClick={router.back}
        >
          Back
        </Button>
      </div>
      <Wrapper className="mt-9 max-w-[1400px]">
        <div className="flex items-start gap-10">
          <div className="w-1/5">
            <Skeleton loading={loading} type="image">
              {facility && (
                <img
                  src={facility.icon}
                  alt="Restroom image"
                  width={250}
                  height={200}
                />
              )}
            </Skeleton>
          </div>
          <div className="flex flex-grow flex-col gap-[34px] items-start">
            <div className="flex flex-col gap-2 self-stretch items-start ">
              <Skeleton loading={loading}>
                {facility && (
                  <>
                    <p className="text-2xl font-bold text-[--primary]">
                      {facility.name}
                    </p>
                    <div
                      dangerouslySetInnerHTML={{ __html: facility.description }}
                    />
                  </>
                )}
              </Skeleton>
            </div>
            <div className="w-full flex gap-[34px] flex-col items-start mb-8">
              {loading && (
                <Skeleton loading={loading} type="image" className="h-52" />
              )}
              {!loading &&
                facility &&
                facility.images.map((item, index) => (
                  <div
                    key={index}
                    className="bg-cover bg-no-repeat w-full aspect-[16/9]"
                    style={{
                      backgroundImage: `url(${item.image})`
                    }}
                  />
                ))}
            </div>
          </div>
        </div>
      </Wrapper>
    </>
  );
};

DetailFacility.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default DetailFacility;
