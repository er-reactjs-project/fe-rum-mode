/* eslint-disable @next/next/no-img-element */
/* eslint-disable react-hooks/exhaustive-deps */
import { NextPageWithLayout } from '../../_app';
import MainLayout from '@/layout/MainLayout';

// import Image from 'next/image';
import Link from 'next/link';

import MetaHead from '@/components/shared/MetaHead';
import Navigation from '@/components/discover/Navigation';
import DivBreaker from '@/components/shared/DivBreaker';
import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { useEffect } from 'react';
import { DiscoverService } from '@/store/services';
import Skeleton from '@/components/shared/Skeleton';
import Wrapper from '@/components/shared/Wrapper';

const Facilities: NextPageWithLayout = () => {
  const dispatch = useAppDispatch();
  const { loading, discover, loadingFacilities, facilities } = useAppSelector(
    (state) => state.discover
  );
  const { lang } = useAppSelector((state) => state.global);

  useEffect(() => {
    const payload = {
      lang: lang,
      menu: 'facilities'
    };

    dispatch(DiscoverService.getDiscoverByMenu(payload));
    dispatch(DiscoverService.getFacilities(payload));
  }, [lang]);

  return (
    <>
      <MetaHead title="Facilities | Rumah Mode" />
      <div className="flex flex-col">
        <DivBreaker />
        {loading && (
          <div className="flex-grow">
            <Skeleton loading={loading} type="image" fullHeight={true} />
          </div>
        )}
        {discover && (
          <div
            className="flex-grow bg-center bg-cover aspect-[7/1] "
            style={{ backgroundImage: `url(${discover?.banners[0].banner})` }}
          ></div>
        )}
      </div>

      <Wrapper className="pt-3">
        <Skeleton loading={loading} paragraph={{ rows: 1 }}>
          {discover && (
            <div dangerouslySetInnerHTML={{ __html: discover.description }} />
          )}
        </Skeleton>
        <div className="flex justify-center items-center pt-5">
          <div className="grid grid-cols-4 gap-8 w-full">
            {loadingFacilities &&
              [...Array(4)].map((_, i) => (
                <div key={i} className="flex flex-col items-center gap-2">
                  <Skeleton loading={loadingFacilities} type="image" />
                  <Skeleton
                    loading={loadingFacilities}
                    type="rectangle"
                    className="h-4"
                  />
                </div>
              ))}
            {!loadingFacilities &&
              facilities &&
              facilities.map((item) => (
                <Link
                  key={item.id}
                  className="flex flex-col items-center gap-2 mt-2 hover:mt-0 duration-200"
                  href={`/discover/facilities/${item.id}`}
                >
                  <img
                    src={item.icon}
                    alt={item.name}
                    width={161}
                    height={130}
                  />
                  <p className="text-center text-lg">{item.name}</p>
                </Link>
              ))}
          </div>
        </div>
      </Wrapper>
      <Navigation active="facilities" />
    </>
  );
};

Facilities.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default Facilities;
