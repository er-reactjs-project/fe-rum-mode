/* eslint-disable react-hooks/exhaustive-deps */
import { NextPageWithLayout } from '../_app';
import MainLayout from '@/layout/MainLayout';

import MetaHead from '@/components/shared/MetaHead';
import Carousel from '@/components/discover/Carousel';
import Navigation from '@/components/discover/Navigation';
import DivBreaker from '@/components/shared/DivBreaker';
import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { useEffect } from 'react';
import { DiscoverService } from '@/store/services';
import Skeleton from '@/components/shared/Skeleton';

const Store: NextPageWithLayout = () => {
  const dispatch = useAppDispatch();
  const { loading, discover } = useAppSelector((state) => state.discover);
  const { lang } = useAppSelector((state) => state.global);

  useEffect(() => {
    const payload = {
      lang: lang,
      menu: 'store'
    };

    dispatch(DiscoverService.getDiscoverByMenu(payload));
  }, [lang]);

  return (
    <>
      <MetaHead title="Store | Rumah Mode" />
      <DivBreaker />
      {loading && (
        <div className="w-4/5 text-center m-auto pt-8">
          <Skeleton loading={loading} />
        </div>
      )}
      {!loading && discover && (
        <>
          <div
            className="w-4/5 text-center m-auto pt-8"
            dangerouslySetInnerHTML={{ __html: discover.description }}
          />
        </>
      )}
      {!loading && discover ? (
        <Carousel items={discover.banners} />
      ) : (
        <div className="w-4/5 text-center m-auto pt-5 h-96">
          <Skeleton loading={loading} type="image" fullHeight={true} />
        </div>
      )}
      <Navigation active="store" />
    </>
  );
};

Store.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default Store;
