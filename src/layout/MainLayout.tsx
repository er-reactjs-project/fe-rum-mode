import { ReactNode, useEffect, useState } from 'react';
import Link from 'next/link';
import Image from 'next/image';

import Header from '@/components/shared/Header';
import Dropdown from '@/components/shared/Drowdown';
// import SearchInput from '@/components/shared/SearchInput';
import Footer from '@/components/shared/Footer';
import { useAppDispatch, useAppSelector } from '@/hooks/store';
import { PersonalizationService, ShoppingService } from '@/store/services';
import Wrapper from '@/components/shared/Wrapper';

import { IoMenu } from 'react-icons/io5';
import { IoClose } from 'react-icons/io5';
import { IoIosArrowDown } from 'react-icons/io';

import { SiWhatsapp, SiInstagram, SiTiktok } from 'react-icons/si';
import { IoLocationOutline } from 'react-icons/io5';
import fonts from '@/libs/font';

interface Props {
  children: ReactNode;
}

const MainLayout = ({ children }: Props) => {
  const dispatch = useAppDispatch();
  const { estore } = useAppSelector((state) => state.shopping);
  const { loading, personalization } = useAppSelector(
    (state) => state.personalization
  );
  const [openMenu, setOpenMenu] = useState(false);
  const [selectedNav, setSelectedNav] = useState(999);
  const [primaryFont, setPrimaryFont] = useState(fonts.Glory);
  const [fontMenu, setFontMenu] = useState(fonts.Glory);

  const NavigationItem = [
    {
      text: 'DISCOVER',
      child: [
        { text: 'Store', href: '/discover/store' },
        { text: 'Food Court', href: '/discover/food-court' },
        { text: 'Kids Playground', href: '/discover/kids-playground' },
        { text: 'Siong Sui Clinic Center', href: '/discover/clinic' },
        {
          text: 'Ekakarya Graha Flora',
          href: '/discover/ekakarya'
        },
        { text: 'Gallery', href: '/discover/gallery' },
        { text: 'Facilities', href: '/discover/facilities' }
      ]
    },
    {
      text: 'SHOPPING',
      child: [
        { text: 'Fashion', href: '/shopping/fashion' },
        { text: 'Shopping Assistance', href: '/shopping/assistant' },
        { text: 'E-Store', href: estore ? estore.link : '' }
      ]
    },
    {
      text: 'MODE KITCHEN',
      child: [
        { text: 'Food', href: '/kitchen#food' },
        { text: 'Dessert & Beverages', href: '/kitchen#dessert' },
        {
          text: 'Traditional Snack',
          href: '/kitchen#traditional-snack'
        },
        { text: 'Oleh-Oleh Bandung', href: '/kitchen#oleh-oleh' }
      ]
    }
  ];

  useEffect(() => {
    dispatch(ShoppingService.getEstore({}));
    dispatch(PersonalizationService.getPersonalization({}));
  }, [dispatch]);

  useEffect(() => {
    if (personalization && personalization.font_family) {
      const font = fonts[personalization.font_family_menu];
      if (font) setPrimaryFont(font);
    }
    
    if (personalization && personalization.font_family_menu) {
      const font = fonts[personalization.font_family_menu];
      if (font) setFontMenu(font);
    }
  }, [personalization])
  

  return loading ? (
    <div>Loading</div>
  ) : (
    <div
      style={{
        ['--primary' as any]: personalization?.primary_color ?? '#010066',
        ['--primary-alpha' as any]: `${
          personalization?.primary_color ?? '#010066'
        }bf`,
        ['--secondary' as any]: personalization?.secondary_color ?? '#C97A4E'
      }}
      className={`${primaryFont.className} relative min-h-full lg:max-w-[1600px] lg:my-0 lg:mx-auto`}
    >
      <div className="absolute z-49 w-full">
        <Header />
        <nav
          className={`${fontMenu.className} font-bold hidden lg:grid grid-cols-2 md:grid-cols-3 lg:grid-cols-6 items-center px-[50px] bg-neutral-02-background`}
        >
          <Link href="/information/about-us">
            <div className="text-[--primary] group-hover:text-secondary text-lg">
              ABOUT US
            </div>
          </Link>
          {NavigationItem?.map((navigation, indexNavigation) => (
            <div key={indexNavigation}>
              <Dropdown
                title={navigation.text}
                isLink
                child={navigation.child}
              />
            </div>
          ))}
          <Link href="/reservation">
            <div className="text-[--primary] group-hover:text-secondary text-lg">
              RESERVATION
            </div>
          </Link>
          {/* <div>
            <SearchInput />
          </div> */}
        </nav>
        <Wrapper
          className="lg:hidden"
          backgroundColor={'bg-neutral-02-background'}
        >
          <nav className="flex items-center py-4 gap-8 text-3xl">
            {/* <div className="w-full">
              <SearchInput />
            </div> */}
            <div
              className="text-black flex items-center font-bold text-xl gap-2"
              onClick={() => setOpenMenu(true)}
            >
              <IoMenu />
              Menu
            </div>
          </nav>
        </Wrapper>
      </div>
      {children}
      <Footer />
      {openMenu ? (
        <>
          <div className="fixed top-0 left-0 w-[100vw] min-h-[100vh] flex flex-col items-center z-[99] bg-neutral-02-background">
            <div className="w-full fixed top-2 flex justify-between items-center text-3xl py-6 px-4 md:px-[36px] ">
              <Link href={'/'}>
                <Image
                  src="/assets/images/logo_blue.svg"
                  alt="white logo"
                  width={160}
                  height={0}
                />
              </Link>
              <IoClose onClick={() => setOpenMenu(false)} />
            </div>
            <nav className="w-full h-full flex flex-col px-[50px] bg-neutral-02-background mt-[130px] gap-2">
              <Link
                href="/information/about-us"
                onClick={() => setOpenMenu(false)}
              >
                <div className="text-[--primary] font-bold group-hover:text-secondary text-lg w-fit">
                  ABOUT US
                </div>
              </Link>
              {NavigationItem?.map((navigation, indexNavigation) => (
                <div
                  onClick={() => {
                    if (selectedNav === indexNavigation) {
                      setSelectedNav(999);
                    } else {
                      setSelectedNav(indexNavigation);
                    }
                  }}
                  key={indexNavigation}
                >
                  <div className="flex justify-between items-center">
                    <div className="text-[--primary] font-bold group-hover:text-secondary text-lg w-fit">
                      {navigation.text}
                    </div>
                    <IoIosArrowDown
                      className={` duration-200 ${
                        selectedNav === indexNavigation
                          ? 'rotate-180'
                          : 'rotate-0'
                      }`}
                    />
                  </div>
                  <div
                    className={`pl-8 gap-2 flex flex-col overflow-hidden ${
                      selectedNav === indexNavigation
                        ? 'duration-1000 max-h-[999px]'
                        : 'duration-500 max-h-[0px]'
                    }`}
                  >
                    {navigation.child.map((navchild, indexNavchild) => (
                      <Link
                        onClick={() => {
                          setOpenMenu(false);
                          setSelectedNav(999);
                        }}
                        className="font-semibold text-[--primary]"
                        key={indexNavchild}
                        href={navchild.href}
                      >
                        {navchild.text}
                      </Link>
                    ))}
                  </div>
                </div>
              ))}
              <Link href="/reservation" onClick={() => setOpenMenu(false)}>
                <div className="text-[--primary] font-bold group-hover:text-secondary text-lg w-fit">
                  RESERVATION
                </div>
              </Link>
            </nav>
            <div>
              <div className="flex gap-4 mt-12">
                <Link
                  href={'/'}
                  className="w-8 h-8 rounded-full bg-[--primary] flex justify-center items-center text-white text-lg shadow-white shadow-sm"
                >
                  <SiWhatsapp />
                </Link>
                <Link
                  href={'/'}
                  className="w-8 h-8 rounded-full bg-[--primary] flex justify-center items-center text-white text-lg shadow-white shadow-sm"
                >
                  <SiInstagram />
                </Link>
                <Link
                  href={'/'}
                  className="w-8 h-8 rounded-full bg-[--primary] flex justify-center items-center text-white text-lg shadow-white shadow-sm"
                >
                  <SiTiktok />
                </Link>
                <Link
                  href={'/'}
                  className="w-8 h-8 rounded-full bg-[--primary] flex justify-center items-center text-white text-lg shadow-white shadow-sm"
                >
                  <IoLocationOutline />
                </Link>
              </div>
            </div>
          </div>
        </>
      ) : (
        <></>
      )}
    </div>
  );
};

export default MainLayout;
