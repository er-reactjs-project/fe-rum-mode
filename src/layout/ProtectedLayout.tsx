/* eslint-disable no-unused-vars */
import { useAppDispatch } from '@/hooks/store';
import { UserService } from '@/store/services';
import { ReactNode } from 'react';
import MainLayout from './MainLayout';
import { getToken } from '@/libs/local-storage/token';
import { useRedirectToAfterLoad } from '@/hooks/useRedirectAfterLoad';
import { NextRouter } from 'next/router';
import { setUser } from '@/libs/local-storage/user';
import { User } from '@/store/redux/user/types';

interface Props {
  children: ReactNode;
}
const ProtectedLayout = ({ children }: Props) => {
  const dispatch = useAppDispatch();
  const authPath = ['/login', '/register', '/membership', '/forgot-password'];

  const { isLoading, isRedirecting } = useRedirectToAfterLoad(
    async (router: NextRouter) => {
      let storedToken = getToken();
      if (!storedToken) {
        if (authPath.includes(router.pathname)) {   
          return '';
        }
        return '/login';
      }
      if (storedToken && authPath.includes(router.pathname)) return '/profile';

      const response = await dispatch(UserService.getProfile({}));
      let payload = undefined as Record<string, any> | undefined
      if (response) {
        payload = response.payload as Record<string, any>;
      }

      if (!payload || payload?.status !== 200) {
        localStorage.removeItem('user');
        localStorage.removeItem('accessToken');
        return '/login';
      }

      const userData = payload.data.data as User;
      setUser(userData);

      return '';
    }
  );

  return !isLoading && !isRedirecting && <MainLayout>{children}</MainLayout>;
};

export default ProtectedLayout;
