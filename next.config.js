/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'be-dev.rumahmode.co.id',
        port: '',
        pathname: '/api/**',
      },
      {
        protocol: 'http',
        hostname: 'ec2-34-229-158-177.compute-1.amazonaws.com',
        port: '',
        pathname: '/api/**',
      },
      {
        protocol: 'http',
        hostname: '127.0.0.1',
        port: '5000',
        pathname: '/api/**',
      },
    ],
  },
}

module.exports = nextConfig
